
;                          SERVER

alias pacserv_findip {
  sockclose pacservip 
  if (!$1) || ($1 == 6) var %api 6
  else var %api 4
  sockopen -e $+ %api pacservip api $+ %api $+ .ipify.org 443
  if (!$1) sockmark pacservip 6
}
on *:sockopen:pacservip:if ($sockerr) return | sockwrite $sockname GET / $+(HTTP/1.0,$crlf,Host: $sock($sockname).addr,$str($crlf,2))
on *:sockread:pacservip:var %a | sockread %a
on *:sockclose:pacservip:{
  var %a
  sockread -f %a
  if ($sock(pacservip).mark == 6) {
    sockclose pacservip
    sockopen -e4 pacservip api.ipify.org 443
    sockmark pacservip %a
  }
  elseif ($dialog(pacman)) {
    if ($sock(pacservip).mark) {
      var %6 $regsubex($v1,/([a-fA-F0-9]+):?/g,$right(0000 $+ \1,4))
      var %4 $regsubex(%a,/(\d+)\.?/g,$base(\1,10,16,2)) $+ $base($did(pacman,36),10,16,4)
      did -vra pacman 98 pcmn:// $+ $lower($base(%6 $+ %4,16,36))  
    }
    elseif ($iptype(%a) == ipv6) {
      var %6 $regsubex(%a,/([a-fA-F0-9]+):?/g,$right(0000 $+ \1,4)) $+ $base($did(pacman,36),10,16,4)
      did -vra pacman 101 pcmn:// $+ $lower($base(%6 $+ %4,16,36))  

    }
    else {
      var %4 $regsubex(%a,/(\d+)\.?/g,$base(\1,10,16,2)) $+ $base($did(pacman,36),10,16,4)
      if (!%a) echo -sg pacman * api for getting ips is returning empty result, try creating a server less often.
      else did -vra pacman 164 pcmn:// $+ $lower($base(%6 $+ %4,16,36))  
    }

  }
}

; DIALOG EVENT
on *:dialog:pacman:*:*:{
  if ($devent == init) {
    did -e pacman 38
    did -c pacman 22 1
  }
  elseif ($devent == sclick) {
    if ($did == 47) && ($did(23) != $null) noop $input($v1,ou,Pacman - Password)
    elseif ($did == 98) && ($istok(sclick,$devent,32)) && (pcmn* iswm $did(98)) { clipboard $did(98) | .timer -ho 1 0 var % $+ pcmn $!did(pacman,98) $(|) did -ra pacman 98 $chr(10004) Copied to the clipboard $chr(10004) $(|) .timer -ho 1 1300 did -ra pacman 98 % $+ pcmn }
    elseif ($did == 101) && ($istok(sclick,$devent,32)) && (pcmn* iswm $did(101)) { clipboard $did(101) | .timer -ho 1 0 var % $+ pcmn $!did(pacman,101) $(|) did -ra pacman 101 $chr(10004) Copied to the clipboard $chr(10004) $(|) .timer -ho 1 1300 did -ra pacman 101 % $+ pcmn }
    elseif ($did == 164) && ($istok(sclick,$devent,32)) && (pcmn* iswm $did(164)) { clipboard $did(164) | .timer -ho 1 0 var % $+ pcmn $!did(pacman,164) $(|) did -ra pacman 164 $chr(10004) Copied to the clipboard $chr(10004) $(|) .timer -ho 1 1300 did -ra pacman 164 % $+ pcmn }
    elseif ($did == 4) dialog -t pacman Pacman - $iif($hget(pacserv),A server is running,No server is running)
    elseif ($did == 38) {
      if ($did(38) == Start) {
        if ($did(36) !isnum 0-65535) || ($did(37) !isnum 0-65535) return $input(Port $did(36) or $did(37) is invalid (not in the range 0-65535),wou,Pacman - Port error)
        elseif ($did(36) == $did(37)) return $input(Tcp and udp ports must be different!,ouw,Pacman - Server)
        elseif (!$portfree($did(36))) || (!$portfree($did(37))) return $input(Port $did(36) or $did(37) isn't free ! $+ $crlf $+ If you just closed a server  you might just need to wait a few seconds before trying again,owu,Pacman - Port error)
        if ($did(50) == $null) did -ra pacman 50 $did(75) $+ 's Server
        did -b pacman 36,37,38,39,45,23,40,50,79,12,11,73
        if ($did(45).state) did -e pacman 41
        $iif($did(39).state,.) $+ pacserv_start $did(36) $did(37) $did(23)
        .timer -m 1 800 did -rea pacman 38 Stop
      }
      else { 
        pacserv_stop
      }
    }
    elseif ($did == 40) {
      if ($regex($$input(Enter the server key,upe,Pacman - Server key),/([^@]+)@([^@]+)@([^@]+)(?:@([^@]+))?/)) noop $pacclient_start($regml(1), $regml(3),$regml(4),$regml(2))
    }
    elseif ($did == 41) clipboard $iif($hget(pacserv,ipv6),::1,127.0.0.1) $+ @ $+ $hget(pacserv,key) $+ @ $+ $hget(pacserv,tcpport) $+ @ $+ $hget(pacserv,password)
  }
  elseif ($devent == edit) {
    if ($did == 23) did $iif($did(23) == $null,-b,-e) pacman 47
    elseif ($istok(36 37,$did,32)) {
      did $iif($did(36) !isnum 0-65535 || $did(37) !isnum 0-65353,-b,-e) pacman 38
      var %r $mid($read($scriptdirpacman.mrc,tn,1),2)
      tokenize 32 %r
      write -dl1 $qt($scriptdirpacman.mrc)
      write -il1 $qt($scriptdirpacman.mrc) ; $+ $1-7 $did(91).state $did(92).state $did(93).state $did(73).state $did(94).state $did(97).state $did(99).state $did(100).state $did(96).state $iif($did(pacman,36) isnum 0-65535,$v1,8000) $iif($did(pacman,37) isnum 0-65535,$v1,8001)
      .reload -rs $qt($scriptdirpacman.mrc)
    }
  }
  elseif ($devent == close) pacserv_stop
}

; START SERVER
alias pacserv_start {
  if ($iptype($bindip(::)) == ipv6) && ($mouse.key !& 8) {
    socklisten -d $+ $iif($did(pacman,73).state,p) $+ $iif($did(pacman,100).state,u) $+ $iif($did(pacman,99),-n) :: pacserv $1
    if ($did(pacman,100).state) hadd -m pacserv dual 1
    hadd -m pacserv ipv6 1
  }
  else socklisten -d $+ $iif($did(pacman,73).state,p) $+ $iif($did(pacman,99),-n) 0.0.0.0 pacserv $1
  :error
  if ($error) {
    var %e $error
    reseterror
    pacserv_stop
    noop $input(An error occured on socklisten: $+ $crlf $+ %e,wuo,Pacman - Server Error)
    pacserv_stop
    return
  }
  if ($hget(pacserv,dual)) pacserv_findip
  else pacserv_findip $iif($hget(pacserv,ipv6),6,4)
  hadd -m pacserv udpport $2
  hadd pacserv tcpport $1
  if ($3 != $null) hadd pacserv password $md5($3-)
  hadd pacserv tlives 1
  hadd pacserv users 0
  hadd pacserv key $md5($ticksqpc)
  hadd pacserv ukey $md5($ctime $+ $ticksqpc)
  if (!$did(pacman,45).state) { .timer -ho 1 0 noop $!pacclient_start($iif($hget(pacserv,ipv6),::1,127.0.0.1), $unsafe($1) , $unsafe($3-) , $!hget(pacserv,key) ).d }
  if ($dialog(pacman)) dialog -t pacman Pacman $pac_version - A server is running
  noop $findfile($scriptdirmaps,*.plvl,0,did -a pacman 26 $left($nopath($1-),-5) [ $(|) ] did -a pacman 6 $1-)
  if ($show) && ($sock(pac_lobby).status == active) { .timerpacservspam -o 0 30 pacservspam | .timer 1 0 pacservspam }
}

; UPDATE SERVER INFOS
alias pacservspam  {
  if ($dialog(pacman)) {
    var %a $+($ip,:,$hget(pacserv,tcpport)), %b $+($replace($did(pacman,50),$chr(32),_),$str($chr(160),35) $chr(32),-,%a,-,$iif($hget(pacserv,users) > 1,$v1 users,$v1 user) ,-, $iif($hget(pacserv,password) != $null,•••,no),-,$iif($hget(pacserv,owner),managed by: $v1 $+ -),$hget(pacserv,ukey))
    if ($sock(pac_lobby).status == active) sockwrite -n pac_lobby privmsg $pl_chan $+(:,$chr(1),pacgame %b,$chr(1))
  }
}

; STOP SERVER
alias pacserv_stop {
  if ($hget(pacserv)) && (!$timer(pacstop)) {
    sockclose pacuserv
    sockclose pacservip
    if ($hget(pacserv,upnpudp)) noop $upnp_openipv4port($hget(pacserv,updport),0)
    swriting end
    .timerpacstop 1 1 sockclose pacserv?* $(|) hfree -w pacserv* $(|) if ($dialog(pacman)) $({,0) dialog -t pacman Pacman $pac_version - No server is running $(|) did -ra pacman 38 Start  $(},0)
    sockclose pacserv
    .timerpacserv* off
    ;astar_unload
    jsAStarCleanup
    if (!$did(pacman,39).state) && ($sock(pac_lobby).status == active) && ($show) sockwrite -n pac_lobby privmsg $pl_chan $+($chr(1),pacdead $hget(pacserv,ukey),$chr(1)) 
    did -e pacman 36,37,39,45,23,40,50,79,12,11,73
    did -h pacman 98,101,164
    did -b pacman 41,43
  }
}

; LISTENING
on *:socklisten:pacserv:{
  var %s pacservuc $+ $calc($hget(pacserv,connexion) + 1)
  sockaccept %s
  if ($istok($hget(pacserv,banlist),$sock(%s).ip,32)) {
    sockwrite -n %s ban
    .timer -o 1 1 sockclose %s
  }
  else hinc pacserv connexion
}

; READING TCP
on $*:sockread:/pacservu(c|p|n)\d+/:{
  var %n $regml(1)
  if ($sockerr) pacserv_quit
  elseif (%n == c) pacserv_login
  elseif (%n == n) pacserv_users
  else pacserv_gamer
}

on *:sockwrite:pacuserv:if ($sockerr) && ($window(@pac_debug)) aline @pac_debug $timestamp SERVER UDP SOCKWRITE $sockerr -- $sock($sockname).wsmsg

on *:sockclose:pacuserv:{
  echo -s sockclose pacuserv $sockerr
  if ($window(@pac_debug)) aline @pac_debug $timestamp SERVER UDP SOCKCLOSE $sockerr -- $sock(pacuserv).wsmsg | return
}

; READING UDP
on *:udpread:pacuserv:{
  set -nl %id $+($sock(pacuserv).saddr,/,$sock(pacuserv).sport)
  if ($sockerr) { echo 4 -sg udpread error $sockerr | return }
  sockread -n &a
  if (!$sockbr) return
  if ($bvar(&a,0)) {
    tokenize 32 $bvar(&a,1-128).text
    if ($1 == move) && ($hget(pacserv,game)) && (!$timer(pacservloop).pause) && ($2 == $hget(pacserv,keypl)) && ($hfind(pacservp,$3).data) {
      if ($v1 != %id) {
        hdel pacservp $v1
        hadd pacservp $v2 $3
        hadd pacserv udpsend $reptok($hget(pacserv,udpsend),$v1,$v2,32)
        ;echo -sg SOURCE PORT CHANGED old: $v1 new: %id
      }
      pacuserv_move $3 $4
    }
    elseif ($1 == pready) && ($hget(pacserv,ready)) && (!$hget(pacserv,game)) && (!$hget(pacserv,playing)) && ($regex($hget(pacserv,nicks),/[@~] $+ $2(?: |$)/)) && ($3 == $hget(pacserv,keyp)) pacuserv_pready %id $2
    elseif ($1 == spready) && ($hget(pacserv,ready)) && (!$hget(pacserv,game)) && (!$hget(pacserv,playing)) && ($regex($hget(pacserv,nicks),/(?:^| ) $+ $2(?: |$)/)) && ($3 == $hget(pacserv,keyp)) pacuserv_spready %id $2
  }
}

; QUIT
on $*:sockclose:/pacservu(c|p|n)\d+/:pacserv_quit

; HANDLING PLAYERS
alias -l pacserv_gamer {
  sockread -n &a
  if ($window(@pac_debug)) aline @pac_debug $timestamp SERVER TCP RCVD from $sockname ( $sock($sockname).mark ) : $bvar(&a,1-512).text
  if ($bvar(&a,0)) {
    var %s $sock($sockname).mark
    tokenize 32 $bvar(&a,1-512).text
    if ($1 == pnready) && ($regex($hget(pacserv,nicks),/[@~] $+ %s $+ (?: |$)/)) && ($hget(pacserv,ready)) && (!$hget(pacserv,game)) && (!$hget(pacserv,playing)) pacserv_pnready %s
    elseif ($1 == spnready) && ($regex($hget(pacserv,nicks),/(?:^| ) $+ %s $+ (?: |$)/)) pacserv_snready %s
    elseif ($1 == sready) && ($hget(pacserv,ready)) && (!$hget(pacserv,playing)) && (!$hget(pacserv,game)) && ($hget(pacserv,owner) == %s) pacserv_sready $2
    elseif ($1 == gsready) && ($hget(pacserv,playing)) && (!$regex($hget(pacserv,nicks),/[@~] $+ %s $+ (?: |$)/)) pacserv_gsready %s
    elseif ($1 == txt) pacserv_txt %s $2-
    elseif ($1 == gready) && ($regex($hget(pacserv,nicks),/[@~] $+ %s $+ (?: |$)/)) && ($hget(pacserv,playing)) pacserv_gready %s
    elseif ($1 == quitgame) pacserv_quitgame %s
    elseif ($1 == taunt) swriting taunt $hfind(pacservi,& & & & & & %s & & & &,1,w).data $2-
    elseif ($1 == pause) && ($hget(pacserv,game)) && (%s == $hget(pacserv,owner)) && (!$timer(pacservsoon)) { pacserv_pause | swriting pause }
    elseif ($1 == pause) { pacserv_pause | swriting pause  }
  }
}

; HANDLING NORMAL USERS 
alias -l pacserv_users {
  sockread -n &a
  if ($window(@pac_debug)) aline @pac_debug $timestamp  SERVER TCP RCVD from $sockname ( $sock($sockname).mark ) : $bvar(&a,1-512).text
  if ($bvar(&a,0)) {
    var %s $sock($sockname).mark
    tokenize 32 $bvar(&a,1-512).text
    if ($1 == txt) pacserv_txt %s $2-
    elseif ($1 == maps) && (!$hget(pacserv,game)) && ($hget(pacserv,owner) == %s) pacserv_maps $2-
    elseif ($1 == lives) && (!$hget(pacserv,game)) && ($hget(pacserv,owner) == %s) pacserv_lives $iif($2 isnum 1-99,$2)
    elseif ($1 == player) && (!$hget(pacserv,game)) && ($hget(pacserv,owner) == %s) pacserv_player $2-
    elseif ($1 == cpu) && ($hget(pacserv,owner) == %s) { 
      if ($2 isnum 0- $+ $calc(6 - $count($hget(pacserv,nicks),~))) hadd pacserv cpu $2
      else hdel pacserv cpu 
      swriting cpu $hget(pacserv,cpu)
    }
    elseif ($1 == sready) && (!$hget(pacserv,game)) && ($hget(pacserv,owner) == %s) pacserv_sready $2
    elseif ($1 == kick) && ($hget(pacserv,owner) == %s) {
      swriting kick $2
      .timer 1 2 sockclose $gsockname($2) 
      sockmark $gsockname($2) kick 
      hadd pacserv nicks $regsubex($hget(pacserv,nicks),/(?:^|[@~]|( )) $+ $2(?:( )|$)/,\1\2)
      hdec pacserv users 
    }
    elseif ($1 == ban) && ($hget(pacserv,owner) == %s) {
      swriting ban $2
      .timer 1 2 sockclose $gsockname($2)
      hadd pacserv banlist $addtok($hget(pacserv,banlist),$sock($gsockname($2)).ip,32)
      sockmark $gsockname($2) ban
      hadd pacserv nicks $regsubex($hget(pacserv,nicks),/(?:^|[@~]|( )) $+ $2(?:( )|$)/,\1\2)
      hdec pacserv users 
    }
  }
}

; HANDLING USERS THAT NEED TO LOG IN
alias -l pacserv_login {
  sockread -n &a
  if ($bvar(&a,0)) {
    if ($window(@pac_debug)) aline @pac_debug $timestamp SERVER (login) $bvar(&a,1-96).text
    if ($bvar(&a,0) > 96) sockwrite -n $sockname error nick invalid login communication
    elseif (!$v1) sockclose $sockname
    elseif ($istok($hget(pacserv,banlist),$sock($sockname).ip,32)) return
    elseif ($regex(check,$bvar(&a,1-96).text,/^login ([^@]+)@([^@]*)@(.*)$/)) {
      if ($regex($regml(check,1),/([^-a-z\d])|(^\d+$|^kick$|^cpu)/gi)) sockwrite -n $sockname error nick illegal
      elseif ($len($regml(check,1)) > 9) sockwrite -n $sockname error nick too long
      elseif ($regex($hget(pacserv,nicks),/(?:^|[@~]| ) $+ $regml(check,1) $+ ( |$)/i)) sockwrite -n $sockname error nick already in use
      elseif ($hget(pacserv,password) != $null) && ($v1 != $md5($regml(check,2))) sockwrite -n $sockname error pass
      else {
        hinc pacserv users
        var %sock $replace($sockname,pacservuc,pacservun),%r $regml(check,1)
        swriting connection %r
        sockrename $sockname %sock
        sockmark %sock %r
        hadd pacserv nicks $hget(pacserv,nicks) %r
        sockwrite -n %sock hello
        if ($regml(check,3) == $hget(pacserv,key)) && (!$hget(pacserv,owner)) { did -b pacman 41 | hadd pacserv owner %r | sockwrite -n %sock owner }
        if ($hget(pacserv,cpu)) sockwrite -n %sock cpu $v1
        sockwrite -n %sock nicks $hget(pacserv,nicks)
        sockwrite -n %sock paclives $hget(pacserv,tlives)
        sockwrite -n %sock maps $regsubex($hget(pacserv,maps),/([^\x04]+)/g,$left($nopath(\1),-5))
        if ($hget(pacserv,game)) sockwrite -n %sock gaming
        elseif ($hget(pacserv,ready)) {
          sockwrite -n %sock sready 1
          sockwrite -n %sock play $hget(pacserv,udpport) $hget(pacserv,keyp)
        }
      }
    }
    elseif ($regex(check,$bvar(&a,1-96).text,/^mapping (\S+) (\S+)$/)) && ($hget(pacserv,keyp) != $null) && ($hget(pacserv,playing)) && ($regml(check,1) == $hget(pacserv,keyp)) {
      var %a 1
      while ($gettok($hget(pacserv,maps),%a,4)) {
        var %v $v1
        if ($md5(%v,2) == $regml(check,2)) {
          var %r $+($left($sockname,8),s,$mid($sockname,10))
          sockrename $sockname %r
          .fopen %r $qt(%v)
          var %n $fread(%r, 16384,&r) 
          sockwrite -b %r %n &r
          return
        }
        inc %a
      }
      sockclose $sockname
    }
    else sockclose $sockname
  }
}

; SENDING MAP 
on *:sockwrite:pacservus*:{
  if ($sock($sockname).sq == 0) {
    var %n $fread($sockname, 16384,&r)
    if (!$fopen($sockname).eof) && (!$fopen($sockname).err) && (%n) { sockwrite -b $sockname %n &r }
    else { .fclose $sockname | sockclose $sockname }
  }
}

; CHATTING - TCP
alias -l pacserv_txt {
  swriting txt $1 $$2-
  if ($1- == $hget(pacserv,owner) !abort) && (($hget(pacserv,game)) || ($hget(pacserv,playing))) {
    var %a 1,%v
    while ($gettok($hget(pacserv,nicks),%a,32)) {
      %v = $v1
      if (@ isin %v) || (~ isin %v) { pacserv_quitgame $remove(%v,@,~) }
      inc %a
    }
  }
  else {
    var %p /(?:^!setmap |(?!^)\G)(:\d+|[^\/]+)(?:\/|$)/g
    if ($regex($2-,%p)) && ($hget(pacserv,owner) == $sock($sockname).mark) {
      var %a 1,%v,%m,%maps
      while ($regml(%a) != $null) {
        var %v = $v1
        if (:* iswm %v) && ($mid(%v,2) isnum 1 - $did(pacman,26).lines) %m = $v1
        elseif ($didwm(pacman,26,%v)) %m = $v1
        if (%m) {
          var %maps = $+(%maps,$chr(4),$did(pacman,6,%m))
        }
        else swriting mapseterr %v
        inc %a
      }
      if (%maps) pacserv_maps $v1
    }
  }
}

; GAME IS READY - TCP
alias -l pacserv_gready {
  if (!$istok($hget(pacserv,gready),$1,32)) hadd pacserv gready $hget(pacserv,gready) $1
  if ($numtok($hget(pacserv,gready),32) == $hget(pacservp,0).item) {
    pacserv_go
    if ($numtok($hget(pacserv,gsready),32) == $hget(pacservs,0).item) hdel pacserv playing
  }
}

alias -l pacserv_gsready {
  if (!$istok($hget(pacserv,gsready),$1,32)) hadd pacserv gsready $hget(pacserv,gsready) $1
  if ($numtok($hget(pacserv,gsready),32) == $hget(pacservs,0).item) {
    if ($numtok($hget(pacserv,gready),32) == $hget(pacservp,0).item) hdel pacserv playing
  }
}

; GO - TCP
alias -l pacserv_go {
  if (!$hget(pacservs,0).item) hdel pacserv playing
  hadd pacserv game 1
  swriting soon 3
  .timerpacservsoon -ho 3 1000 pacserv_soon
}

; SOON - TCP
alias pacserv_soon {
  if ($timer(pacservsoon).reps != 0) swriting soon $v1
  else { 
    swriting startg 
    var %a 1
    hdel pacserv pl
    while $hget(pacservi,%a).item {
      hadd pacserv pl $hget(pacserv,pl) $v1
      inc %a
    }
    tokenize 32 $sizesprite $hget(pacservg,size)
    set -l %xm $3 / $1
    set -l %ym $4 / $2
    pacserv_loop $1-2 %xm %ym
  }
}

; PLAYER IS NOT READY - TCP
alias -l pacserv_pnready {
  if ($hfind(pacservp,/^ $+ $1$/,1,r).data) {
    var %v $v1
    hdel pacservp %v
    swriting selnick $regsubex($str(a,$hget(pacservp,0).item),/(a)/g,$hget(pacservp,\n).data $+ $chr(32)) $regsubex($str(a,$hget(pacservs,0).item),/(a)/g,$hget(pacservs,\n).data $+ $chr(32))
    hadd pacserv udpsend $remtok($hget(pacserv,udpsend),%v,32)
    var %g $gsockname($1)
    sockwrite -n %g pnreadyok
    sockrename %g pacservun $+ $mid(%g,10)
  }
}

; QUIT GAME - TCP
alias -l pacserv_quitgame {
  if ($hget(pacserv,game)) || ($hget(pacserv,playing)) {
    var %ok
    if ($hfind(pacservp,$1).data) {
      hdel pacservp $v1
      %ok = 1
    }
    elseif ($hfind(pacservs,$1).data) hdel pacservs $v1
    else return
    hadd pacserv udpsend $remtok($hget(pacserv,udpsend),$v1,32)
    var %g $gsockname($1)
    sockrename %g pacservun $+ $mid(%g,10)
    hadd pacserv nicks $regsubex($hget(pacserv,nicks),/(?:^|[@~]|( ))( $+ $1)(?:( )|$)/,\1\2\3)
    swriting nicks $hget(pacserv,nicks)
    if (%ok) {
      var %hf $hfind(pacservi,& & & & & & $1 & & & &,1,w).data
      if (%hf) {
        hdel pacservi %hf
        hadd pacserv pl $remtok($hget(pacserv,pl),%hf,32)
      }
      if ($hget(pacservp,0).item <= 1) { 
        ;??? upck should not be sent on this if
        sockwrite -n pacservun $+ $mid(%g,10) upck $hget(pacserv,upck) 
        pacserv_end 
        pacserv_finish 4
      }
    }
    swriting selnick $regsubex($str(a,$hget(pacservp,0).item),/(a)/g,$hget(pacservp,\n).data $+ $chr(32)) $regsubex($str(a,$hget(pacservs,0).item),/(a)/g,$hget(pacservs,\n).data $+ $chr(32))
  }
  if ($window(@pac_debug)) aline @pac_debug $timestamp quitgame -- $hget(pacserv,game) -- udpsend: $hget(pacserv,udpsend)
}



; DECONNECTION - TCP
alias -l pacserv_quit {
  var %s $sock($sockname).mark
  sockclose $iif($sockname,$v1,$1)
  if ($istok(kick ban,%s,32)) return
  if ($regex($hget(pacserv,nicks),/[~@] $+ %s $+ (?: |$)/)) {
    if (!$hget(pacserv,game)) && ($hget(pacserv,ready)) pacserv_sready 0
    elseif ($hget(pacserv,game)) || ($hget(pacserv,playing)) {
      var %h $hfind(pacservi,& & & & & & %s & & & &,1,w).data
      if ($hfind(pacservp,%s).data) hdel pacservp $v1
      if (%h) {
        swriting part %h
        hdel pacservi %h
        hadd pacserv pl $remtok($hget(pacserv,pl),%h,32)
      }
      if ($hget(pacservp,0).item <= 1) { pacserv_end | pacserv_finish 4 }
    }
  }
  elseif ($hfind(pacservs,%s).data) hdel pacservs $v1
  if (%s) {
    hadd pacserv nicks $regsubex($hget(pacserv,nicks),/(?:^|[@~]|( )) $+ %s $+ (?:( )|$)/,\1\2)
    swriting deconnection %s $hget(pacserv,nicks)
    hdec pacserv users
  }
  if ($hget(pacserv,owner)) && ($v1 == %s) {
    hdel pacserv owner
    if (!$hget(pacserv,game)) pacserv_sready 0
    if ($dialog(pacman)) did -e pacman 40
  }
  swriting selnick $regsubex($str(a,$hget(pacservp,0).item),/(a)/g,$hget(pacservp,\n).data $+ $chr(32)) $regsubex($str(a,$hget(pacservs,0).item),/(a)/g,$hget(pacservs,\n).data $+ $chr(32))
}



; SPECTATOR NOT WATCHING - TCP
alias -l pacserv_snready {
  if ($hfind(pacservs,/^ $+ $1$/,1,r).data) {
    var %v $v1
    hdel pacservs %v
    hadd pacserv udpsend $remtok($hget(pacserv,udpsend),%v,32)
    swriting selnick  $regsubex($str(a,$hget(pacservp,0).item),/(a)/g,$hget(pacservp,\n).data $+ $chr(32)) $regsubex($str(a,$hget(pacservs,0).item),/(a)/g,$hget(pacservs,\n).data $+ $chr(32))
    var %g $gsockname($1)
    sockwrite -n %g snreadyok
    sockrename %g pacservun $+ $mid(%g,10)
  }
}

; SPECTATOR IS READY - UDP
alias -l pacuserv_spready {
  if ($hfind(pacservs,$2).data) return
  hadd -m pacservs $1 $2
  swriting selnick $regsubex($str(a,$hget(pacservp,0).item),/(a)/g,$hget(pacservp,\n).data $+ $chr(32)) $regsubex($str(a,$hget(pacservs,0).item),/(a)/g,$hget(pacservs,\n).data $+ $chr(32))
  var %g $gsockname($2)
  sockwrite -n %g preadyok
  sockrename %g pacservup $+ $mid(%g,10)
  hadd pacserv udpsend $hget(pacserv,udpsend) $1

}

; PLAYER IS READY - UDP
alias -l pacuserv_pready {
  if ($hfind(pacservp,$2).data) return
  hadd -m pacservp $1 $2
  hadd pacserv udpsend $hget(pacserv,udpsend) $1
  var %g $gsockname($2)
  sockwrite -n %g preadyok
  sockwrite -n %g keypl $hget(pacserv,keypl)
  sockrename %g pacservup $+ $mid(%g,10)
  swriting selnick $regsubex($str(a,$hget(pacservp,0).item),/(a)/g,$hget(pacservp,\n).data $+ $chr(32)) $regsubex($str(a,$hget(pacservs,0).item),/(a)/g,$hget(pacservs,\n).data $+ $chr(32))
  if ($hget(pacservp,0).item == $regex($hget(pacserv,nicks),/[@~]/g)) pacserv_startgame
}

; GET SOCKNAME FROM NICKNAME
alias -l gsockname {
  var %a 1
  while ($sock(pacservu*,%a).mark) {
    ;  echo -s >>> $sock(pacservu*,%a) -- $v1 --- $1
    if ($v1 == $1) return $sock(pacservu*,%a)
    inc %a
  }
}

; CHANGING PLAYERS - TCP
alias -l pacserv_player {
  var %n $1,%s $2
  hadd pacserv nicks $regsubex($hget(pacserv,nicks),/(?:^|[@~]|( )) $+ $1(?:( )|$)/,$+(\1,$replace(%s,pacman,@,ghost,~),%n,\1\2))
  if ($calc(6 - $count($hget(pacserv,nicks),~) - $hget(pacserv,cpu)) <= 0) {
    sockwrite -n $sockname cpu $iif($calc(6 - $count($hget(pacserv,nicks),~)) > 0,$v1)
    hadd pacserv cpu $iif($calc(6 - $count($hget(pacserv,nicks),~)) > 0,$v1)
  }
  swriting nicks $hget(pacserv,nicks)
}

; SERVER IS READY - TCP
alias pacserv_sready {
  if ($1) {
    if (!$hget(pacserv,tlives)) || (!$hget(pacserv,maps)) || ($regex($hget(pacserv,nicks),/[~@]/g) < 1) sockwrite -n $sockname eready 1
    else {
      if ($hget(pacserv,ipv6)) sockudp -dk $+ $iif($hget(pacserv,dual),u) :: pacuserv $hget(pacserv,udpport)
      if ($did(pacman,73).state) && (($hget(pacserv,dual)) || (!$hget(pacserv,ipv6))) {
        noop $upnp_openipv4port($hget(pacserv,udpport),$true)
        hadd pacserv upnpudp 1
      }
      sockudp -dk 0.0.0.0 pacuserv $hget(pacserv,udpport)
      swriting sready 1
      hadd pacserv keyp $md5($calc($ticksqpc / $ctime))
      hadd pacserv keypl $md5($calc($ctime / $ticksqpc))
      sockwrite -n pacservu* play $hget(pacserv,udpport) $hget(pacserv,keyp)
      hadd pacserv ready 1
      if ($window(@pac_debug)) aline @pac_debug $timestamp SERVER IS READY udpsend: $hget(pacserv,udpsend)
      :error
      if ($error) {
        var %e $v1
        reseterror
        echo -sg * Pacman error tcp server is ready: %e
      }
    }
  }
  elseif (!$hget(pacserv,game)) && (!$hget(pacserv,playing)) {
    swriting selnick 
    sockclose pacuserv
    hdel pacserv keyp
    hdel pacserv ready
    hdel pacserv udpsend
    hfree -w pacservp
    hfree -w pacservs
    var %a 1
    while ($sock(pacservu*,%a)) {
      if ($v1 != pacservun $+ $mid($v1,10)) sockrename $v1 $v2
      inc %a
    }
    swriting sready 0
    if ($getsockowner) sockwrite -n $v1 eready
  }
  elseif ($getsockowner) sockwrite -n $v1 eready 2
}

; MAPS - TCP
alias -l pacserv_maps {
  hadd pacserv maps $1-
  swriting maps $regsubex($1-,/([^\x04]+)/g,$left($nopath(\1),-5))
}

; LIVES - TCP
alias -l pacserv_lives hadd pacserv tlives $1 | swriting paclives $1

; GETOWNER
alias -l getsockowner {
  var %a 1
  while ($sock(pacservu*,%a).mark) {
    if ($hget(pacserv,owner) == $v1) return $sock(pacservu*,%a)
    inc %a
  }
}

; SENDING
alias swriting {
  if ($sock(pacservup*,0)) sockwrite -n pacservup* $1-
  if ($sock(pacservun*,0)) && (!$istok(mapping soon posd lives startg +1 pacwin paclose dead fraise fraise1 grenade apple banana orange eatbar hspeed lspeed toclear part busyport pladd stunt invinc cli shield pldel upck,$1,32)) sockwrite -n pacservun* $1-
}

; STARTING GAME - TCP
alias -l pacserv_startgame {
  swriting game start
  hadd pacserv nmaps 1
  hmake pacservg
  hadd pacserv lives $hget(pacserv,tlives)
  pacserv_mapping
}  

; SENDING MAP - INIT PLAYERS
alias -l pacserv_mapping {
  hdel -w pacserv dead
  hdel -w pacserv gready
  hdel -w pacserv invinc
  hdel -w pacserv toclear
  hdel -w pacserv +1
  hdel -w pacserv sbonus
  var %f $gettok($hget(pacserv,maps),$hget(pacserv,nmaps),4)
  if (!%f) { pacserv_finish $iif(!$hget(pacserv,maperror),1,2) $1 | hdel -w pacserv maperror | return }
  hdel -w pacserv maperror
  hdel -w pacservg *
  if ($isfile(%f)) hload pacservg $qt(%f)
  if (!$hget(pacservg,pacman)) {
    echo -s * Pacman map error, trying next map
    hinc pacserv nmaps 
    hadd pacserv maperror 1 
    .timer -ho 1 0 pacserv_mapping 
    return
  }
  hadd pacserv playing 1
  hadd pacserv score 0
  hdel -w pacserv eat*
  hdel -w pacserv dead*
  hadd pacserv bonusi 0
  hadd pacserv gsspeed 3
  hadd pacserv gscount 0
  swriting mapping $lof(%f) $md5(%f,2) $hget(pacservg,size) $nopath(%f)
  swriting lives $hget(pacserv,lives)
  tokenize 32 $sizesprite
  if ($hget(pacservi)) hdel -w pacservi *
  else hmake pacservi
  var %spawn $hget(pacservg,spawn)
  pacserv_initpos $1-
  if (~ isin $hget(pacserv,nicks)) || ($hget(pacserv,cpu)) {
    ; astar_load
    maptobvar pacservg
    noop $jsAStarLoadMap(&map)
    var %a 1,%p 1,%s $hget(pacserv,cpu)
    while (%s) {
      if (!$hget(pacservi,pacman $+ %p)) %p = 1
      astar_go ghost $+ %a pacman $+ %p
      inc %a
      inc %p
      dec %s
    }
  }
  if (%spawn) {
    tokenize 32 $replace($v1,.,$chr(32)) $1-
    if ($1 == d) {
      hadd pacserv isinspawn $+($calc($2 * $6),$chr(44),$calc(($3 -3) * $7),$chr(44),70,$chr(44),42)
      hadd pacservg $+($calc($2 +1),.,$calc($3 -1)) wf
      hadd pacservg $+($calc($2 +2),.,$calc($3 -1)) wf
      hadd pacservg $+($calc($2 +3),.,$calc($3 -1)) wf
    }
    elseif ($1 == r) {
      hadd pacserv isinspawn $+($calc(($2 - 3) * $6),$chr(44),$calc($3 * $7),$chr(44),42,$chr(44),70)
      hadd pacservg $+($calc($2 -1),.,$calc($3 +1)) wf
      hadd pacservg $+($calc($2 -1),.,$calc($3 +2)) wf
      hadd pacservg $+($calc($2 -1),.,$calc($3 +3)) wf
    }
    elseif ($1 == l) {
      hadd pacserv isinspawn $+($calc($2 * $6),$chr(44),$calc($3 * $7),$chr(44),42,$chr(44),70)
      hadd pacservg $2. $+ $calc($3 +1) wf
      hadd pacservg $2. $+ $calc($3 +2) wf
      hadd pacservg $2. $+ $calc($3 +3) wf
    }
    else {
      hadd pacserv isinspawn $+($calc($2 * $6),$chr(44),$calc($3 * $7),$chr(44),70,$chr(44),42)
      hadd pacservg $+($calc($2 +1),.,$3) wf
      hadd pacservg $+($calc($2 +2),.,$3) wf
      hadd pacservg $+($calc($2 +3),.,$3) wf
    }
  }
  hadd pacserv boules $hfind(pacservg,?coin*,0,w).data
}

; INIT POS
alias -l pacserv_initpos {
  var %a $hget(pacserv,cpu),%g 0,%p 0,%s %a
  while (%a) var %ni %ni ghost,%a %a - 1
  %a = 1
  while ($hget(pacservp,%a).data) && ($regex($hget(pacserv,nicks),/([@~]) $+ $v1 $+ (?: |$)/)) var %ni %ni $replace($regml(1),@,pacman,~,ghost),%a %a + 1
  %a = 1
  while ($gettok(%ni,%a,32)) {
    var %i $v1 $+ $calc($iif($v1 == pacman,%p,%g) +1) ,%t $iif($v1 == pacman,$v2,$v1 $+ $calc(%g +1))
    inc % $+ $iif($v1 == pacman,p,g)
    ;01 = X    ;02 = Y    ;03 = CURRENT DIR    ;04 = WANTED DIR    ;05 = CAN BE EATEN (ghost only) -2 = killed by mirc -1 = ghost waiting for position not in external teleporter    ;06 = COORDINATE IF EATING AN ITEM OR 1/0 IF MOVING    ;07 = NICKNAME    ;08 = # PILLS COLLECTED    ;09 = SCORE    ;10 = OLD X    ;11 = OLD Y
    var %x $gettok($hget(pacservg,%t),1,32) * $1,%y $gettok($hget(pacservg,%t),2,32) * $2
    hadd pacservi %i %x %y 0 0 0 0 $iif(!%s || %a > %s,$hget(pacservp,$calc(%a - %s)).data,cpu) 0 0 %x %y
    swriting posd %i $hget(pacservi,%i)
    inc %a
  }
}

; HANDLING PLAYER INPUT
alias -l pacuserv_movenew {
  var %h $$hfind(pacservi,& & & & & & $1 & *,1,w).data,%a $2
  if !$hget(pacserv,as_ $+ %h) {
    tokenize 32 $hget(pacservi,%h)
    echo -s > in ( %a ) $1-
    if (!%a) hadd pacservi %h $1-2 0 0 $5-
    else hadd pacservi %h $1-3 %a $5-
    tokenize 32 $hget(pacservi,%h)
    echo -s > out $1-

  }
}

alias -l pacuserv_move {
  var %h $$hfind(pacservi,& & & & & & $1 & *,1,w).data
  if !$hget(pacserv,as_ $+ %h) {
    tokenize 32 $$2 $hget(pacservi,%h)
    hadd pacservi %h $2-4 $1 $6-
  }
}

alias -l pacserv_eat {
  var %r $2,%v $1
  tokenize 32 $hget(pacservi,%v)
  if (%r) && (!$inrect($1,$2, [ $hget(pacserv,isinspawn) ] )) && ($4 != -1) && (!$istok($hget(pacserv,shield),%v,32)) hadd pacservi %v $1-4 1 $6-
  else hadd pacservi %v $1-4 0 $6-
}

; CHANGING STATE
alias -l pacserv_mange {
  hadd pacserv mange $1
  set -l %r $1
  noop $hfind(pacservi,ghost*,0,w,pacserv_eat $1 %r)
}

; END
alias -l pacserv_end {
  if ($timer(pacservspam)) .timer -ho 1 0 .timerpacservspam 0 30 pacservspam
  .timerpacserv* off
  pacserv_mange 0
  var %hdel invinc stunt invis pl kmode shield kmt kml rplshoot
  hdel -w pacserv letter*
  hdel -w pacserv pellet*
  hdel -w pacserv surround*
  if ($istok(win lose,$1,32)) swriting pac $+ $1
  if ($1 == win) {
    hinc pacserv nmaps
    %hdel = %hdel pacend
    hinc pacserv lives
    hdel -w pacserv lspeed*
    hdel -w pacserv hspeed*
    .timerpacservstart -o 1 2 pacserv_mapping $!hget(pacserv,sbonus)
  }
  elseif ($1 == lose) {
    %hdel = %hdel pacend
    .timer -ho 1 0 pacserv_finish
  }
  swriting upck $hget(pacserv,upck)
  %hdel = %hdel upck
  tokenize 32 %hdel 
  hdel pacserv $*
}
alias pacserv_finish {
  ;$1 = $null = paclose
  ;$1 == 2 = maperror
  ;$1 == 1 = pacwin
  ;$1 == 3 == gamerror
  ;$1 == 4 == toofewplayer
  .timerspacservoon off
  var %hdel = %hdel game playing boules gready gsready isinspawn +1,%score $hget(pacserv,sbonus)
  hfree -w pacservi
  hdel -w pacserv dead
  hdel -w pacserv mange
  hdel -w pacserv as_*
  hdel -w pacserv hspeed*
  hdel -w pacserv lspeed*
  hdel -w pacserv sbonus
  ;astar_unload
  jsAStarCleanup
  %hdel = %hdel nmaps
  hadd pacserv lives $hget(pacserv,tlives)
  hdel -w pacserv udpsend
  hfree -w pacserv?
  var %1 $1,%2 $2-
  tokenize 32 %hdel
  hdel pacserv $*
  pacserv_sready 0
  if (%1 == 2) .swriting game stop map error
  elseif (%1 == 3) swriting game stop game error %2
  else swriting game stop $iif(!%1,Ghost(s) won,$iif(%1 == 1,Pacman(s) won,Too few player)) %2
}

;CLOSEST PAC
alias pac_closest {
  set -l %1 $1
  var %pacs $regsubex($hget(pacserv,pl),/(pacman\d+)(?= |$)|\S+(?: |$)/Fg,\1)
  noop $hfind(pacserv,as_ghost*,0,w,set %pacs $remtok(%pacs,$gettok($hget(pacserv,$1),1,32),32))
  if (%pacs) && ($regex($hget(pacserv,pl),/ghost/g) > $regex($hget(pacserv,pl),/pacman/g)) set -l %p $gettok(%pacs,$r(1,$numtok(%pacs,32)),32)
  else {
    tokenize 32 $hget(pacservi, $1)
    %pi = $hget(pacserv, invis)
    set -l %h $hfind(pacservi, /^(?:pacman\d+|letter[mirc])/g, 0, r, pac_closest_check [ $1-2 ] $1)
    if (%pp) set -l %p $v1
    else set -l %p $gettok(%pl, $r(1, %h), 32)
    unset %pl %pi %pct %pp
  }
  hdel pacserv surround $+ %1
  if (pacman* iswm %p) && ($calc($pacserv_hmgonp(%p) / $pacserv_hmgonps(%p)) >= $calc($pacserv_hmgonp(%p) / 2)) || (($pacserv_hmgonps(%p) == 0) && ($pacserv_hmgonp(%p) > 1)) hadd pacserv surround $+ %1 %p
  return %p
}

alias pacserv_hmgonps {
  set -u %no  0
  set -l %1 $1
  noop $hfind(pacserv,as_ghost*,0,w,if ($hget(pacserv,surround $+ $mid($1,4)) == %1) inc % $+ no )
  return %no
}


alias pacserv_hmgonp {
  %paccalc = 0
  noop $hfind(pacserv,as_ghost*,0,w,pac_calc [ $1 ] $1)
  set -l %n %paccalc
  unset %paccalc
  return %n
}
alias -l pac_calc if ($gettok($hget(pacserv,$2),1,32) == $1) inc %paccalc

alias pac_closest_check {
  tokenize 32 $1-3 $hget(pacservi, $3)
  %pl = %pl $3
  if !$istok(%pi, $3, 32) && ((letter* !iswm $3) || ($pacserv_hmgonp($3) == 0)) {
    set -l %t $calc(($4 - $1)^2 + ($5 - $2)^2)
    if !%pct || %t < %pct {
      %pct = %t
      %pp = $3
    }
  }
}

; COLLISIONS

alias -l pacserv_collision1 {
  ;echo -a collisions? $1-
  set -l %xs $3
  set -l %ys $4
  set -l %2 $2
  set -u %pl $5
  set -u %gl $6
  tokenize 95 $1
  scon -r noop $!pacserv_collision1sub( $* , %2 , %xs , %ys )
  unset %colp %colg
  if (%exl) {
    unset %exl
    return 1
  }
}

alias pacserv_collision1sub {
  inc %colp
  set -l %pac $1
  set -l %xs $3
  set -l %ys $4
  tokenize 95 $2
  scon -r noop $!pacserv_collision1sub1( $* , %pac , %xs , %ys )
  unset %colg
}
alias pacserv_mircboss {
  set -l %x $calc($gettok($hget(pacservg,size),1,32) / 2 - 20)
  set -l %y 0
  hadd pacserv pl $hget(pacserv,pl) @mirc
  hadd pacservi @mirc %x %y 0 0 0 1 @mirc 0 0 %x %y
  swriting @mirc 1 90
  hadd pacserv kmode 1
  .timerpacservkmtl -o 1 90 swriting @mirc $(|) hadd pacserv pl $!remtok($hget(pacserv,pl),@mirc,32) $(|) hdel pacserv kmt $(|) hdel pacserv kml $(|) hdel pacserv kmode $(|) .timerkshoot off
}
alias pacserv_collision1sub1 {
  inc %colg
  set -ln %va $gettok(%pl,%colp,32)
  set -ln %vb $gettok(%gl,%colg,32)
  if (!%va) || ((letter* iswm %va) && (letter* iswm %vb)) || ($istok(%exl,%va,32)) || ($istok(%exl,%vb,32)) return
  tokenize 32 $1-4
  if ($calc($abs($calc(($1 + 1.5 + ($5 -3) /2) - ($3 + $5 /2))) *2) <= $calc($5 *2 -3)) && ($calc($abs($calc(($2 + 1.5 + ($6 -3) /2) - ($4 + $6 /2))) *2) <= $calc($6 * 2 -3)) {
    if (letter* iswm %va) {
      %exl = %exl %va
      hadd pacserv pl $remtok($hget(pacserv,pl),%va,32)
      hdel pacservi %va
      swriting pldel %va
      hdel pacserv as_ $+ %va
      hdec pacserv sbonus 50
      swriting score hdec 50
    }
    elseif (letter* iswm %vb) {
      %exl = %exl %vb
      hadd pacserv pl $remtok($hget(pacserv,pl),%vb,32)
      hdel pacservi %vb
      swriting pldel %vb
      hadd pacserv %vb %va
      hdel pacserv as_ $+ %vb
      swriting letter %vb %va
      hinc pacserv sbonus 50
      swriting score hinc 50
      hadd pacservi %va $puttok($hget(pacservi,%va),$calc($gettok($hget(pacservi,%va),9,32) + 50),9,32)
      if ($hget(pacserv,letterm)) && ($hget(pacserv,letteri)) && ($hget(pacserv,letterr)) && ($hget(pacserv,letterc)) pacserv_mircboss
    }
    elseif ($gettok($hget(pacservi,%vb),5,32)) {
      hinc pacserv sbonus 200
      swriting score hinc 200 %va
      hadd pacserv lspeedpac $remtok($hget(pacserv,lspeedpac),%vb,32)
      hadd pacserv stunt $remtok($hget(pacserv,stunt),%vb,32)
      hadd pacserv hspeedpac $remtok($hget(pacserv,hspeedpac),%vb,32)
      hadd pacserv shield $remtok($hget(pacserv,shield),%vb,32)
      hadd pacserv invis $remtok($hget(pacserv,invis),%vb,32)
      if ($timer(pacservflip $+ %va))  .timerpacservflip $+ %va -e
      .timerpacservflip $+ %vb off
      .timerpacservshield $+ %vb off
      .timerpacservlspeed $+ %vb off
      .timerpacservhspeed $+ %vb off
      .timerpacservhstunt $+ %vb off
      swriting shield $hget(pacserv,shield)
      swriting stunt $hget(pacserv,stunt)
      swriting invis $hget(pacserv,invis)
      set -ln %t $hget(pacservi,%va)
      hinc pacserv dead $+ %vb
      hinc pacserv eat $+ %va
      hadd pacservi %va $puttok(%t,$calc($gettok(%t,9,32) + 200),9,32)
      noop $astar_go(%vb,1)
      %exl = %exl %vb
    }
    ;otherwise if pacman is not invincible
    elseif (!$istok($hget(pacserv,invinc),%va,32)) {
      ;if pacman has a shield
      if ($istok($hget(pacserv,shield),%va,32)) {
        ;if ghost does not have a shield
        if (!$istok($hget(pacserv,shield),%vb,32)) {
          ;echo -sg pacman loses the shield
          hadd pacserv shield $remtok($hget(pacserv,shield),%va,32)
          .timerpacservshield $+ %va off
          hadd pacserv invinc $addtok($hget(pacserv,invinc),%va,32)
          swriting lshield %va     
          swriting shield $hget(pacserv,shield)
          hadd pacserv stunt $addtok($hget(pacserv,stunt),%vb,32)
          swriting stunt $hget(pacserv,stunt)
          .timerpacservstunt $+ %vb -ho 1 2000 hadd pacserv stunt $!remtok($hget(pacserv,stunt), %vb , 32) $(|) swriting stunt $!hget(pacserv,stunt)
          .timerpacservinvinc $+ %va -ho 1 2000 hadd pacserv invinc $!remtok($hget(pacserv,invinc), %va , 32) $(|) swriting invinc $!hget(pacserv,invinc)
          %exl = %exl %va %vb
        }
        else {
          ;double shield
        }
      }
      else {
        if ($hget(pacserv,lives) > 1) {
          ;   echo -sg $ticksqpc %va dies to %vb -- %exl
          if ($window(@pac_debug)) aline @pac_debug $timestamp serv %va eat %vb paclose
          %exl = %exl %va %vb
          hadd pacserv lspeedpac $remtok($hget(pacserv,lspeedpac),%va,32)
          hdel pacserv lspeed $+ %va
          hdel pacserv hspeed $+ %va
          hadd pacserv shield $remtok($hget(pacserv,shield),%vb,32)
          .timerpacservshield $+ %vb off
          .timerpacservlspeed $+ %va off
          .timerpacservhspeed $+ %va off
          if ($timer(pacservflip $+ %va))  .timerpacservflip $+ %va -e
          .timerpacservflip $+ %va off
          .timerpacservinvis $+ %va off
          hadd pacserv hspeedpac $remtok($hget(pacserv,hspeedpac),%va,32)
          set -ln %h $hget(pacservi,%va)
          set -ln %g $hget(pacservg,pacman)
          set -ln %p $calc($gettok(%g,1,32) * $5) $calc($gettok(%g,2,32) * $6)
          hadd pacservi %va %p $gettok(%h,3-4,32) 0 0 $gettok(%h,7-9,32) %p
          hadd pacserv invinc $addtok($hget(pacserv,invinc),%va,32)
          hadd pacserv invis $remtok($hget(pacserv,invis),%va,32)
          hadd pacserv stunt $addtok($hget(pacserv,stunt),%vb,32)
          swriting invinc $hget(pacserv,invinc)
          swriting shield $hget(pacserv,shield)
          swriting invis $hget(pacserv,invis)
          swriting stunt $hget(pacserv,stunt)
          .timerpacservinvinc $+ %va -ho 1 4000 hadd pacserv invinc $!remtok($hget(pacserv,invinc), %va , 32) $(|) swriting invinc $!hget(pacserv,invinc)
          .timerpacservstunt $+ %vb -ho 1 10000 hadd pacserv stunt $!remtok($hget(pacserv,stunt), %vb , 32) $(|) swriting stunt $!hget(pacserv,stunt)
          hinc pacserv eat $+ %vb
          hinc pacserv dead $+ %va
          hdec pacserv lives 
          swriting lives $hget(pacserv,lives)
          if ($hget(pacserv,as_ $+ %vb)) && ($pac_closest(%vb)) {
            hadd pacserv as_ $+ %vb $v1 1
            astar_go %vb $v1
          }
        }
        else {
          hadd pacserv lspeedpac $remtok($hget(pacserv,lspeedpac),%va,32)
          hdel pacserv lspeed $+ %va
          hdel pacserv hspeed $+ %va
          hadd pacserv shield $remtok($hget(pacserv,shield),%vb,32)          
          .timerpacservshield $+ %vb off
          .timerpacservlspeed $+ %va off
          .timerpacservhspeed $+ %va off
          if ($timer(pacservflip $+ %va))  .timerpacservflip $+ %va -e
          .timerpacservflip $+ %va off
          .timerpacservinvis $+ %va off
          hadd pacserv hspeedpac $remtok($hget(pacserv,hspeedpac),%va,32)
          hadd pacserv invis $remtok($hget(pacserv,invis),%va,32)
          hadd pacserv dead $hget(pacserv,dead) %va
          hadd pacserv pl $remtok($hget(pacserv,pl),%va,32)
          swriting lives 0
          hdel pacservi %va
          swriting dead %va
          hadd pacserv stunt $addtok($hget(pacserv,stunt),%vb,32)
          swriting stunt $hget(pacserv,stunt)
          swriting shield $hget(pacserv,shield)
          swriting invis $hget(pacserv,invis)
          .timerpacservstunt $+ %vb -ho 1 10000 hadd pacserv stunt $!remtok($hget(pacserv,stunt), %vb , 32) $(|) swriting stunt $!hget(pacserv,stunt)
          %exl = %exl %vb %va
          if ($hfind(pacservi,pacman*,0,w) == 0) hadd pacserv pacend lose
          else noop $hfind(pacserv,as_ghost*,0,w,pacserv_astar_redirect $1 %va)
        }
        pacserv_mange 1
        swriting cli 0
        var %tt 5
        if ($hget(pacserv,pellet+)) %tt = %tt * 1.5
        elseif ($hget(pacserv,pellet-)) %tt = %tt * 0.5
        var %s $calc($timer(pacservmange).secs + $int(%tt))
        swriting eatbar %s
        hadd pacservg %xy free
        .timerpacservmange -oh 1 $calc((%s - 3) * 1000) swriting cli 1 $(|) .timerpacservmange -oh 1 3000 pacserv_mange 0 $!(|) swriting cli 0
      }
    }
  }
}

alias pacserv_astar_redirect {
  if (letter* iswm $1) {
    hadd pacserv pl $remtok($hget(pacserv,pl),$v2,32)
    hdel pacservi $v2
    hdel pacserv as_ $+ $v2
    swriting pldel $v2
    return
  }
  if ($gettok($hget(pacserv,$1),1,32) == $2) {
    set -nl %h $pac_closest($mid($1,4))
    hadd pacserv $1 %h 1
    astar_go $mid($1,4) %h
  }
}
alias -l pacserv_addfruit {
  if ($hget(pacservg,$1) == free) {
    hadd pacservg $1-
    swriting $2 $1
  }
}
alias -l pacserv_add+1 {
  if ($hget(pacservg,$1) == free) {
    hadd pacservg $1 +1
    swriting +1 $1 1
  }
}
alias -l pacserv_del+1 {
  if ($hget(pacservg,$1) == +1) {
    hadd pacservg $1 free
    swriting +1 $1
  }
}
alias -l pacserv_handlep {
  set -l %cmd
  set -l %x $int($calc($3 / $1)) * $1
  if ($5 == 39) && ($1 \\ $3) inc %x $1
  set -l %y $int($calc($4 / $2)) * $2
  if ($5 == 40) && ($2 \\ $4) inc %y $2
  set -l %r $replace($5,37,$calc(%x + $1 - $3),39,$calc($3 + $1 - %x),38,$calc(%y + $2 - $4),40,$calc($4 + $2 - %y))
  set -l %s $replace($5,37,$1,39,$1,38,$2,40,$2) / 2
  set -l %x. %x / $1
  set -l %y. %y / $2
  set -l %xy $+(%x.,.,%y.)
  set -l %h $hget(pacservg,%xy)
  if (!%r) || (%r >= $calc($int(%s) + 2)) && ($regex(%h,/^(?!ghost|pacman|i?teleport|free|wf)./)) {
    set -l %v $hget(pacservi,$6)
    set -l %m $gettok(%v,8,32)
    set -l %k $gettok(%v,9,32)
    if (pacman* iswm $6) %cmd = hinc
    else %cmd = hdec
    if (%h == scoin) && (pacman* iswm $6) {
      inc %m
      inc %k 30
      hadd pacservi $6 $gettok(%v,1-7,32) %m %k $gettok(%v,10-,32)
      hinc pacserv sbonus 30
      swriting score hinc 30
      hdec pacserv boules
      hadd pacservg %xy free
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      set -ln %r $r(0,100)
      set -ln %r1 $r(0,100)
      if (%r == 100) && (%r1 > 95) && (!$hget(pacserv,+1))  {
        .timerpacserv+1 -o 1 10 pacserv_add+1 %xy
        hadd pacserv +1 1
        .timerpacserv+1 -o 1 17 pacserv_del+1 %xy
      }
      elseif (%r > 98) && (%r1 > 90) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy orange 
      elseif (%r > 96) && (%r1 > 90) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy banana
      elseif (%r > 94) && (%r1 > 90) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy apple
      elseif (%r > 92) && (%r1 > 90) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy grenade
      elseif (%r > 90) && (%r1 > 90) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy fraise1 
      elseif (%r > 88) && (%r1 > 90) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy fraise
      ; elseif (%r > 1) && (%r1 > 1) .timerpacservfruit $+ %xy -o 1 10 pacserv_addfruit %xy fraise
    }
    elseif (bcoin* iswm %h) && (pacman* iswm $6) {
      inc %m
      inc %k 100
      hadd pacservi $6 $gettok(%v,1-7,32) %m %k $gettok(%v,10-,32)
      hinc pacserv sbonus 100
      swriting score hinc 100
      hdec pacserv boules
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),$gettok(%h,1,32))
      pacserv_mange 1
      swriting cli 0
      set -ln %t 10
      if ($gettok(%h,2,32)) %t = $v1
      if ($hget(pacserv,pellet+)) %t = %t * 1.5
      elseif ($hget(pacserv,pellet-)) %t = %t * 0.5
      set -l %s $calc($timer(pacservmange).secs + $int(%t))
      swriting eatbar %s
      hadd pacservg %xy free
      .timerpacservmange -oh 1 $calc((%s - 3) * 1000) swriting cli 1 $(|) .timerpacservmange -oh 1 3000 pacserv_mange 0 $!(|) swriting cli 0
    }
    elseif (%h == +1) && (pacman* iswm $6) {
      .timerpacserv+1 off
      hinc pacserv sbonus 50
      swriting score hinc 50
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      hinc pacserv lives 
      hadd pacservg %xy free
      swriting lives $hget(pacserv,lives)
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
    }
    elseif (%h == fraise) {
      %cmd pacserv sbonus 200
      swriting score %cmd 200
      hadd pacservg %xy free
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 200),9,32)
    }
    elseif (%h == fraise1) {
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      swriting score %cmd 250
      %cmd pacserv sbonus 250
      hadd pacservg %xy free
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 250),9,32)
    }
    elseif (%h == grenade) {
      swriting score %cmd 300
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      %cmd pacserv sbonus 300
      hadd pacservg %xy free
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 300),9,32)
    }
    elseif (%h == apple) {
      swriting score %cmd 350
      %cmd pacserv sbonus 350
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacservg %xy free
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 350),9,32)
    }
    elseif (%h == banana) {
      swriting score  %cmd 400
      %cmd pacserv sbonus 400
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacservg %xy free
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 400),9,32)
    }
    elseif (%h == orange) {
      swriting score %cmd 500
      %cmd pacserv sbonus 500
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacservg %xy free
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 500),9,32)
    }
    elseif (%h == hspeed) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacserv hspeedpac $addtok($hget(pacserv,hspeedpac),$6,32)
      hadd pacserv lspeedpac $remtok($hget(pacserv,lspeedpac),$6,32)
      .timerpacservlspeed $+ $6 off
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      hadd pacservg %xy free
      .timerpacservegg $+ %xy off
      .timerpacservhspeed $+ $6 -ho 1 7000 hadd pacserv hspeedpac $!remtok($hget(pacserv,hspeedpac), $6 ,32)
    }
    elseif (%h == hspeedt) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      var %p $regsubex($6,\d+,)
      noop $hfind(pacservi,%p $+ *,0,w,if (%p != ghost) || ($gettok($hget(pacservi,$1),4,32) != -1) hadd pacserv lspeedpac $remtok($hget(pacserv,lspeedpac),$1,32))
      hdel pacserv lspeed_ $+ %p
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      hadd pacservg %xy free
      hadd pacserv hspeed_ $+ %p 1
      .timerpacservegg $+ %xy off
      .timerpacservhspeedt $+ %p -ho 1 7000 hdel pacserv hspeed_ $+ %p
    }
    elseif (%h == lspeedt) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      var %p $regsubex($6,\d+,)
      noop $hfind(pacservi,%p $+ *,0,w,if (%p != ghost) || ($gettok($hget(pacservi,$1),4,32) != -1) hadd pacserv hspeedpac $remtok($hget(pacserv,hspeedpac),$1,32))
      hdel pacserv hspeed_ $+ %p
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      hadd pacservg %xy free
      hadd pacserv lspeed_ $+ %p 1
      .timerpacservegg $+ %xy off
      .timerpacservlspeedt $+ %p -ho 1 7000 hdel pacserv lspeed_ $+ %p
    }
    elseif (%h == lspeed) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacserv lspeedpac $addtok($hget(pacserv,lspeedpac),$6,32)
      hadd pacserv hspeedpac $remtok($hget(pacserv,hspeedpac),$6,32)
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      hadd pacservg %xy free
      .timerpacservhspeed $+ $6 off
      .timerpacservegg $+ %xy off
      .timerpacservlspeed $+ $6 -ho 1 7000 hadd pacserv lspeedpac $!remtok($hget(pacserv,lspeedpac), $6 ,32)
    }
    elseif (%h == shield) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacserv shield $addtok($hget(pacserv,shield),$6,32)
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      .timerpacservegg $+ %xy off
      .timerpacservshield $+ $6 -oh 1 18000 hadd pacserv shield $!remtok($hget(pacserv,shield), $6 ,32) $(|) swriting shield $!hget(pacserv,shield)
      swriting shield $hget(pacserv,shield)
      hadd pacservg %xy free
    }
    elseif (%h == invis) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      hadd pacserv invis $addtok($hget(pacserv,invis),$6,32)
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      swriting invis $hget(pacserv,invis)
      hadd pacservg %xy free
      .timerpacservegg $+ %xy off
      .timerpacservinvis $+ $6 -ho 1 7000 hadd pacserv invis $!remtok($hget(pacserv,invis), $6 ,32) $(|) swriting invis $!hget(pacserv,invis)
    }
    elseif (%h == pellet) {
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
      if (pacman* iswm * $6) var %i pellet+
      else var %i pellet-
      if (%i == pellet+) && ($hget(pacserv,pellet-)) hdel pacserv pellet-
      elseif (%i == pellet-) && ($hget(pacserv,pellet+)) hdel pacserv pellet+
      swriting %i 1
      hadd pacserv %i 1
      hadd pacservg %xy free
      .timerpacservegg $+ %xy off
      .timerpacservpellet -ho 1 7000 hdel pacserv %i $(|) swriting %i 
    }
    elseif (%h == eheart) {
      var %f $hfind(pacservg,free,$r(1,$hfind(pacservg,free,0).data)).data
      if (%f) {
        swriting heart %f 1
        hadd pacservg %f heart
        hadd pacservg %xy free
        hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
        .timerpacservegg $+ %xy off
      }
    }
    elseif ($istok(flipv fliph,%h,32)) && (cpu* !iswm $gettok($hget(pacservi,$6),7,32)) {
      set -l %g $gsockname($gettok($hget(pacservi,$6),7,32))
      sockwrite -n %g %h 1
      swriting flipadd $6
      .timerpacservegg $+ %xy off
      .timerpacservflip $+ $6 -oh 1 8000 sockwrite -n %g %h $(|) swriting flipdel $6
      hadd pacservg %xy free
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
    }
    elseif (%h == rteleport) {
      .timerpacservegg $+ %xy off
      var %f $hfind(pacservg,free,$r(1,$hfind(pacservg,free,0).data)).data
      if (%f) {
        var %xq $gettok(%f,1,46) * $1,%yq $gettok(%f,2,46) * $2
        hadd pacservi $6 %xq %yq $gettok($hget(pacservi,$6),3-9,32) %xq %yq
        hadd pacservg %xy free
        hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
        if ($hget(pacserv,as_ $+ $6)) && ($pac_closest($6)) {
          hadd pacserv as_ $+ $6 $v1 1
          astar_go $6 $v1
        }
      }
    }
    elseif (%h == heart) {
      .timerpacservegg $+ %xy off
      swriting score %cmd 50
      %cmd pacserv sbonus 50
      hadd pacservi $6 $puttok(%v,$calc($gettok(%v,9,32) + 50),9,32)
      %cmd pacserv lives 
      hadd pacservg %xy free
      swriting lives $hget(pacserv,lives)
      hadd pacserv toclear $hget(pacserv,toclear) $+(%x.,$chr(44),%y.,$chr(44),$6,$chr(44),%h)
    }
    if ($calc($hget(pacserv,sbonus) // 1000) != $hget(pacserv,bonusi)) {
      hadd pacserv bonusi $v1
      :again
      if (%no) || ($hget(pacserv,kmode)) || (!$hget(pacserv,cpu)) || (($hget(pacserv,letterm)) && ($hget(pacserv,letteri)) && ($hget(pacserv,letterr)) && ($hget(pacserv,letterc))) set -l %l hspeed lspeed hspeedt lspeedt shield invis pellet eheart flipv fliph rteleport
      else set -l %l hspeed lspeed hspeedt lspeedt shield invis pellet eheart flipv fliph rteleport letterm letteri letterr letterc
      ;else set -l %l letterm letteri letterr letterc
      set -l %egg
      set -l %e 0.9 1.9 2.9 3.9 4.9 5.9 0.10 1.10 2.10 3.10 4.10
      set -l %o $gettok(%l,$r(1,$numtok(%l,32)),32)
      set -l %ll letterm letteri letterr letterc
      while (letter* iswm %o) && (($istok($hget(pacserv,pl),%o,32)) || ($hget(pacserv,%o))) {
        %ll = $remtok(%ll,%o,32) 
        %o = $gettok(%ll,$r(1,$numtok(%ll,32)),32)
      }
      if (!%o) { 
        set -l %no 1
        goto again
      }
      ;%o = shield
      if (%o == lspeed) %egg = $gettok(%e,6,32)
      elseif (%o == hspeed) %egg = $gettok(%e,9,32)
      elseif (%o == hspeedt) %egg = $gettok(%e,5,32)
      elseif (%o == lspeedt) %egg = $gettok(%e,7,32)
      elseif (%o == shield) %egg = $gettok(%e,10,32)
      elseif (%o == pellet) %egg = $gettok(%e,11,32)
      elseif (%o == invis) %egg = $gettok(%e,3,32)
      elseif (%o == eheart) %egg = $gettok(%e,1,32)
      elseif (%o == flipv) %egg = $gettok(%e,2,32)
      elseif (%o == fliph) %egg = $gettok(%e,4,32)
      elseif (%o == rteleport) %egg = $gettok(%e,8,32)
      elseif (letter* iswm %o) %egg =
      else echo 4 -sg egg not set
      .timerpacservegg $+ %xy -oh 1 $r(3000,6000) pacserv_addegg %egg %o
    }
    if (!$hget(pacserv,boules)) hadd pacserv pacend win
    if (%h) return %xy
  }
}

alias pacserv_addegg {
  set -ln %f $hfind(pacservg,free,$r(1,$hfind(pacservg,free,0).data)).data
  if (%f) {
    if (letter* iswm $1) {
      if ($hget(pacserv,kmode)) || ($istok($hget(pacserv,pl),$1,32)) || ($hget(pacserv,$1)) return
      set -nl %l $1
      tokenize 32 $sizesprite $replace(%f,.,$chr(32))
      hadd pacserv pl $hget(pacserv,pl) %l
      ;  echo -sg adding letter %l : $hget(pacserv,pl)
      hadd pacservi %l $calc($1 * $3) $calc($2 * $4) 0 0 0 1 %l 0 0 $calc($1 * $3) $calc($2 * $4)
      swriting pladd %l
      set -ln %g $wildtok($hget(pacserv,pl),ghost?*,1,32)
      if (!%g) echo -sg no ghost and letter?? should not be possible??
      else astar_go %l %g
    }
    else {
      swriting bonus $2 %f 1 $1
      hadd pacservg %f $2
      .timerpacservegg $+ %f -ho 1 7000 hadd pacservg %f free $(|) swriting bonus $2 %f
    }
  }
}

alias -l pacserv_targetmirc {
  set -ln %3 $3
  set -ln %xs $1
  set -ln %ys $2
  tokenize 32 $regsubex($hget(pacserv,pl),/(ghost\d+)(?= |$)|\S+(?: |$)/Fg,\1)
  if (!$1) {
    swriting @mirc
    hadd pacserv pl $remtok($hget(pacserv,pl),@mirc,32)
    hdel pacserv kmt
    hdel pacserv kml
    hdel pacserv rplshoot
    hadd pacserv pacend win
    return 
  }
  scon -r if (!%h) && ($pacserv_starg( $* )) set -ln % $+ h $!hget(pacservi, $* )
  if (!%h) return 2
  set -l %x $calc($gettok(%h,1,32) // %xs)
  set -l %y $calc($gettok(%h,2,32) // %ys)
  hadd pacserv rplshoot %x %y
  swriting rplshoot %x %y
  return 1
}

alias -l pacserv_starg {
  tokenize 32 $hget(pacservi,$1)
  if ($4 != -1) && (!$inrect($1,$2 , [ $hget(pacserv,isinspawn) ] )) return 1
}

alias -l pacserv_sloop {
  set -ln %no
  set -ln %id $1
  set -ln %xs $2
  set -ln %ys $3
  set -ln %wall?
  tokenize 32 $hget(pacservi,$1)
  set -ln %h $1-
  ;echo -s in %id %h
  if ($0 < 7) {
    echo -s warning %id : %h
    return
  }
  if $hget(pacserv,as_ $+ %id) {
    set -ln %path $v1
    set -ln %n $gettok(%path,2,32)
    set -l %x $1 / %xs
    set -l %y $2 / %ys
    set -ln %nt
    set -ln %i
    if $4 == -1 {
      if (%path == -1) && (*.* !iswm %x %y) $astar_go(%id,1)
      elseif $+(%x,.,%y) == $remove($gettok(%path,%n -,32),!) {
        if ($7 == cpu) astar_go %id $pac_closest(%id)
        else {
          hadd pacservi %id $1-3 0 $5-
          hdel pacserv as_ $+ %id
          return
        }
      }
    }
    else {
      %nt = $numtok(%path,32)
      if ((%n >= $calc(%nt //1.2)) && (%nt > 5) && (!$hget(pacserv,surround $+ %id))) || (%n == %nt) || (%nt == 2) {
        if (letter* !iswm $7) {
          astar_go %id $pac_closest(%id)
        }
        else {
          hadd pacserv pl $remtok($hget(pacserv,pl),$7,32)
          hdel pacservi $7
          hdel pacserv as_ $+ $7
          swriting pldel $7

          return
        }
        %no = 1
      }
    }
    if (!%no) && ($remove($gettok(%path,%n,32),!) == $+(%x,.,%y)) {
      inc %n 
      set -ln %c $3
      tokenize 46 $gettok(%path,%n,32)
      ;   if (letter* iswm %id) echo -sg %id  %c -- %path -- %n -- $1-
      if $1 != $null {
        hadd pacserv as_ $+ %id $puttok(%path,%n,2,32)
        if !* !iswm $1 {
          if $1 == %x {
            if ($2 < %y) %i = 38
            elseif ($2 > %y) %i = 40
            else %i = %c
            hadd pacservi %id $puttok(%h,%i,3,32)
            ;$gettok(%h,1-2,32) %i $gettok(%h,4-,32)
          }
          else {
            if ($1 < %x) %i = 37
            elseif ($1 > %x) %i = 39
            else %i = %c
            hadd pacservi %id $puttok(%h,%i,3,32)
            ;$gettok(%h,1-2,32) %i $gettok(%h,4-,32)
          }
        }
      }
      else {
        if (letter* iswm %id) {
          hadd pacserv pl $remtok($hget(pacserv,pl),%id,32)
          hdel pacservi %id
          hdel pacserv as_ $+ %id
          swriting pldel %id
          return
        }
        else astar_go %id $pac_closest(%id)
      }
    }
    tokenize 32 $hget(pacservi,%id)
  }
  if $6 ghost* iswm 130 %id {
    hadd pacservi %id $calc($gettok($hget(pacservg,%id),1,32)*%xs) $calc($gettok($hget(pacservg,%id),2,32)*%ys) 0 0 $5 1 $7 0 0
    return
  }
  elseif (%id == @mirc) {
    if ($calc($gettok($hget(pacservg,size),2,32) / 2 - 12) > $2) hadd pacservi %id $1 $calc($2 + 0.8) $3-
    else {
      if (!$timer(pacservkshoot)) && (!$hget(pacserv,kmt)) && (!$hget(pacserv,kml)) .timerpacservkshoot -ho 1 0 if ($pacserv_targetmirc( %xs , %ys ) == 1) hadd pacserv kmt $!ticksqpc $(|) elseif ($v1 == 2) .timerpacservkshoot -ho 1 $!gettok(3000 2600 2200 1800 1400 1000,$hfind(pacservi,ghost*,0,w),32) $!timer(pacservkshoot).com
      else {
        if ($hget(pacserv,kmt) != $null) {
          var %i $calc(($ticksqpc - $hget(pacserv,kmt)) // 50)
          swriting kmt %i
          if (%i >= 9) { 
            hdel pacserv kmt
            .timerpacservkshoot off
            swriting kmt
            hadd pacserv kml 1
            swriting kml 1
            var %a 1
            while ($gettok($hget(pacserv,pl),%a,32)) {
              if (ghost?* iswm $v1) {
                var %gv $v2
                var %hg $hget(pacservi,$v2),%rpl $hget(pacserv,rplshoot)
                var %gx $gettok(%hg,1,32),%gy $gettok(%hg,2,32),%xr $gettok(%rpl,1,32) * %xs,%yr $gettok(%rpl,2,32) * %ys
                if ($inrect($calc(%gx + 7),$calc(%gy + 7),$calc(%xr - 7),$calc(%yr - 7),28,28)) {
                  if ($istok($hget(pacserv,shield),%gv,32)) {
                    hadd pacserv shield $remtok($hget(pacserv,shield),%gv,32)
                    swriting shield $hget(pacserv,shield)
                  }
                  else {
                    ;echo -s kill %gv
                    swriting mirckill %gv
                    hadd pacserv pl $remtok($hget(pacserv,pl),%gv,32)
                    hdel pacservi %gv
                  }
                }
              }
              inc %a
            }
            .timerpacservkml -ho 1 140 hdel pacserv kml $(|) swriting kml $(|) hdel pacserv kmt $(|) .timerpacservkshoot -ho 1 $!gettok(3000 2600 2200 1800 1400 1000,$hfind(pacservi,ghost*,0,w),32) if ($pacserv_targetmirc( %xs , %ys ) == 1) hadd pacserv kmt $!!ticksqpc $!(|) elseif ($v1 == 2) .timerpacservkshoot -ho 1 $!!gettok(3000 2600 2200 1800 1400 1000,$hfind(pacservi,ghost*,0,w),32) $!!timer(pacservkshoot).com
          }
        }
      }
    }
  }
  set -ln %o $1-2
  set -ln %xp $1
  set -ln %yp $2
  if pacman* iswm %id {
    if ($istok($hget(pacserv,lspeedpac),%id,32)) set -ln %sp 2
    elseif ($istok($hget(pacserv,hspeedpac),%id,32)) set -ln %sp 6
    elseif ($hget(pacserv,lspeed_pacman)) set -ln %sp 2
    elseif ($hget(pacserv,hspeed_pacman)) set -ln %sp 6
    elseif ($hget(pacserv,mange)) set -ln %sp 4
    else set -ln %sp 3
  }
  elseif (letter* iswm %id) set -ln %sp 1
  elseif ($4 == -1) set -ln %sp 3
  elseif ($istok($hget(pacserv,stunt),%id,32)) set -ln %sp 1
  elseif ($istok($hget(pacserv,lspeedpac),%id,32)) set -ln %sp 2
  elseif ($istok($hget(pacserv,hspeedpac),%id,32)) set -ln %sp 6
  elseif ($hget(pacserv,lspeed_ghost)) set -ln %sp 2
  elseif ($hget(pacserv,hspeed_ghost)) set -ln %sp 6
  elseif ($hget(pacserv,mange)) && ($5) set -ln %sp 3
  else set -ln %sp 4
  ;set -l %sp 0.3
  set -ln %xp1 $10
  set -ln %yp1 $11
  set -ln %oldx %xp
  set -ln %oldy %yp
  set -ln %no1
  if (letter* iswm %id) && (%sp != 1) echo -sg letter speed not 1 %id : %sp
  ;try wanted dir with current pos
  if $4 !isin -10 {
    if (%xp1 != $null) && ($3) && %xs // %xp1 && %ys // %yp1 {
      set -ln %add1
      if $4 == 37 {
        %add1 = $calc(%xp1 - $calc((%xp1 -1)//%xs) * %xs)
        if (%add1 < %sp) %xp1 = %xp1 - $v1
        else %xp1 = %xp1 - $v2
      }
      elseif $4 == 39 {
        %add1 = $calc(($calc(%xp1 // %xs)+1) * %xs - %xp1)
        if (%add1 < %sp) %xp1 = %xp1 + $v1
        else %xp1 = %xp1 + $v2
      }
      elseif $4 == 38 {
        %add1 = $calc(%yp1 - $calc((%yp1 -1) // %ys) * %ys)
        if (%add1 < %sp) %yp1 = %yp1 - $v1
        else %yp1 = %yp1 - $v2
      }
      else {
        %add1 = $calc(($calc(%yp1 // %ys) + 1) * %ys - %yp1)
        if (%add1 < %sp) %yp1 = %yp1 + $v1
        else %yp1 = %yp1 + $v2
      }
      if $findtok(37 39,$4,32) {
        if ($v1 == 2) set -ln %pt $+($calc((%xp1 +%xs) //%xs),.,$calc(%yp1 //%ys))
        else set -ln %pt $+($calc(%xp1 //%xs),.,$calc(%yp1 //%ys))
        if ($hget(pacservg,%pt) == wall) || (($hget(pacservg,%pt) == wf) && ((pacman* iswm %id) || (!$inrect( %xp , %yp , [ $hget(pacserv,isinspawn) ] )))) %wall? = 1
      }
      elseif $findtok(38 40,$4,32) {
        if ($v1 == 2) set -ln %pt $+($calc(%xp1 //%xs),.,$calc((%yp1 +%ys) //%ys))
        else set -ln %pt $+($calc(%xp1 //%xs),.,$calc(%yp1 //%ys))
        if ($hget(pacservg,%pt) == wall) || (($hget(pacservg,%pt) == wf) && ((pacman* iswm %id) || (!$inrect( %xp , %yp , [ $hget(pacserv,isinspawn) ] )))) %wall? = 1
      }
      if (!%wall?) hadd pacservi %id %xp1 %yp1 $4 0 $5- 
      else %no1 = 1
    }
    else %no1 = 1
    ;if didn't work, try wanted dir with old pos (compensate for lag and actually allow faints
    if (%no1) {
      if %xs // %xp && %ys // %yp {
        set -ln %add
        if $4 == 37 {
          %add = $calc(%xp - $calc((%xp -1)//%xs) * %xs)
          if (%add < %sp) %xp = %xp - $v1
          else %xp = %xp - $v2
        }
        elseif $4 == 39 {
          %add = $calc(($calc(%xp // %xs)+1) * %xs - %xp)
          if (%add < %sp) %xp = %xp + $v1
          else %xp = %xp + $v2
        }
        elseif $4 == 38 {
          %add = $calc(%yp - $calc((%yp -1) // %ys) * %ys)
          if (%add < %sp) %yp = %yp - $v1
          else %yp = %yp - $v2
        }
        else {
          %add = $calc(($calc(%yp // %ys) + 1) * %ys - %yp)
          if (%add < %sp) %yp = %yp + $v1
          else %yp = %yp + $v2
        }             
        if $findtok(37 39,$4,32) {
          if ($v1 == 2) set -ln %pt $+($calc((%xp +%xs) //%xs),.,$calc(%yp //%ys))
          else set -ln %pt $+($calc(%xp //%xs),.,$calc(%yp //%ys))
          if ($hget(pacservg,%pt) == wall) || (($hget(pacservg,%pt) == wf) && ((pacman* iswm %id) || (!$inrect(%oldx,%oldy, [ $hget(pacserv,isinspawn) ] )))) %wall? = 1
        }
        elseif $findtok(38 40,$4,32) {
          if ($v1 == 2) set -ln %pt $+($calc(%xp //%xs),.,$calc((%yp +%ys) //%ys))
          else set -ln %pt $+($calc(%xp //%xs),.,$calc(%yp //%ys))
          if ($hget(pacservg,%pt) == wall) || (($hget(pacservg,%pt) == wf) && ((pacman* iswm %id) || (!$inrect(%oldx,%oldy, [ $hget(pacserv,isinspawn) ] )))) %wall? = 1
        }
        if (!%wall?) hadd pacservi %id %xp %yp $4 0 $5-
        elseif (!$6) hadd pacservi %id $1-2 $4 0 $5 0 $7-
        else {
          set -ln %xp $1
          set -ln %yp $2
          set -ln %no 1
        }
      }
      else %no = 1
    }
  }
  ;otherwise try with current pos/dir
  else %no = 1
  if ($3 && %no) || !$4 {
    set -ln %add
    if $3 == 37 {
      %add = $calc(%xp - $calc((%xp -1) // %xs) * %xs)
      if (%add < %sp) %xp = %xp - $v1
      else %xp = %xp - $v2
    }
    elseif $3 == 39 {
      %add = $calc(($calc(%xp // %xs) +1) * %xs - %xp)
      if (%add < %sp) %xp = %xp + $v1
      else %xp = %xp + $v2
    }
    elseif $3 == 38 {
      %add = $calc(%yp - $calc((%yp -1) // %ys) * %ys)
      if (%add < %sp) %yp = %yp - $v1
      else %yp = %yp - $v2
    }
    else {
      %add = $calc(($calc(%yp // %ys) +1) * %ys - %yp)
      if (%add < %sp) %yp = %yp + $v1
      else %yp = %yp + $v2
    }
    if $gettok(- +,$findtok(37 39,$3,32),32) {
      set -ln %v1 $v1
      if ($3 == 39) set -ln %pt $+($calc((%xp + %xs) // %xs),.,$calc(%yp // %ys))
      else set -ln %pt $+($calc(%xp // %xs),.,$calc(%yp // %ys))

      if ($hget(pacservg,%pt) != wall) && (($hget(pacservg,%pt) != wf) || ((pacman* !iswm %id) && (($inrect($1,$2, [ $hget(pacserv,isinspawn) ] ) || ($4 == -1))))) hadd pacservi %id %xp %yp $3-
      elseif (%xs \\ $1) hadd pacservi %id $calc($1 %v1 %add) %yp $3-
    }
    elseif $gettok(- +,$findtok(38 40,$3,32),32) {
      set -ln %v1 $v1
      if ($3 == 40) set -ln %pt $+($calc(%xp // %xs),.,$calc((%yp + %ys) // %ys))
      else set -ln %pt $+($calc(%xp // %xs),.,$calc(%yp // %ys))
      if ($hget(pacservg,%pt) != wall) && (($hget(pacservg,%pt) != wf) || ((pacman* !iswm %id) && (($inrect($1,$2, [ $hget(pacserv,isinspawn) ] ) || ($4 == -1))))) hadd pacservi %id %xp %yp $3-
      elseif (%ys \\ $2) hadd pacservi %id %xp $calc($2 %v1 %add) $3-
    }
  }
  if teleport* iswm $hget(pacservg,$+($calc($gettok($hget(pacservi,%id),1,32) / %xs),.,$calc($gettok($hget(pacservi,%id),2,32) / %ys))) {
    set -ln %xx
    set -ln %yy
    set -ln %h $v2
    if ($gettok(%h,4,32)) && ($4 != -1) {
      noop $hfind(pacservg,teleport*,0,w,set -ln %j %j $1).data
      %j = $gettok(%j,$r(1,$numtok(%j,32)),32)
      set -ln %g $gettok($hget(pacservg,%j),3,32)
      if ($hget(pacserv,as_ $+ %id)) {
        hadd pacserv as_ $+ %id $gettok($v1,1,32) 1
      }
    }
    else {
      set -ln %j $gettok(%h,2,32)
      set -ln %g $gettok($hget(pacservg,%j),3,32)
    }
    set -ln % $+ $replacex(%g,r,xx -1,l,xx +1,u,yy +1,d,yy -1)
    hadd pacservi %id $calc($gettok(%j,1,46) * %xs %xx) $calc($gettok(%j,2,46) * %ys %yy) $replacex(%g,l,39,r,37,d,38,u,40) $4-
  }
  elseif pacman* iswm %id && iteleport* iswm $hget(pacservg,$+($calc($gettok($hget(pacservi,%id),1,32) / %xs),.,$calc($gettok($hget(pacservi,%id),2,32) / %ys))) {
    set -ln %h $v2
    set -ln %n $remove($gettok(%h,1,32),iteleport)
    if (!$istok($hget(pacserv,busyport),%n,32)) {
      set -ln %j $gettok(%h,2,32)
      ;      set -ln %x $gettok(%j,1,46)
      ;      set -ln %y $gettok(%j,2,46)
      hadd pacserv busyport $addtok($hget(pacserv,busyport),%n,32)
      swriting busyport %n 1
      .timer 1 2 hadd pacserv busyport $!remtok($hget(pacserv,busyport), %n ,32) $(|) swriting busyport %n 0
      hadd pacservi %id $calc($gettok(%j,1,46) * %xs) $calc($gettok(%j,2,46) * %ys) $3-
    }
  }
  tokenize 32 $hget(pacservi,%id)
  ; if ($istok($hget(pacserv,confused),%id,32)) && ($r(1,100) > 80) hadd pacservi %id $puttok($hget(pacserv,%id,$gettok(37 38 39 40,$r(1,4),32),4,32)
  if pacman* iswm %id {
    if ($pacserv_handlep(%xs,%ys,$1,$2,$3,%id) != $null) set -ln %tmp $puttok($hget(pacservi,%id),$v1,6,32) 
    else {
      if (%o != $1-2) %o = 1
      else %o = 0
      hadd pacservi %id $1-5 %o $7-
      set -ln %tmp $hget(pacservi,%id)
    }
    set -un %pc %pc $1-2
    set -un %pc1 %pc1 $+ $1-2_ 
    set -un %pclist %pclist %id
  }
  elseif (letter* iswm %id) {
    set -un %pacllist %pacllist %id
    set -un %pacl1 %pacl1 $1-2_
    set -ln %tmp $hget(pacservi,%id)
  }
  elseif $4 != -1 && %id != @mirc {
    if ($pacserv_handlep(%xs,%ys,$1,$2,$3,%id) != $null) set -ln %tmp $puttok($hget(pacservi,%id),$v1,6,32)
    else {
      if (%o != $1-2) %o = 1
      else %o = 0
      hadd pacservi %id $1-5 %o $7-
      set -ln %tmp $hget(pacservi,%id)
    }
    set -un %gh %gh $1-2
    set -un %gh1 %gh1 $+ $1-2_ 
    set -un %ghlist %ghlist %id
  }
  else set -ln %tmp $hget(pacservi,%id)
  set -un %send $addtok(%send,%id %tmp,95)
  hadd pacservi %id $gettok(%tmp,-3--,32) %oldx %oldy
  ; echo -sg out %id $hget(pacservi,%id)
}
alias -l pacserv_ssloop {
  set -ln %a $1-
  tokenize 32 $hget(pacserv,pl)
  pacserv_sloop $* %a
}
alias -l pacserv_getsend {
  tokenize 32 $hget(pacserv,pl)
  scon -r set -ln % $+ s $!+(%s,_, $* $!hget(pacservi, $* ))
  set -un %send $mid(%s,2)
}
alias -l pacserv_loop {
  if $hget(pacserv,pacend) {
    pacserv_end $v1
    return
  }
  set -ln %t $ticksqpc
  pacserv_ssloop $1-
  if ($pacserv_collision1(%pc1 %pacl1,%gh1 %pacl1,$1,$2,%pclist %pacllist,%ghlist %pacllist)) pacserv_getsend
  pacuserv_sendgame %send
  if ($hget(pacserv,toclear)) {
    swriting toclear $v1
    hdel pacserv toclear
  }
  :error
  if ($error) { set -l %e $v1 | reseterror | pacserv_end | pacserv_finish 3 %e | return }
  set -ln %c $calc(45 - ($ticksqpc - %t))
  if (%c <= 0) { 
    %t = 0
    echo -sg exceed %c
  }
  else %t = $v1

  .timerpacservloop -ho 1 %t pacserv_loop $1-
}

; SENDING UDP
alias -l pacuserv_sendgame {
  set -ln %d $1-
  tokenize 95 $regsubex($str(a,$numtok($hget(pacserv,udpsend),32)),/a/g,$replace($gettok($hget(pacserv,udpsend),\n,32),/,$chr(32)) _)
  hinc pacserv upck
  sockudp -kn pacuserv $* %d
}
;PAUSE
alias pacserv_pause {
  if ($timer(pacservloop).pause) set -l %a -r
  else set -l %a -P
  tokenize 32 .timerpacservloop .timerpacservmange .timerpacservfruit* .timerpacservshield* .timerpacservegg* .timerpacservkmtl .timerpacservinvis* .timerpacservflip* .timerpacservpellet* .timerpacserv+1  .timerpacservl* .timerpacservh* .timerpacservkshoot .timerpacservkmtl .timerpacservstunt* .timerpacservinvinc* .timerpacservflip* .timerpacservmange
  $* %a
}
menu @pacman {
  mouse:titlebar @pacman $calc($mouse.x // 14) $calc(($mouse.y - $hget(pacclient,height)) // 14)
}
alias astar_go {
  if ($isid) || (($hget(pacservi,$1)) && ($hget(pacservi,$2))) {
    set -ln %ghost $1
    set -ln %pac $2
    set -ln %h $hget(pacservi,%ghost)
    set -ln %ok $false
    set -ln %xm
    set -ln %ym
    tokenize 32 $sizesprite $hget(pacservg,size) $hget(pacservi,$1) $hget(pacservi,$2)
    %xm = $3 / $1
    %ym = $4 / $2
    dec %xm 2
    dec %ym 2
    if (!$isid) {
      set -ln %p $16
      set -ln %p1 $17
      set -ln %p2 $18
      set -l %xxx %p / $1
      set -ln %x1 %xxx
      set -ln %d 0 $9
      if . isin %x1 {
        %x1 = $floor(%x1)
        if (%p2 == 39) inc %x1 
      }
      set -l %yyy %p1 / $2
      set -ln %y1 %yyy
      if . isin %y1 {
        %y1 = $floor(%y1)
        if (%p2 == 40) inc %y1
      }
    }
    else {
      set -ln %x1 $gettok($hget(pacservg,%ghost),1,32)
      set -ln %y1 $gettok($hget(pacservg,%ghost),2,32)
      set -ln %d -1 0
    }
    set -l %x2 $5 / $1
    set -ln %xx %x2
    if (. isin %x2) {
      %x2 = $floor(%x2)
      if ($7 == 39) inc %x2
    }
    else %ok = 0
    set -l %y2 $6 / $2
    set -ln %yy %y2
    if . isin %y2 {
      %y2 = $floor(%y2)
      if ($7 == 40) inc %y2
    }
    elseif (%ok == 0) %ok = 1
    if (%x2 !isnum 1- $+ %xm) || (%y2 !isnum 1- $+ %ym) {
      if ($isid) {
        hadd pacserv as_ $+ %ghost -1
        hadd pacservi %ghost $gettok(%h,1-3,32) -1 -1 $gettok(%h,6-,32)
      }
      return
    }
    if (%x1 == -1) %x1 = 1
    elseif (%x1 >= $calc(%xm +1)) %x1 = %xm
    if (%y1 == -1) %y1 = 1
    elseif (%y1 >= $calc(%ym +1)) %y1 = %ym

    ;echo -s $+(%x2,.,%y2) $+(%x1,.,%y1)
    if ($hget(pacserv,surround $+ %ghost)) && (%d != -1 0) {
      ;echo -s $1- -- $18
      set -ln %18 $18
      if (!%18) %18 = 37
      if (%18) {
        set -ln %x3 %x1
        set -ln %y3 %y1
        set -ln %depth $r(0,$calc($r(1,3) + 2))
        set -ln %dd %depth
        set -ln %trace %18
        if ((%18 == 37) && (%x3 > 0) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32))) || ((%18 == 39) && (%x3 < $calc(%xm - 1)) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32))) || ((%18 == 38) && (%y3 > 0) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32))) || ((%18 == 40) && (%y3 < $calc(%ym - 1)) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32))) {
          while (%depth) {
            if (%18 == 37) {
              dec %x3
              while (%x3 > 0) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) dec %x3
            }
            elseif (%18 == 39) {
              inc %x3
              while (%x3 < $calc(%xm - 1)) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) inc %x3
            }
            elseif (%18 == 38) {
              dec %y3
              while (%y3 > 0) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) dec %y3
            }
            elseif (%18 == 40) {
              inc %y3
              while (%y3 < $calc(%ym - 1)) && (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32)) && ($istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) inc %y3
            }
            if (%18 == 37) {
              if (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) {
                if (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 -1),.,%y3)),9),32)) break
                else %18 = 38
              }
              elseif ($istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32)) break
              else %18 = 40
            }
            elseif (%18 == 39) {
              if (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) {
                if (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) break
                else %18 = 38
              }
              elseif ($istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) break
              else %18 = 40
            }
            elseif (%18 == 38) {
              if (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32)) {
                if (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) break
                else %18 = 37
              }
              elseif ($istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 - 1))),9),32)) break
              else %18 = 39
            }
            elseif (%18 == 40) {
              if (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 - 1),.,%y3)),9),32)) {
                if (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) break
                else %18 = 37
              }
              elseif (!$istok(wall wf iteleport,$left($hget(pacservg,$+($calc(%x3 + 1),.,%y3)),9),32)) || (!$istok(wall wf iteleport,$left($hget(pacservg,$+(%x3,.,$calc(%y3 + 1))),9),32)) break
              else %18 = 39           
            }
            set -l %trace %trace %18 
            dec %depth
          }
        }
        ;    echo -a %ghost surround %x3 %y3 (original %x1 %y1 ) %dd ( %depth ) -- %trace -- %deb
        %x1 = %x3
        %y1 = %y3
      }
    }
    set -ln %i
    set -ln %as $jsAStarSearch(%x2,%y2,%x1,%y1)
    ;echo -sg %ghost : $+(%x2,.,%y2) %as
    if (!%ok) hadd pacserv as_ $+ %ghost %pac 3 $+(%x2,.,%y2) %as
    else hadd pacserv as_ $+ %ghost %pac 3 %as
    ;hadd -m pacservpath $+($hget(pacserv,md5map),%x2,.,%y2,.,%x1,.,%y1) %as
    tokenize 46 $gettok($hget(pacserv,as_ $+ %ghost),3,32)
    if (!* !iswm $1) {
      set -ln %c $gettok($hget(pacservi,%ghost),3,32)
      if ($1 == %xx) {
        if ($2 < %yy) %i = 38
        elseif ($2 > %yy) || (%c == 0) %i = 40
        else %i = %c
        hadd pacservi %ghost $gettok(%h,1-2,32) %i %d $gettok(%h,6-,32)
      }
      else {
        if ($1 < %xx) %i = 37
        elseif ($1 > %xx) || (%c == 0) %i = 39
        else %i = $gettok($hget(pacservi,%ghost),3,32)
        hadd pacservi %ghost $gettok(%h,1-2,32) %i %d $gettok(%h,6-,32)
      }
    }
  }
}
alias -l comShell    return Pacman/jsAStar/Shell
alias -l comEngine return Pacman/jsAStar/JSEngine
alias -l comMap      return Pacman/jsAStar/Map
alias -l jsAStarLoadMap {
  if ($jsAStarInit) return $v1
  if ($com($comMap))  .comclose $v1
  if (!$com($comEngine, createMap, 1, &bstr, $1, dispatch* $comMap)) || ($comerr) return Failed to load the map
}
alias jsAStarSearch {
  if (!$com($comMap, search, 1, integer, $1, integer, $2, integer, $3, integer, $4, bool, false) || $comerr) {
    echo -s Failed to perform astar search > $1- > $com($comMap).errortext
    return
  }
  return $com($comMap).result
}
alias -l jsAStarInit {
  var %error
  if ($com($comShell)) && ($com($comEngine)) return
  jsAStarCleanup
  .comopen $comShell MSScriptControl.ScriptControl
  if (!$com($comShell)) || ($comerr) %error = Failed to create MSScriptControl.ScriptControl instance
  elseif (!$com($comShell, language, 4, bstr, jscript)) %error = Failed to set shell language
  elseif (!$com($comShell, AllowUI, 4, bool, $false)) || ($comerr) %error = Failed to disable js UI
  elseif (!$com($comShell, timeout, 4, integer, -1)) || ($comerr) %error = Failed to unset js timeout
  elseif (!$com($comShell, ExecuteStatement, 1, &bstr, $jsAStarScript)) || ($comerr) %error = Failed to execute js script
  elseif (!$com($comShell, Eval, 1, bstr, this, dispatch* $comEngine)) || ($comerr) || (!$com($comEngine)) %error = Failed to create reference to the JS engine
  else return
  jsAStarCleanup
  return %error
}
alias -l jsAStarCleanup {
  if ($com($comMap)) .comclose $v1
  if ($com($comEngine)) .comclose $v1
  if ($com($comShell)) .comclose $v1
}
alias -l jsAStarScript {
  var %file = $scriptdirmAStar.js
  bread $qt(%file) 0 $file(%file).size &jsAStarScript
  return &jsAStarScript
}

alias maptobvar {
  ;$1 = hash table
  var %t $1
  tokenize 32 $sizesprite $hget($1,size)
  var %x,%y 0,%w $3 / $1,%h $4 / $2
  bset -t &map -1 %w
  bset &map -1 32
  var %s $bvar(&map,0)
  while (%y < %h) {
    %x = 0
    while (%x < %w) {
      if (%x == 0) {
        if (teleport* iswm $hget(%t,-1. $+ %y)) {
          bset -t &map -1 ! $+ $mid($gettok($v2,1,32),9)
          bset &map -1 32
        }
        else {
          bset -t &map -1 0
          bset &map -1 32
        }
      }
      elseif (%x == $calc(%w - 1)) {
        if (teleport* iswm $hget(%t,$+($calc(%x + 1),.,%y))) {
          bset -t &map -1 ! $+ $mid($gettok($v2,1,32),9)
          bset &map -1 32
        }
        else {
          bset -t &map -1 0
          bset &map -1 32
        }
      }
      elseif (%y == 0) {
        if (teleport* iswm $hget(%t,%x $+ .-1)) {
          bset -t &map -1 ! $+ $mid($gettok($v2,1,32),9)
          bset &map -1 32
        }
        else {
          bset -t &map -1 0
          bset &map -1 32
        }
      }
      elseif (%y == $calc(%h - 1)) {
        if (teleport* iswm $hget(%t,$+(%x,.,$calc(%y + 1)))) {
          bset -t &map -1 ! $+ $mid($gettok($v2,1,32),9)
          bset &map -1 32
        }
        else {
          bset -t &map -1 0
          bset &map -1 32
        }
      }
      elseif ($hget(%t,$+(%x,.,%y)) == wall) {
        bset -t &map -1 0
        bset &map -1 32
      }
      else {
        bset -t &map -1 1
        bset &map -1 32
      }
      if (%s == $bvar(&map,0)) echo 4 -s pb
      var %s $bvar(&map,0)
      inc %x
    }
    inc %y
  }
  bcopy -c &map 1 &map 1 $calc($bvar(&map,0) -1)
}

; SIZE FOR THE SPRITES
alias -l sizesprite return 14 14



alias getlocalipv4 {
  .comopen loc WbemScripting.SWbemLocator
  .comclose loc $com(loc, ConnectServer, 3, dispatch* query)
  noop $com(query, ExecQuery, 3, bstr, Select * from Win32_NetworkAdapter WHERE NetConnectionStatus = 2, dispatch* adapter)
  var %a $comval(adapter,0)
  while (%a) { hadd -m getips $comval(adapter,%a,Index) $comval(adapter,%a,NetConnectionID) | dec %a }
  noop $com(query, ExecQuery, 3, bstr, Select * from Win32_NetworkAdapterConfiguration WHERE IPEnabled = True, dispatch* ips)
  var %cv $comval(ips,0)
  while (%cv) {
    if ($hget(getips,$comval(ips, %cv, Index)) != $null) {
      noop $comval(ips, %cv, IPAddress,&test).result
      breplace &test 0 32
      var %r $bvar(&test,1-).text
    }
    dec %cv

  }
  :error
  if ($error || %err) {
    echo -a /myips: $v1
    reseterror
  }
  hfree getips
  .comclose query
  .comclose adapter
  .comclose ips
  return $gettok(%r,1,32)
}

alias upnp_openipv4port {
  var %n = pacupnp, %m = pacmap ,%r = pacres
  .comopen %n HNetCfg.NATUPnP
  var %c = $com(%n,StaticPortMappingCollection,3,dispatch* %m)
  .comclose %n
  if (!%c) || (!$com(%m)) var %a UPNP is not working or not enabled on router
  elseif ($2) {
    if (!$com(%m,Add,3,long,$1,bstr,UDP,long,$1,bstr,$getlocalipv4,bool,$iif($2,$2,$true),bstr,mirc pacman,dispatch* %r)) var %a 0
    else var %a 1
  }
  elseif (!$com(%m,Remove,3,long,$1,bstr,UDP)) var %a 0
  else var %a 1
  if ($com(%r)) .comclose %r
  if ($com(%m)) .comclose %m
  return %a
}

/*
todo:
-eventually redo settings tab
-add stats at the end of the game
-maybe a bug when astar is tried and ghost $4 is not -1 when ghost is in external teleporter ($5 -1)
-when user quit or quitgame and game still going, not reflected in panel
-pacman dies but is not sent to respawn point
-fix collision not resulting in change of pos still formatting udp var
*/

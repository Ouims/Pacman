;                    CLIENT


;DIALOG EVENT
on *:dialog:pacman:*:*:{
  ;echo -a $devent $did
  if ($devent == init) {
    did -c pacman 21 99
  }
  elseif ($devent == active) {
    if ($dialog(pacman).tab == 3) {
      if ($timer(pacflicktab2)) {
        .timerpacflicktab2 off
        did -ra pacman 3 Game
      }
    }
  }
  elseif ($devent $did == sclick 3) {
    did -f pacman 14 | did -t pacman 57
    if (!$hget(pacclient,ip)) {
      dialog -t pacman Pacman $pac_version - You are not connected to any game
      .timer -ho 1 100 did -ra pacman 2 Game status: not connected
    }
    elseif ($sock(pacclient).status == connecting) {
      dialog -t pacman Pacman $pac_version - You are connecting to $hget(pacclient,ip) - $hget(pacclient,nick)
      .timer -ho 1 100 did -ra pacman 2 Game status: connecting... - nickname: $unsafe($hget(pacclient,nick))
    }
    else {
      if ($timer(pacflicktab2)) {
        .timerpacflicktab2 off
        did -ra pacman 3 Game
      }
      dialog -t pacman Pacman $pac_version - You are connected to $hget(pacclient,ip) - $hget(pacclient,nick)
      .timer -ho 1 100 did -ra pacman 2 Game status: connected - nickname: $unsafe($hget(pacclient,nick))
    }
  }
  elseif ($devent $did == sclick 57) && ($dialog(pacman).tab == 3) && ($did(pacman,14).lines != 0) client_input
  elseif ($devent $did == sclick 53) noop $findfile($sdir($scriptdirmaps),*.plvl,0,$() if (!$didwm(pacman,6,$1-,0)) $({,0) did -a pacman 26 $!left($nopath($1-),-5) $(|,0) did -a pacman 6 $!1- $(},0))
  elseif ($devent $did == sclick 80) {
    var %a 1
    while ($did(29,%a).sel) {
      if ($remove($did(29,$v1),~,@) != $hget(pacclient,nick)) sockwrite -n pacclient ban $v1
      inc %a
    }
  }
  elseif ($devent $did == sclick 44) {
    if ($did(6,$did(26).sel)) peditor $v1
  }
  elseif ($devent $did == sclick 43) {
    did -a pacman 49 $did(26,$did(26,1).sel)
    pacclient_updatemaps
  }
  elseif ($devent $did == dclick 26) {
    did -a pacman 49 $did(26,$did(26,1).sel)
    did -e pacman 54
    pacclient_updatemaps
  }
  elseif ($devent $did == sclick 82) peditor
  elseif ($devent $did == sclick 26) {
    if ($did(26,1).sel) did -e pacman 44,42,43
    else { did -b pacman 44,42,43 }
  }
  elseif ($devent $did == sclick 42) {
    hmake pacclienttmp
    var %map $did(6,$did(26,1).sel)
    hload pacclienttmp $qt(%map)
    preview %map
    hfree pacclienttmp 
  }
  elseif ($devent $did == sclick 49) {
    if (!$did(49,0).sel) did -b pacman 52
    else did -e pacman 52,54
  }
  elseif ($devent $did == dclick 49) {
    if ($did(49,1).sel) {
      did -d pacman 49 $v1
      did -b pacman 52
    }
    if (!$did(49).lines) did -b pacman 54,52
    pacclient_updatemaps
  }
  elseif ($devent $did == sclick 52) {
    var %a $did(49,0).sel
    while (%a) {
      did -d pacman 49 $did(49,%a).sel
      dec %a
    }
    did -b pacman 52
    if (!$did(49,0).lines) did -b pacman 54
    pacclient_updatemaps
  }
  elseif ($devent $did == sclick 54) {
    did -r pacman 49
    did -b pacman 54,52
    pacclient_updatemaps
  }
  elseif ($devent $did == sclick 32) {
    var %a 1
    while ($did(29,%a).sel) {
      if ($remove($did(29,$v1),~,@) != $hget(pacclient,nick)) sockwrite -n pacclient kick $v1
      inc %a
    }
  }
  elseif ($devent $did == scroll 21) { 
    if (!$did(22)) { did -ra pacman 22 1 | did -c pacman 21 1 }
    else {
      did -ra pacman 22 $calc(100 - $did(21).sel)
      sockwrite -n pacclient lives $did(22)
    }
  }
  elseif ($devent $did == edit 22) {
    if ($did(22) != $null) {
      if ($did(22) !isnum 1-99) { did -ra pacman 22 1 | did -c pacman 21 1 } 
      else did -c pacman 21 $calc(100 - $did(22))
    }
    else did -c pacman 21 99
    sockwrite -n pacclient lives $did(22)
  }
  elseif ($istok(sclick dclick,$devent,32)) && ($did == 29) {
    did $iif($did(29).sel,-e,-b) pacman 48,30,31,32,80
    var %a 1,%g
    while ($did(29,$did(29,%a).sel)) {
      if (~* iswm $v1) inc %g
      inc %a 
    }
    var %n $wildtok($didtok(pacman,29),~*,0,44) + $hget(pacclient,cpu)
    if ($calc(%n + $did(29,0).sel - %g + ) > 6) did -b pacman 48

    var %a 1,%g
    while ($did(29,$did(29,%a).sel)) {
      if (@* iswm $v1) inc %g
      inc %a 
    }
    var %n $wildtok($didtok(pacman,29),@*,0,44)
    if ($calc(%n + $did(29,0).sel - %g) > 6) did -b pacman 30
  }
  elseif ($devent $did == sclick 48) { 
    var %a 1
    while ($did(29,%a).sel) {
      sockwrite -n pacclient player $remove($did(29,$v1),@,~) ghost
      inc %a
    }
  }
  elseif ($devent $did == sclick 30) { 
    var %a 1
    while ($did(29,%a).sel) {

      sockwrite -n pacclient player $remove($did(29,$v1),@,~) pacman
      inc %a
    }
  }
  elseif ($devent $did == sclick 31) { 
    var %a 1
    while ($did(29,%a).sel) {
      sockwrite -n pacclient player $remove($did(29,$v1),@,~)
      inc %a
    }
  }
  elseif ($devent $did == sclick 55) {
    sockwrite -n pacclient sready $did(55).state
    did $iif($did(55).state,-b,-e) pacman 26,48,30,31,32,29,44,21,22,53,62,49
    did -h pacman 86
    if ($v1) {
      did -u pacman 26,29,49
      did -b pacman 42,52,54,43,49
    }
    else did -b pacman 44,48,30,31,32
  }
  elseif ($devent $did == sclick 62) {
    sockwrite -n pacclient cpu $did(62)
    var %a $did(29,0).sel
    while (%a) {
      did -u pacman 29 $did(29,%a).sel
      dec %a
    }
    did -b pacman 48,30,31,32,80
  }
  elseif ($devent $did == sclick 58) {
    :ready
    var %type = $iif($regex($hget(pacclient,nicks),/(?:^| )@\Q $+ $replacecs($hget(pacclient,nick),\E,\E\\E\Q) $+ \E(?:$| )/),ready)
    if (*loff.png iswm $did(58)) {
      hadd pacclient localport $getfreeport(8002)
      sockudp -kn pacuclient $hget(pacclient,ip) $hget(pacclient,udp) $iif(%type == ready,pready,spready) $hget(pacclient,nick) $hget(pacclient,keyp)
    }
    else { sockwrite -n pacclient $iif(%type == ready,pnready,spnready) | sockclose pacuclient }
  }
  elseif ($devent $did == sclick 88) pacclient_stop
  elseif ($devent == close) pacclient_stop 
}

alias -l pacclient_updatemaps {
  var %a 1
  while ($did(49,%a)) {
    var %d $v1,%n $didwm(pacman,26,%d),%map $did(6,%n)
    if (!$exists(%map)) {
      did -d pacman 26,6 %n $input(The map %map doesn't exist anymore and will be removed from the list.,iuo,Pacman - Information)
      did -d pacman 49 %a
    }
    var %maps $+(%maps,$chr(4),%map)
    inc %a 
  }
  sockwrite -n pacclient maps %maps
}

;CPU
alias -l pacclient_cpu if ($hget(pacclient,owner)) did -ck pacman 62 $calc($1 + 1) | hadd pacclient cpu $1 | did -ra pacman 84 $1

;KEYPL

alias -l pacclient_keypl hadd pacclient keypl $1

; GET FREE PORT
alias -l getfreeport while (!$portfree($1)) tokenize 32 $calc($1 +1) | return $1

; AM I A PLAYER ?
alias -l player? return $regex($hget(pacclient,nicks),/([@~]) $+ $hget(pacclient,nick) $+ (?: |$)/)

; PREVIEW
alias -l preview {
  var %map $1-
  tokenize 32 $hget(pacclienttmp,size) $sizesprite
  window -c @pacman $+ $chr(160) $+ preview
  window -Bfpdh @pac_editbuf -1 -1 $pic($scriptdirpactiles.png).width $pic($scriptdirpactiles.png).height
  drawpic -c @pac_editbuf 0 0 $qt($scriptdirpactiles.png)
  window -Cfpkdao +tL @pac_edit1 -1 -1 $1 $calc($2 + $4)
  titlebar @pac_edit1 of %map
  drawrect -rfn @pac_edit1 $hget(pacclienttmp,background) 0 0 0 $1 $calc($2 + $4)
  drawtext -nr @pac_edit1 0 verdana 9 2 1 $chr(35) of Power pellet: $hfind(pacclienttmp,bcoin*,0,w).data $str($chr(160),7) $chr(35) of Pellet: $hfind(pacclienttmp,scoin,0,w).data $str($chr(160),7) Background color (rgb): $rgb($hget(pacclienttmp,background))
  tokenize 32 $sizesprite
  var %a 1
  while ($hget(pacclienttmp,%a).item != $null) {
    var %i $v1,%d $hget(pacclienttmp,%i)
    if ($findtok(wall useless scoin bcoin,$gettok(%d,1,32),32)) drawcopy -tn @pac_editbuf 16711935 $calc(($v1 -1) * $1) 0 $1- @pac_edit1 $calc($gettok(%i,1,46)* $1) $calc($gettok(%i,2,46) * $2 + $2)
    elseif ($regex(%i,/^(ghost\d+|pacman)/)) {
      drawcopy -tn @pac_editbuf 16711935 $calc( $gettok(1 4 5 6 7 8 9, $iif(ghost? iswm %i,$calc($remove(%i,ghost) +1),1),32) * $1 ) 0 $1- @pac_edit1 $calc($gettok(%d,1,32) * $1) $calc($gettok(%d,2,32) * $2 + $2)
    }
    elseif ($regex(%d,/^teleport(\d+?) ([^ ]+?)\.([^ ]+?) (.)?$/)) {
      var %x $gettok(%i,1,46), %y $gettok(%i,2,46)
      drawrect -frn @pac_edit1 64512 0 $calc((%x $iif($regml(4) == l,+1,$iif($v1 == r,-1))) * $1) $calc((%y $iif($regml(4) == d,-1,$iif($v1 == u,+1))) * $2 + $2) $1-2
      drawtext -nr @pac_edit1 0 verdana 12 $calc((%x $iif($regml(4) == l,+1,$iif($v1 == r,-1))) * $1 + $iif($len($regml(1)) != 1,-2,+3)) $calc((%y $iif($regml(4) == d,-1,$iif($v1 == u,+1))) * $2 + $2) $regml(1)
    }
    elseif ($regex(%d,/^iteleport(\d+?) ([^ ]+?)\.([^ ]+?)$/)) {
      var %x $gettok(%i,1,46), %y $gettok(%i,2,46)
      drawcopy -tn @pac_editbuf 16711935 $calc(10 * $1) 0 $1- @pac_edit1 $calc(%x * $1) $calc(%y * $2 + $2)
      drawtext -nr @pac_edit1 0 verdana 12 $calc((%x $iif($regml(4) == l,+1,$iif($v1 == r,-1))) * $1 + $iif($len($regml(1)) != 1,-2,+3)) $calc((%y $iif($regml(4) == d,-1,$iif($v1 == u,+1))) * $2 + $2) $regml(1)
    }
    inc %a
  }
  if ($hget(pacclienttmp,spawn)) {
    tokenize 32 $1- $replace($v1,.,$chr(32))
    if ($3 == d) drawline -nr @pac_edit1 255 1 $calc($4 * $1) $calc($5 * $2 -1 + $2) $calc($4  * $1 + 5 * $1) $calc($5 * $2 -1 + $2)
    elseif ($3 == r) drawline -nr @pac_edit1 255 1 $calc($4 * $1 -1) $calc($5 * $2 + $2) $calc($4 * $1 -1) $calc($5 * $2 + 5 * $2 + $2)
    elseif ($3 == l) drawline -nr @pac_edit1 255 1 $calc($4 * $1 - 0) $calc($5 * $2 + $2) $calc($4 * $1 - 0) $calc($5 * $2 + 5 * $2 + $2)
    else drawline -nr @pac_edit1 255 1 $calc($4 * $1) $calc($5 * $2 + $2) $calc($4 * $1 + 5 * $1) $calc($5 * $2 + $2)
  }
  drawdot @pac_edit1
  renwin @pac_edit1 @Pacman $+ $chr(160) $+ Preview
  window -c @pac_editbuf
}

; START CLIENT
alias pacclient_start {
  if ($dialog(pacman)) {
    var %i
    if (!$5) && (!$did(pacman,76)) {
      %i = $input(Please provide a valid nickname or the $iif($hget(pacserv),server creation,connection) will be aborted,eu,Pacman game - Nickname request)
      if ($regsubex(%i,/^\s|\s$/g,) == $null) { pacserv_stop | return }
    }
    hadd -m pacclient nick $iif(%i != $null,%i,$iif($5 != $null,$5,$did(pacman,76)))
    did -f pacman 14, $+ $iif(!$hget(pacserv),3) 
    did -t pacman 57
    hadd pacclient ip $1
    hadd pacclient port $2
    hadd pacclient pass $3
    hadd pacclient key $4
    sockopen $iif($did(pacman,99),-n) pacclient $1-2
    dialog -t pacman Pacman $pac_version - You are connecting to $hget(pacclient,ip) - $hget(pacclient,nick)
    .timer -ho 1 100 did -ra pacman 2 Game status: connecting... - nickname: $unsafe($hget(pacclient,nick))
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Connecting to $1 on port $2... $+ $crlf
    did -b pacman 36,37,45,50,23,39,47,10,12,11,79,40, $+ $iif(!$hget(pacserv),38)
    if ($prop != d) did -v pacman 95
    if ($did(pacman,10).sel) did -u pacman 10 $v1
    pacclient_selnick
    return 1
  }
}

; INPUT
alias client_input {
  var %ok
  if ($did(pacman,14,0).lines == 1) {
    if ($regex($did(pacman,14),/^\/connect (.*):(\d+)/)) {
      var %p $regml(1) , $regml(2) , $input(Leave the field empty if no password is required,upe,Pacman - Password request) , $gettok($v2,3,32),%ok 1
      pacclient_stop | noop $pacclient_start( [ %p ] ) 
      ;did -fr pacman 14
    }
    elseif (/clear == $did(pacman,14)) {
      did -ra pacman 16 $+([,$time(HH:nn:ss),]) Game: you cleared the window.. $+ $crlf
      var %ok 1
    }
  }
  if ($sock(pacclient).status == active) && (!%ok) {
    var %a 1,%b $did(pacman,14).lines
    while (%a <= %b) {
      var %t $did(pacman,14,%a)
      if (%t != $null) {
        sockwrite -n pacclient txt %t
        var %r $player?
        did -a pacman 16 $+([,$time(HH:nn:ss),]) $+(<,$iif(%r,$regml(1)),$hget(pacclient,nick),>) %t $+ $crlf
      }
      inc %a
    }
  }
  if ($dialog(pacman)) did -fr pacman 14
}


;STOP CLIENT
alias pacclient_stop {
  sockclose pacclient
  sockclose pacuclient
  .timerpacclient off
  .timerpacclientudpka off
  window -c @Pacman
  window -c @pacani_buf
  window -c @pac_debug
  window -c @Pac_buf
  window -c @Pac_rbuf
  unset %pacclient_retry
  window -c @Pacman $+ $chr(160) $+ Preview
  if ($dialog(pacman)) {
    did -hbg pacman 58 $qt($scriptdirloff.png)
    did -e pacman 10,12,40,38,45,40,36,37,50,39,45,23,79,76, $+ $iif($did(pacman,23) != $null,47)
    did -h pacman 95
    did -c pacman 21 99
    did -b pacman 15,17,21,22,29,26,44,53,48,30,31,32,55,58,62,88,49,54,52,42
    if ($iptype($did(pacman,12))) && ($regsubex($did(pacman,79),/\d+/g,) == $null) && ($did(pacman,79) isnum 1-65535) did -e pacman 11
    else did -b pacman 11
    did -u pacman 55
    did -b pacman 58
    did -r pacman 15,26,19,29,18,6,22,62,49,28,84
    if ($hget(pacserv)) && (!$did(pacman,45).state) pacserv_stop
    if ($dialog(pacman).tab == 3) {
      dialog -t pacman Pacman $pac_version - You are not connected to any game
      did -ra pacman 2 Game status: not connected
    }
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Disconnected $+ $iif($hget(pacclient,kick),: You have been kicked,$iif($hget(pacclient,ban),: You have been banned)) $+ $crlf
  }
  hfree -w pacclient*
}

;CONNEXION
alias -l pacclient_connection {
  did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: $1 joined the game $+ $crlf
  pacclient_nicks $hget(pacclient,nicks) $1
}

;TRACKING USERS STATES FOR NICKLIST
alias -l pacclient_selnick {
  if ($show) hadd pacclient selnick $1-
  .timerselnick -mo 1 700 if (($window(@pacman)) && ($hget(pacclient,game) == start)) || (!$dialog(pacman)) .timerselnick off $(|) else .pacclient_selnick
  var %a 1
  while ($did(pacman,15,%a)) {
    if ($istok($hget(pacclient,selnick),$remove($v1,@,~),32)) {
      if (!$did(pacman,15,%a).cstate) did -s pacman 15 %a
    }
    elseif ($did(pacman,15,%a).cstate) did -l pacman 15 %a
    inc %a
  }
}

; KICK
alias -l pacclient_kick {
  if ($1 != $hget(pacclient,nick)) {
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: $1 has been kicked $+ $crlf
    pacclient_nicks $regsubex($hget(pacclient,nicks),/(?:^|[@~]|( )) $+ $1(?:( )|$)/,\1\2)
  }
  else { hadd pacclient kick 1 | pacclient_stop }
}

alias -l pacclient_ban {
  if ($1 != $hget(pacclient,nick)) {
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: $1 has been banned $+ $crlf
    pacclient_nicks $regsubex($hget(pacclient,nicks),/(?:^|[@~]|( )) $+ $1(?:( )|$)/,\1\2)
  }
  else { hadd pacclient ban 1 | pacclient_stop }
}

;CONFIRM SUCCESSFUL
alias -l pacclient_preadyok did -g pacman 58 $qt($scriptdirlon.png)
alias -l pacclient_pnreadyok did -g pacman 58 $qt($scriptdirloff.png)
alias -l pacclient_sreadyok did -g pacman 58 $qt($scriptdirlon.png)
alias -l pacclient_snreadyok did -g pacman 58 $qt($scriptdirloff.png)

;DECONNEXION
alias -l pacclient_deconnection {
  did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: $1 left the room $+ $crlf
  pacclient_nicks $2-
}

;CHAT
alias -l pacclient_txt {
  if ($1 != $hget(pacclient,nick)) && ($dialog(pacman)) {
    var %r $regex($hget(pacclient,nicks),/([@~]) $+ $1(?: |$)/)
    did -a pacman 16 $+([,$time(HH:nn:ss),]) < $+ $iif(%r,$regml(1)) $+ $1> $2- $+ $crlf
    if ($dialog(pacman).tab != 3) && ($did(pacman,93).state) .timerpacflicktab2 -oh 0 500 if ($dialog(pacman)) did -ra pacman 3 $!iif($did(pacman,3) != Game,Game) $(|) else .timerpacflicktab2 off
  }
}

;DISPLAY WHEN A GAME START/stop
alias -l pacclient_game {
  if ($1 == start) { did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Starting. $+ $crlf | did -b pacman 55 | if (!$player?) did -b pacman 58 | if ($did(pacman,91).state) && (!$did(pacman,92).state) dialog -i pacman }
  else {
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Stopped ( $+ $2- $+ ) $+ $crlf
    did -e pacman 96
    did -u pacman 58
    hdel -w pacclient score
    hdel pacclient invinctime
    hadd pacclient invincstate 0
    hdel pacclient invinc
    hdel pacclient shield
    hdel pacclient stunt
    hdel pacclient flipv
    hdel pacclient fliph
    hdel pacclient flip
    hdel pacclient bossbartime
    hdel pacclient bossbar
    hdel pacclient upck
    .timerpacclientudpka off
    .timerpacclientgo off
    if ($window(@pacman)) {
      if ($regex($2-,/(?:error|too few|^$)/i)) {
        hadd pacclient anim 0
        hadd pacclient cli 0
        tokenize 32 animtime clitime +1 +1time +1state eatbartime eatbar letteri letterm letterc letterr kmt kml rplshoot gametime pausetime
        hdel pacclient $*
        var %pic $scriptdirabort.png
        var %bw $window(@pac_rbuf).dw,%bh $window(@pac_rbuf).dh
        var %picwidth = $pic(%pic).width, %picheight = $pic(%pic).height
        if (%picwidth > %bw) var %picwidth = $v2, %picheight = $calc(%picwidth / $v2 * %picheight)
        if (%picheight > %bh) var %picheight = $v2, %picwidth = $calc(%picheight / $v2 * %picwidth)
        var %xd $int($calc((%bw - %picwidth) / 2)),%yd $int($calc((%bh - $hget(pacclient,height) - %picheight) / 2)) + $hget(pacclient,height)
        hadd pacclient msg drawpic -tsmc @pac_rbuf 16777215 %xd %yd %picwidth %picheight $qt(%pic)
        pacman_loop $sizesprite $hget(pacclient,height) $hget(pacclient_db,background) $hget(pacclient_db,size)
        pacman_draw
      }
      pac_saverecord
      titlebar @pacman You can close this window.
    }
    if ($hget(pacclient,owner)) {
      did -ec pacman 55
      sockwrite -n pacclient sready $did(pacman,55).state
      did $iif($did(pacman,55).state,-b,-e) pacman 26,48,30,31,32,29,44,21,22,53,62,49
      did -h pacman 86
      if ($v1) {
        did -u pacman 26,29,49
        did -b pacman 42,52,54,43,49
      }
      else did -b pacman 44,48,30,31,32
    }
  }
  hadd pacclient game $1
}


;PLAYER OR SPECTATOR - UDP
alias -l pacclient_play {
  hadd pacclient udp $1
  hadd pacclient keyp $2
}

;ERROR HANDLING
alias -l pacclient_error {
  if ($1 == nick) .timer -ho 1 0 pacclient_enick $unsafe($2-)
  else .timer -ho 1 0 pacclient_epass
}

;PASS ERROR
alias -l pacclient_epass {
  noop $input(Missing or incorrect password $+ $chr(44) try again,uew,Pacman - Pass error,$hget(pacclient,pass))
  if ($! != $null) {
    hadd pacclient pass $v1
    sockwrite -n pacclient login $+($hget(pacclient,nick),@,$hget(pacclient,pass),@,$hget(pacclient,key))
  }
  else pacclient_stop
}

;NUMBER OF LIVES
alias -l pacclient_paclives did -ra pacman 19, $+ $iif($did(pacman,22).enabled,22) $1

;MAPS
alias -l pacclient_maps {
  did -r pacman 18
  var %a 1
  while ($gettok($1-,%a,4)) {
    did -a pacman 18 $v1
    if ($didwm(pacman,26,$v1)) did -s pacman 26 $v1
    inc %a
  }
}

;NICK ERROR
alias -l pacclient_enick {
  noop $input($hget(pacclient,nick) is $+($1-,$crlf,Please provide another nickname,$crlf),uew,Pacman - Nick error,$hget(pacclient,nick))
  if ($! != $null) {
    hadd pacclient nick $v1
    sockwrite -n pacclient login $+($hget(pacclient,nick),@,$hget(pacclient,pass),@,$hget(pacclient,key))
  }
  else {
    pacclient_stop
    if ($hget(pacserv)) pacserv_stop
  }
}

;ERROR - SERVER CONF IS NOT OK
alias -l pacclient_eready {
  if ($1 != 2) {
    did -e pacman 26,29,21,22,53,62,54,55,49
    did -u pacman 55
    if ($1 == 1) {
      if ($did(pacman,18,1) == $null) did -vra pacman 86 You must set at least one map
      elseif (!$regex($hget(pacclient,nicks),/[@~]/)) did -vra pacman 86 You must set at least one player
    }
  }
  else {
    did -cb pacman 55
    did -vra pacman 86 You can't change the status of the server while a game is playing
  }
}


;SERVER IS READY
alias -l pacclient_sready {
  ;did -ra pacman 58 $iif($player?,Ready,Watch)
  if ($1) {
    ;beep 2 250
    did -ev pacman 58
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Server is ready! $+ $crlf
    if ($hget(pacclient,owner)) {
      did -ec pacman 55
      did -b pacman 26,48,30,31,32,29,44,21,22,53,62,49
      did -h pacman 86
      did -u pacman 26,29,49
      did -b pacman 42,52,54,43,49
    }
  }
  else  {
    did -gbh pacman 58 $qt($scriptdirloff.png)
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Server is being configured.. $+ $crlf
  }
}

;USER IS OWNER
alias -l pacclient_owner {
  hadd pacclient owner 1
  did -e pacman 53,26,22,21,29,55,62,49,21
  didtok pacman 62 32 0 1 2 3 4 5 6
  did -ck pacman 62 1
  did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Recognized as owner. $+ $crlf
}

;HELLO
alias -l pacclient_hello {
  did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Connected to $hget(pacclient,ip) on port $hget(pacclient,port) $+ $crlf 
  dialog -t pacman Pacman $pac_version - You are connected to $hget(pacclient,ip) - $hget(pacclient,nick)
  did -ra pacman 2 Game status: connected - nickname: $hget(pacclient,nick)
  did -e pacman 88
  did -ra pacman 84 0
  did -bra pacman 76 $hget(pacclient,nick)
  var %r $mid($read($scriptdirpacman.mrc,tn,1),2)
  write -dl1 $qt($scriptdirpacman.mrc)
  write -il1 $qt($scriptdirpacman.mrc) ; $+ $gettok(%r,1-5,32) $hget(pacclient,nick) $gettok(%r,7-,32)
  .reload -rs $qt($scriptdirpacman.mrc)
}

;NICKS
alias -l pacclient_nicks {
  did -r pacman 15,29
  didtok pacman 15 32 $1-
  if ($hget(pacclient,owner)) { didtok pacman 29 32 $1- | did -b pacman 48,30,31,32,80 }
  hadd pacclient nicks $1-
}

alias -l pacclient_gaming did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: A game is being played $+ $crlf

;LIVES
alias pacclient_lives {
  set -l %l $hget(pacclient,lives)
  hadd pacclient lives $1
  if ($window(@pacman)) && (%l != $1) {
    var %x $hget(pacclient,height) - 20,%c 64512
    if (%l > $1) %c = 255
    .timerpacclientlives -mo 10 100 if (!$window(@pacman)) return $(|) drawrect -fnr @pac_rbuf 0 0 22 %x $width(x $+ $calc($1 + 1),fixedsys,9) 15 $(|) drawtext -rn @pac_rbuf $!pacclient_interpol( %c ,13816530,$calc(1 - 0. [[ $!+ [[ $!timer(pacclientlives).reps ]] ]] )) fixedsys 9 22 %x x $+ $1
  }
}

; OPENING/CLOSING FILE - INIT
alias -l pacclient_mapping {
  var %1- $1-
  ;sizef md5 sizew name
  if ($did(pacman,96).state) {
    if (!$isdir($scriptdirrecords)) mkdir $qt($scriptdirrecords)
    var %file $scriptdirrecords\ $+ $replace($date,/,_) $+ -- $+ $replace($time,:,_)
    hadd pacclient recordfile %file
    hadd pacclient recordcount 0
  }
  .timerpacclient off
  hfree -w pacclienti
  tokenize 32 anim stack msg dead gametime invinc invincstate invinctime pausetime shield stunt invis invisstate flip flipv fliph
  scon 0 if ($hget(pacclient, $* )) hdel pacclient $*
  tokenize 32 %1-
  noop $regex($5-,/(.+)\.plvl/)
  did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Playing map: $regml(1) $+ $crlf
  did -b pacman 58,96
  var %m $2,%f
  noop $findfile($scriptdirmaps\,*.plvl,0,if ($md5($1-,2) == %m) [ $({,0) ] var %f $1- [ $(|) ] halt [ $(},0) ] )
  if (%f != $null) {
    hadd pacclient map %f
    pacclient_initwin
    pacman_init
    return
  }
  var %b $ctime
  if (!$isdir($scriptdirmaps)) mkdir $qt($scriptdirmaps\)
  var %map $+($regml(1),$iif($isfile($+($scriptdirmaps\,$regml(1),.plvl)),%b),.plvl)
  .fopen -no pacmap $qt($scriptdirmaps\ $+ %map)
  hadd pacclient map $scriptdirmaps\ $+ %map
  sockopen pacmap $hget(pacclient,ip) $hget(pacclient,port)
  sockmark pacmap %m
}

; DOWNLOADING MAP
on *:sockopen:pacmap:if (!$sockerr) { sockwrite -n pacmap mapping $hget(pacclient,keyp) $sock(pacmap).mark } | else echo -s pacman error pacdl sockopen $sockerr
on *:sockread:pacmap:if (!$sockerr) { sockread $sock(pacmap).rq &r | .fwrite -b pacmap &r } | else echo -s pacman error pacdl sockread $sockerr
on *:sockclose:pacmap:.fclose pacmap | pacclient_initwin | pacman_init

;INIT WINDOWS
alias -l pacclient_initwin {
  hfree -w pacclient_db
  hmake pacclient_db
  hload pacclient_db $qt($hget(pacclient,map))
  tokenize 32 $hget(pacclient_db,size) $sizesprite
  var %tiles $qt($scriptdirpactiles.png),%anitiles $qt($scriptdirpacanitiles.png)
  var %nw $iif($regex($hget(pacclient,nicks),/@/g) < $calc($regex($hget(pacclient,nicks),/~/g) + $iif($hget(pacclient,cpu),1)),$v2,$v1),%yw $calc(%nw * 18 + 30)
  hadd pacclient height %yw
  if ($window(@pacman)) .timer -ho 1 0 window -af $+ $iif($did(pacman,92).state,o) @pacman -1 -1 $1 $calc($2 + %yw)
  else window -CBdfkpa $+ $iif($did(pacman,92).state,o) +xstn @Pacman -1 -1 $1 $calc($2 + %yw) / "" $scriptdirpacman.ico
  if ($window(@pacman).state == minimized) window -r @pacman
  if ($did(pacman,96).state) hadd -m pacrecord size $window(@pacman).bw $window(@pacman).bh
  drawrect -fr @pacman 0 0 0 0 $window(@pacman).dw $hget(pacclient,height)
  window -Bdfhp @pac_rbuf 50 50 $1 $calc($2 + %yw)
  window -Bdfhp @Pacani_buf -1 -1 $pic(%anitiles).width $pic(%anitiles).height
  window -Bdfhp @Pac_buf -1 -1 $1 $calc($2 + $4)
  ;window -ehj500000 @pac_debug
  drawpic -c @Pac_buf 0 0 %tiles
  drawpic -c @Pacani_buf 0 0 %anitiles
}

; SOON
alias -l pacclient_soon {
  if ($window(@pacman)) && ($1 isnum 1-3) {
    titlebar @pacman $1
    var %xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdir $+ $1.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - $hget(pacclient,height)) / 2 - $pic($scriptdir $+ $1.png).height / 2)) + $hget(pacclient,height)
    hadd pacclient msg drawpic -ctn @pac_rbuf 16777215 %xd %yd $qt($scriptdir $+ $1.png)
    if ($1 == 3) {
      ;  splay pacman opening song.mp3
      var %a 1
      while ($hget(pacclienti,%a).item) {
        hadd pacclient playlist $hget(pacclient,playlist) $v1
        inc %a
      }
      pacclient_panel
    }
    pacman_loop $sizesprite $hget(pacclient,height) $hget(pacclient_db,background) $hget(pacclient_db,size)
    pacman_draw 
  }
}

;START GAME
alias -l pacclient_startg {
  if ($window(@pacman)) {
    titlebar @Pacman Go!
    var %xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdirgo.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - $hget(pacclient,height)) / 2 - $pic($scriptdirgo.png).height / 2)) + $hget(pacclient,height)
    hadd pacclient msg drawpic -ct @pac_rbuf 16777215 %xd %yd $qt($scriptdirgo.png)
    .timerpacclientgo -ho 1 100 if ($window(@pacman)) titlebar @Pacman - $!hget(pacclient,nick) - Playing map: $left($nopath($hget(pacclient,map)),-5) $(|) if ($hget(pacclient,msg)) hdel pacclient msg
    hadd pacclient animtime $ticksqpc
    hadd pacclient gametime $ticksqpc
    pacman_loop $sizesprite $hget(pacclient,height) $hget(pacclient_db,background) $hget(pacclient_db,size) 1
  }
}

;STOP GAME
alias -l pacclient_stopg {
  .timerpacclient off
  .timerpacclientbonus off
  .timerpacclientlives off
  .timerpacclientudpka off
  did -u pacman 58
  hfree -w pacclient_db
  sockclose pacuclient
  close -@ @pacman @pac_buf @pacani_buf @pac_rbuf
  hfree -w pacclienti
  tokenize 32 msg anim animtime +1 +1time +1state dead gametime movewin eatbar eatbartime coin cticks height pausetime invinc invinctime invincstate playlist stunt shield invis letterm letteri letterr letterc kml kmt rplshoot flip flipv fliph replayfile
  scon 0 if ($hget(pacclient, $* )) hdel pacclient $*
}

;SOCKOPEN
on *:sockopen:pacclient: {
  if ($dialog(pacman)) {
    if (!$sockerr) {
      did -e pacman 15,17,14
      sockwrite -n $sockname login $+($hget(pacclient,nick),@,$hget(pacclient,pass),@,$hget(pacclient,key))
    }
    else { 
      did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: Unable to connect to $hget(pacclient,ip) on port $hget(pacclient,port) ( $+ $sock($sockname).wsmsg $+ ) $crlf
      if (%pacclient_retry) {
        tokenize 32 $v1
        .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe($1) , $unsafe($2) ,,, $unsafe($hget(pacclient,nick)) )
      }
      else pacclient_stop     
    }
  }
}

alias pac_saverecord {
  if ($hget(pacrecord)) && ($hget(pacrecord,0).item > 1) {
    hsave -B pacrecord $qt($hget(pacclient,recordfile))
    if ($window(@pacman)) {
      drawtext -rpn @pac_rbuf 8355711 fixedsys 9 500 $calc($hget(pacclient,height) - 20) $chr(31) $+ Watch a replay of the game
      hadd pacclient replayfile $hget(pacclient,recordfile)
    }
    hfree pacrecord
    hdel pacclient recordcount
    hdel pacclient recordfile
    hdel pacclient recordticks
  }
}

menu @pacman {
  sclick: {
    if ($inrect($mouse.x,$mouse.y,500,$calc($hget(pacclient,height) - 20),208,16)) {
      pac_record $hget(pacclient,replayfile)
      hdel pacclient replayfile
    }
  }
}

;SOCKREAD
on *:sockread:pacclient:{
  if ($sockerr) { .timer -ho 1 0 pacclient_stop | return }
  var %r
  sockread %r
  tokenize 32 %r
  if ($window(@pac_debug)) && (%r != $null) aline @pac_debug $+([,$time(HH:nn:ss),]) CLIENT TCP RCVD %r
  if ($1 == posd) hadd -m pacclienti $2-
  elseif ($1 == end) {
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: The server has been shutdown. $+ $crlf
    pacclient_stop
  }
  elseif ($1 == upck) {
    did -a pacman 16 $+([,$time(HH:nn:ss),]) Stat: received $hget(pacclient,upck) out of $2 udp packets $+ $crlf
    hdel pacclient upck
  }
  elseif ($window(@pacman)) {
    if ($1 == gscount) {
      hadd pacclient gscount
    }
    elseif ($1 == busyport) {
      set -l %xs $gettok($sizesprite,1,32)
      set -l %ys $gettok($sizesprite,2,32)
      set -l %1 $hfind(pacclient_db,iteleport $+ $2*,1,w).data
      set -l %2 $hfind(pacclient_db,iteleport $+ $2*,2,w).data
      drawcopy -t @Pac_buf 16711935 $calc($iif($3,11,10) * %xs) 0 %xs %ys @Pac_buf $calc($token(%1,1,46) * %xs) $calc($token(%1,2,46) * %ys + %ys)
      drawcopy -t @Pac_buf 16711935 $calc($iif($3,11,10) * %xs) 0 %xs %ys @Pac_buf $calc($token(%2,1,46) * %xs) $calc($token(%2,2,46) * %ys + %ys)
    }
    elseif ($istok(kmt flipv fliph rplshoot shield invis stunt,$1,32)) hadd pacclient $1-
    elseif ($1 == flipadd) hadd pacclient flip $hget(pacclient,flip) $2
    elseif ($1 == flipdel) hadd pacclient flip $remtok($hget(pacclient,flip),$2,32)
    elseif ($1 == taunt) {
      hadd pacclient taunt $+ $2 $3-
      hadd pacclient tauntticks $+ $2 $ticksqpc
    }
    elseif ($1 == invinc) {
      hadd pacclient invinc $2-
      if ($2-) && (!$hget(pacclient,invinctime)) hadd pacclient invinctime $ticksqpc
    }
    elseif ($1 == +1) {
      tokenize 32 $hget(pacclient_db,background) $sizesprite $replace($2,.,$chr(32)) $3
      if ($6) {
        hadd pacclient_db $4. $+ $5 +1
        drawcopy -t @pacani_buf 16711935 $calc(5 * $2) 0 $2-3 @pac_buf $calc($4 * $2) $calc($5 * $3 + $3)
        hadd pacclient +1 $4 $5
        hadd pacclient +1time $ticksqpc
      }
      else {
        hdel pacclient_db $4. $+ $5
        hdel pacclient +1
        hdel pacclient +1state
        hdel pacclient +1time
        drawrect -frn @pac_buf $1 0 $calc($4 * $2) $calc($5 * $3 + $3) $2 $3
      }
    }
    elseif ($1 == heart) {
      tokenize 32 $hget(pacclient_db,background) $sizesprite $replace($2,.,$chr(32)) $3
      if ($6) {
        hadd pacclient_db $4. $+ $5 heart
        drawcopy -t @pacani_buf 16711935 $calc(5 * $2) 0 $2-3 @pac_buf $calc($4 * $2) $calc($5 * $3 + $3)
      }
      else {
        hdel pacclient_db $4. $+ $5
        drawrect -frn @pac_buf $1 0 $calc($4 * $2) $calc($5 * $3 + $3) $2 $3
      }
    }
    elseif ($1 == bonus) {
      tokenize 32 $hget(pacclient_db,background) $sizesprite $replace($3,.,$chr(32)) $4 $replace($5,.,$chr(32))
      if ($6) {
        hadd pacclient_db $4. $+ $5 $7-
        drawcopy -t @pacani_buf 16711935 $calc($7 * $2) $calc($8 * $3) $2-3 @pac_buf $calc($4 * $2) $calc($5 * $3 + $3)
      }
      else {
        hdel pacclient_db $4. $+ $5
        drawrect -fr @pac_buf $1 0 $calc($4 * $2) $calc($5 * $3 + $3) $2 $3
      }
    }
    elseif ($findtok(fraise fraise1 grenade apple banana orange,$1,32)) {
      tokenize 32 $sizesprite $replace($2,.,$chr(32)) $1
      hadd pacclient_db $3. $+ $4 $5
      drawcopy -t @pacani_buf 16711935 $calc(($v1 - 1) * $1) $calc(8 * $2) $1-2 @pac_buf $calc($1 * $3) $calc($2 * $4 + $2)
    }
    elseif ($1 == pause) {
      if ($hget(pacclient,pausetime)) {
        .timerpacclientudpka off
        set -l %t $ticksqpc - $hget(pacclient,pausetime)
        tokenize 32 gametime eatbartime bossbartime
        hinc pacclient $* %t
        hdel pacclient pausetime
        hdel pacclient msg
      }
      else {
        hadd pacclient pausetime $ticksqpc
        set -l %pic $scriptdirpaused.png
        set -l %bw $window(@pac_rbuf).dw
        set -l %bh $window(@pac_rbuf).dh
        set -l %picwidth $pic(%pic).width
        set -l %picheight $pic(%pic).height
        if (%picwidth > %bw) {
          %picwidth = $v2
          %picheight = $calc(%picwidth / $v2 * %picheight)
        }
        if (%picheight > %bh) {
          %picheight = $v2
          %picwidth = $calc(%picheight / $v2 * %picwidth)
        }
        set -l %xd $calc((%bw - %picwidth) // 2)
        set -l %yd $calc((%bh - $hget(pacclient,height) - %picheight) // 2) + $hget(pacclient,height)
        hadd pacclient msg drawpic -tsmcn @pac_rbuf 16777215 %xd %yd %picwidth %picheight $qt(%pic)
        .timerpacclientudpka 0 25 !sockudp -kn pacuclient $hget(pacclient,ip) $hget(pacclient,udp) move $hget(pacclient,keypl) $hget(pacclient,nick)
      }
    }
    elseif ($1 == score) { 
      set -l %ht $hget(pacclient,height)
      drawrect -fnr @pac_rbuf 0 0 200 $calc(%ht - 20) $width(Score : $hget(pacclient,score),fixedsys,9) 15
      $2 pacclient score $3
      drawtext -rn @pac_rbuf 8355711 fixedsys 9 200 $calc(%ht - 20) Score : $hget(pacclient,score)
      if ($4) pacclient_updpnlspecpl $4
    }
    elseif ($1 == pacwin) {
      if ($window(@pacman)) {
        hdel pacclient gametime
        titlebar @Pacman Pacman wins
        if ($player?) {
          if ($regml(1) == @) var %f win ,%xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdirwin.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - $hget(pacclient,height)) / 2 - $pic($scriptdirwin.png).height / 2)) + $hget(pacclient,height)
          else var %f lose, %xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdirlose.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - $hget(pacclient,height)) / 2 - $pic($scriptdirlose.png).height / 2)) + $hget(pacclient,height)
          hadd pacclient msg drawpic -ctmn @pac_rbuf 16777215 %xd %yd $qt($+($scriptdir,%f,.png))
        }
        pac_saverecord
        hadd pacclient anim 0
        hadd pacclient cli 0
        tokenize 32 animtime clitime +1 +1time +1state eatbartime eatbar letteri letterm letterc letterr kmt kml rplshoot
        hdel pacclient $*
      }
    }
    elseif ($1 == part) {
      var %xs $gettok($sizesprite,1,32),%ys $gettok($sizesprite,2,32),%yy,%xx 4,%ww 56,%n $remove($2,pacman,ghost),%hh $hget(pacclienti,$2),%nbp $gettok(%hh,8,32),%scp $gettok(%hh,9,32),%w $width($gettok(%hh,7,32),fixedsys,9)
      if (ghost* iswm $2) {
        inc %xx 260
        %ww = 16
        if ($hget(pacclient,cpu) > 1) {
          if (%n > 2) {
            dec %n $calc($hget(pacclient,cpu) - 1)
          }
        }
      }
      %yy = $calc(2 + %n * %ys - %ys + %n * 2 +7)
      drawline -nr @pac_rbuf 127 3 %xx %yy $calc(%xx + %w + $width(%nbp,fixedsys,9) + $width(%scp,fixedsys,9) + %ww) %yy
      ; hdel -w pacclienti $2
      hadd pacclient playlist $remtok($hget(pacclient,playlist),$2,32)
    }
    elseif ($1 == dead) { 
      set -l %ys $gettok($sizesprite,2,32)
      set -l %n $remove($2,pacman,ghost)
      drawtext -nr @pac_rbuf 127 fixedsys 24 4 $calc(2 + %n * %ys - %ys + %n * 2 -7) X
      hadd pacclient dead $hget(pacclient,dead) $2
      hadd pacclient playlist $remtok($hget(pacclient,playlist),$2,32)
    }
    elseif ($1 == paclose) {
      if ($window(@pacman)) {
        hdel pacclient gametime
        titlebar @Pacman Pacman loses
        if ($player?) {
          if ($regml(1) == @) var %f lose ,%xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdirlose.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - $hget(pacclient,height)) / 2 - $pic($scriptdirlose.png).height / 2)) + $hget(pacclient,height)
          else var %f win, %xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdirwin.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - $hget(pacclient,height)) / 2 - $pic($scriptdirwin.png).height / 2)) + $hget(pacclient,height)
          hadd pacclient msg drawpic -ctmn @pac_rbuf 16777215 %xd %yd $qt($+($scriptdir,%f,.png))
        }
        pac_saverecord
        hadd pacclient anim 0
        hadd pacclient cli 0
        tokenize 32 animtime clitime +1 +1time +1state eatbartime eatbar letteri letterm letterc letterr kmt kml rplshoot
        hdel pacclient $*
      }
    }
    elseif ($1 == cli) {
      if ($2) { 
        hadd pacclient cli 1
        hadd pacclient clitime $ticksqpc
      }
      else {
        hadd pacclient cli 0
        hdel pacclient clitime
        hdel pacclient eatbar
        hdel pacclient eatbartime
        drawline -rn @pac_rbuf 0 2 0 $calc($hget(pacclient,height) - 24) $window(@pac_rbuf).dw $calc($hget(pacclient,height) - 24)
      }
    }
    elseif ($1 == lshield) {
      hadd pacclient lshield $+ $2 1
      hadd pacclient lshieldticks $+ $2 $ticksqpc
    }
    elseif ($1 == pladd) {
      hadd pacclient playlist $hget(pacclient,playlist) $2
    }
    elseif ($1 == pldel) hadd pacclient playlist $remtok($hget(pacclient,playlist),$2,32)
    elseif ($1 == @mirc) {
      if ($2) {
        hadd pacclient playlist $hget(pacclient,playlist) $1
        hadd pacclient bossbar $calc($window(@pac_rbuf).dw / $3 / 16) $window(@pac_rbuf).dw
        hadd pacclient bossbartime $ticksqpc
        drawline -rn @pac_rbuf 255 3 0 $calc($hget(pacclient,height) - 22) $window(@pac_rbuf).dw $calc($hget(pacclient,height) - 22)
      }
      else {
        hadd pacclient playlist $remtok($hget(pacclient,playlist),$1,32)
        hdel pacclienti @mirc
        hdel pacclient kmt
        hdel pacclient kml
      }
    }
    elseif ($1 == mirckill) {
      hadd pacclienti $2 $puttok($hget(pacclienti,$2),-2,5,32)
      hadd pacclient dead $hget(pacclient,dead) $2
      if ($window(@pac_debug)) aline @pac_debug $+([,$time(HH:nn:ss),]) mirckill
      .timerpacclientmirckill $+ $2 -ho 1 1500 hadd pacclient playlist $!remtok($hget(pacclient,playlist), $2 ,32)
      if ($gettok($hget(pacclienti,$2),7,32) == cpu) {
        set -l %n $remove($2,ghost)
        set -l %yy -2
        tokenize 32 $sizesprite
        set -l %xx $calc(259 + (%n - 1) * $1 + (%n -1) * 2)
      }
      else {
        set -l %sh $hfind(pacclienti,& & & & & & cpu & & & &,0,w).data - 1
        set -l %n $mid($2,6)
        set -l %nn %n - $iif(%sh > 0,$v1,0)
        tokenize 32 $sizesprite
        set -l %yy $calc(2 + (%nn - 1) * $2 + %nn * 2)
        set -l %xx 259
        dec %yy 6
      }
      drawtext -nr @pac_rbuf 127 fixedsys 24 %xx %yy X
    }
    elseif ($1 == kml) {
      hadd pacclient kml $2
      if (!$2) hdel pacclient rplshoot
    }
    elseif ($1 == letter) {
      if $3 {
        hadd pacclient $2 $3
        pacclient_updpnlspecpl $3 $hget(pacclient,height)
      }
      else hdel pacclient $2
    }
    elseif ($1 == eatbar) {
      hadd pacclient eatbar $calc($window(@pac_rbuf).dw / $2 / 16) $window(@pac_rbuf).dw
      hadd pacclient eatbartime $ticksqpc
      drawline -rn @pac_rbuf 64512 2 0 $calc($hget(pacclient,height) - 24) $window(@pac_rbuf).dw $calc($hget(pacclient,height) - 24)
    }
    elseif ($1 == pellet-) || ($1 == pellet+) /

    elseif ($1 == toclear) { scon -r noop $!pacclient_toclear($hget(pacclient_db,background), [ $sizesprite1 ] , $hget(pacclient,height) , [[ $* ]] ) }
    else pacclient_ $+ $$1 $2-
  }
  elseif (!$istok(cli part paclose dead score pause pacwin orange apple banana grenade pellet fraise fraise1 +1 clear eatbar bossbar toclear gscount invinc invis stunt shield bonus kmt kml rplshoot @mirc upck taunt pellet- pellet+ letter flipadd flipdel mirckill,$1,32)) pacclient_ $+ $$1 $2-
}

;MAP SET ERROR
alias -l pacclient_mapseterr {
  did -a pacman 16 $+([,$time(HH:nn:ss),]) Game: The server couldn't set the map from the parameter $1- $+ $crlf
}
alias -l sizesprite1 return 14,14

;SOCKCLOSE
on *:sockclose:pacclient:pacclient_stop
on *:sockwrite:pacuclient:{
  if ($sockerr) {
    echo 4 -s * pacman client udp send error : $v1 $sock($sockname).wsmsg $sock($sockname).wserr
  }
}
on *:sockclose:pacuclient:{
  if ($window(@pac_debug)) aline @pac_debug $timestamp CLIENT UDP SOCKCLOSE $sockerr -- $sock($sockname).wsmsg
}

alias -l pacclient_udp {
  hadd -m pacclienti $4-
  if ($hget(pacclient,teleport $+ $4)) {
    set -l %x $gettok($gettok($v1,1,32),1,46) * $1
    set -l %y $gettok($gettok($v1,1,32),2,46) * $2
    inc %y $2
    drawrect -fr @pac_buf $3 0 %x %y $1-2
    drawrect -fr @pac_rbuf 0 0 $5 $calc($6 + $hget(pacclient,height)) $1-2
    hdel pacclient teleport $+ $4
  }
  if (letter* !iswm $4) && ($10 !isin 10) {
    ;ensure an item is still shown if appearing on a player so instantly being picked up
    if ($hget(pacclient_db,$10) != $null) && ((*coin !iswm $gettok($v1,1,32)) || (pacman* iswm $4)) {
      set -l %x $gettok($10,1,46) * $1
      set -l %y $gettok($10,2,46) * $2
      inc %y $2
      drawrect -fr @pac_buf $3 0 %x %y $1-2
      if ($hget(pacclient_db,$10) == 1 10) {
        drawrect -fr @pac_buf 0 0 %x %y $1-2
        hadd pacclient teleport $+ $4 $10 1
      }
    }
  }
}

;UDPREAD
on *:udpread:pacuclient:{
  if ($sockerr) { echo 4 -s udpread client error: $v1 - $sock(pacuclient).wserr $sock(pacuclient).wsmsg | return }
  sockread -n &a
  if (!$sockbr) { echo -sg udpread client sockbr = 0 | return }
  hinc pacclient upck
  tokenize 95 $bvar(&a,1-512).text
  scon -r if (!$istok($hget(pacclient,dead),$gettok( $* ,1,32),32)) pacclient_udp $sizesprite $hget(pacclient_db,background) $* 
}
alias -l pacclient_updpnlspecpl {
  set -ln %pl $1
  tokenize 32 $sizesprite
  set -ln %n $mid(%pl,7)
  set -ln %y $calc(2 + %n * $2 - $2 + %n * 2)
  set -ln %h $hget(pacclienti,%pl)
  drawrect -fnr @pac_rbuf 0 0 105 %y 142 15
  drawtext -rn @pac_rbuf 16777215 fixedsys 9 105 %y $+($gettok(%h,8,32),$str($chr(160),1),$gettok(%h,9,32))
  set -l %mirc
  if ($hget(pacclient,letterm) == %pl) %mirc = 58m
  if ($hget(pacclient,letterI) == %pl) %mirc = %mirc $+ 4I
  if ($hget(pacclient,letterR) == %pl) %mirc = %mirc $+ 4R
  if ($hget(pacclient,letterC) == %pl) %mirc = %mirc $+ 8C
  if (%mirc) drawtext -prn @pac_rbuf 16515072 fixedsys 9 220 %y %mirc
}

alias -l pacclient_toclear {
  if ($5 == toclear) return
  set -l %x $5 * $2
  set -l %y $6 * $3
  if (?coin iswm $8) {
    drawrect -fnr @pac_rbuf 0 0 60 $calc($4 - 20) $width(Remaining : $hget(pacclient,coin),fixedsys,9) 15
    hdec pacclient coin
    drawtext -rn @pac_rbuf 8355711 fixedsys 9 60 $calc($4 - 20) Remaining : $hget(pacclient,coin)
  }
  elseif ($8 == +1) hdel -w pacclient +1*
  hdel pacclient_db $5. $+ $6
  drawrect -fr @pac_buf $1 0 %x $calc(%y + $3) $2-3
  if (p* iswm $7) pacclient_updpnlspecpl $7 $4
}

;PACMAN INIT
alias -l pacman_init {
  hdel pacclient coin
  var %ht $hget(pacclient,height)
  tokenize 32 $hget(pacclient_db,size) $sizesprite
  titlebar @Pacman Loading...
  hadd pacclient sizewin $1 $calc($2 + %ht)
  drawrect -rfn @Pac_buf $hget(pacclient_db,background) 0 0 $4 $1-2
  var %a 1
  while ($hget(pacclient_db,%a).data != $null) {
    var %v $gettok($v1,1,32),%xy $hget(pacclient_db,%a).item
    if ($findtok(wall scoin bcoin,%v,32)) {
      if ($v1 isin 23) hinc pacclient coin
      drawcopy -tn @Pac_buf 16711935 $calc($replacex($v1,1,0) * $3) 0 $3-4 @Pac_buf $calc($token(%xy,1,46) * $3) $calc($token(%xy,2,46) * $4 + $4)
    }
    elseif (iteleport* iswm %v) drawcopy -tn @Pac_buf 16711935 $calc(10 * $3) 0 $3-4 @Pac_buf $calc($token(%xy,1,46) * $3) $calc($token(%xy,2,46) * $4 + $4)
    inc %a
  }
  if ($hget(pacclient_db,spawn)) {
    tokenize 32 $3-4 $replace($v1,.,$chr(32))
    if ($3 == d) drawline -nr @pac_buf 255 1 $calc($4 * $1) $calc($5 * $2 + $2 -1) $calc($4  * $1 + 5 * $1) $calc($5 * $2 + $2 -1)
    elseif ($3 == r) drawline -nr @pac_buf 255 1 $calc($4 * $1 -1) $calc($5 * $2 + $2) $calc($4 * $1 -1) $calc($5 * $2 + 6 * $2)
    elseif ($3 == l) drawline -nr @pac_buf 255 1 $calc($4 * $1) $calc($5 * $2 + $2) $calc($4 * $1) $calc($5 * $2 + 6 * $2)
    else drawline -nr @pac_buf 255 1 $calc($4 * $1) $calc($5 * $2 + $2) $calc($4 * $1 + 5 * $1) $calc($5 * $2 + $2)
  }
  drawdot @Pac_buf

  drawrect -fnr @pac_rbuf 0 0 0 0 $window(@pac_rbuf).dw $window(@pac_rbuf).dw
  var %xd $int($calc($window(@pac_rbuf).dw / 2 - $pic($scriptdirload.png).width / 2)),%yd $int($calc(($window(@pac_rbuf).dh - %ht) / 2 - $pic($scriptdirload.png).height / 2)) + %ht
  drawpic -ctn @pac_rbuf 16777215 %xd %yd $qt($scriptdirload.png)
  drawcopy -n @Pac_buf 0 $2 $hget(pacclient_db,size) @pac_rbuf 0 %ht
  drawdot @pac_rbuf
  pacman_draw
  hdel pacclient playlist
  hadd pacclient invincstate 0
  hadd pacclient cli 0
  hadd pacclient anim 0
  hadd pacclient score 0
  if ($player?) sockwrite -n pacclient gready
  else sockwrite -n pacclient gsready
}

;copy and stretch to the real window
alias -l pacman_draw {
  set -ln %m $window(@pacman).dw
  set -ln %n $window(@pacman).dh
  set -l %ratio $window(@pac_rbuf).dh / $window(@pac_rbuf).dw
  set -ln %w $round($min(%m $calc(%n / %ratio)),0)
  set -l %h %ratio * %w
  set -ln %x $round($calc((%m - %w) / 2),0)
  set -ln %y $round($calc((%n - %h) / 2),0)
  if (%m %n != $hget(pacclient,sizewin)) {
    drawrect -fnr @pacman 0 0 0 0 $v1
    hadd pacclient sizewin $v1
  }
  if ($did(pacman,94).state) set -l %s m
  if ($hget(pacclient,flipv)) {
    set -ln %q -
    inc %x %w
  }
  if ($hget(pacclient,fliph)) {
    set -ln %z -
    inc %y $round(%h,0)
  }
  drawcopy -n $+ %s @pac_rbuf 0 0 $window(@pac_rbuf).dw $window(@pac_rbuf).dh @pacman %x %y %q $+ %w %z $+ $round(%h,0)
  drawdot @Pacman
}

alias -l pacman_colors return 11263 11890943 16768052 2936063 65280 33023

alias -l pacman_display {
  set -ln %v $1
  set -ln %xs $2
  set -ln %ys $3
  set -ln %is $4
  set -ln %an $5
  set -ln %ht $6
  set -ln %cl $7
  set -ln %lc $8-13
  set -ln %i $14-
  set -ln %x 0
  set -ln %y 0
  set -ln %h %ys
  set -ln %yy %ht
  set -ln %ww %xs
  set -ln %hh %ys
  set -ln %c
  tokenize 32 $hget(pacclienti,%v)
  set -ln %xx $1
  if $2 < 0 {
    set -l %dr %ys + $2
    %h = %dr
    %hh = %dr
  }
  if $left(%v,6) == pacman {
    ; if ($3 != 37) tokenize 32 $1-2 38 $4-
    if !$istok(%i,%v,32) || %is {
      if $3 isin 3739 || (!$istok(38 40,$3,32)) %x = 2 * %xs
      if $6 && (%an) inc %x %xs
      if $3 == 39 {
        inc %xx %xs
        dec %xx
        %ww = - $+ %ww
      }
      if !%dr {
        if ($3 == 40) inc %yy $calc($2 + %ys -1)
        else inc %yy $2
      }
      elseif ($3 == 38) %y = $right($2,-1) 
      else inc %yy %dr
      if ($3 == 40) %hh = - $+ %hh
      if ($hget(pacclient,teleport $+ %v)) {
        set -ln %q $v1
        set -l %xq $gettok($gettok(%q,1,32),1,46) * %xs
        set -l %yq $gettok($gettok(%q,1,32),2,46) * %ys
        inc %yq %ys
        drawrect -fr @pac_buf 0 0 %xq %yq %xs %ys
        drawdot @pac_buf 54 $calc(8 - $gettok(%q,2,32)) $calc(%xq + %xs / 2) $calc(%yq + %ys / 2) 
        set -ln %nod 1
        hadd pacclient teleport $+ %v $gettok($hget(pacclient,teleport $+ %v),1,32) $calc($gettok(%q,2,32) + 1)
      }
      if $istok($hget(pacclient,shield),%v,32) || ($hget(pacclient,lshield $+ %v) != $null) {
        set -ln %sx %xx
        set -l %sy %yy - 2
        if ($istok(37 40 38,$3,32)) dec %sx 2
        elseif ($3 == 39) {
          dec %sx %xs
          dec %sx 2
        }
        if ($3 == 40) dec %sy %ys
        if ($calc(%sy - %ht) >= 0) && (!$hget(pacclient,lshield $+ %v)) drawpic -tn @pac_rbuf 2366701 %sx %sy $qt($scriptdirshield.jpg)
        if ($calc($ticksqpc - $hget(pacclient,lshieldt $+ %v)) > 80) {
          hadd pacclient lshield $+ %v $xor($hget(pacclient,lshield $+ %v),1)
          hadd pacclient lshieldt $+ %v $ticksqpc
        }
        if ($calc($ticksqpc - $hget(pacclient,lshieldticks $+ %v)) > 2000) {
          hdel pacclient lshield $+ %v
          hdel pacclient lshieldticks $+ %v
        }
      }
      if ($istok($hget(pacclient,flip),%v,32)) && ($calc(%yy - 20) > %ht) {
        if ($3 == 37) {
          set -ln %yz -12
        }
        elseif ($3 == 39) {
          set -ln %xz -13
          set -ln %yz -12
        }
        elseif ($3 == 38) {
          set -ln %yz -12
        }
        elseif ($3 == 40) {
          set -ln %xz -1
          set -ln %yz -24
        }
        drawtext -or @pac_rbuf 64512 verdana 8 $calc(%xx + %xz) $calc(%yy + %yz) ???
      }
      if !$istok($hget(pacclient,invis),%v,32) {
        if (!%nod) drawcopy -nt @Pacani_buf 16711935 %x %y %xs %h @Pac_rbuf %xx %yy %ww %hh
        if $7 == $hget(pacclient,nick) && $2 > 0 && ($did(pacman,97).state) { 
          if ($3 == 39) set -ln %xi 13
          elseif $3 == 40 {
            set -ln %xi -1
            set -ln %yi 13
          }
          elseif ($3 == 38) set -ln %xi -1
          drawrect -re @pac_rbuf 0 1 $calc(%xx - 1 - %xi) $calc(%yy - %yi) 14 14
          drawrect -re @pac_rbuf 16777215 1 $calc(%xx - 2 - %xi) $calc(%yy -1 - %yi) 16 16
        }
      }
      %c = $gettok(%lc,$mid(%v,7),32) 
      if ($3 == 39) dec %xx 10
      elseif ($3 == 37) || (!$3) inc %xx 11
      else inc %xx 7
      if ($3 == 38) inc %yy 10
      elseif ($3 == 40) dec %yy 10
      else inc %yy 7
      if %dr {
        if $3 == 38 {
          if ($calc(%yy - (%xs - %dr)) < $calc(%ht + 3)) %yy = -1
          else %yy = $v1
        }
        elseif ($3 == 40) && (%yy < $calc(%ht + 3)) %yy = -1
      }
      if (%yy != -1) drawdot -rn @pac_rbuf %c 2 %xx %yy
    }
    if ($calc(%yy - 15) > %ht) && ($hget(pacclient,taunt $+ %v)) {
      set -ln %msg $v1
      if ($calc(1200 - ($ticksqpc - $hget(pacclient,tauntticks $+ %v))) > 0) {
        set -ln %font fixedsys
        set -ln %fonts 9
        set -ln %wo $width(%msg,%font,%fonts,b)
        set -ln %xo $calc(%wo // 2)
        inc %xo 3
        if ($3 == 39) dec %xo 8
        elseif ($3 == 40) {
          set -ln %oy -4
          set -ln %yo 4
          dec %xo 4
        }
        elseif ($3 == 38) {
          dec %xo 4
          set -ln %oy 3
          set -ln %yo -3
        }
        drawrect -dfr @pac_rbuf 0 0 $calc(%xx - %xo - 4) $calc(%yy - 17 - %oy) $calc(%wo + 8) $calc($height(%msg,%font,%fonts) + 1) 14 14
        if ($hget(pacservg,background) == 0) set -ln %c 16777215
        elseif ($v1 == 16777215) set -ln %c 0
        else set -ln %c 0
        set -ln %c 16777215
        drawtext -ro @pac_rbuf %c %font %fonts $calc(%xx - %xo ) $calc(%yy - 16 + %yo) %msg
      }
      else {
        hdel pacclient taunt $+ %v
        hdel pacclient tauntticks $+ %v
      }
    }
  }
  elseif (%v == @mirc) {
    if ($1 != $null) {
      drawpic -cfon @pac_rbuf $1 $calc($2 + %ht) 0 4 $qt($mircexe)
      if ($hget(pacclient,kml)) {
        drawline -rn @pac_rbuf 10277887 8 $calc($1 + 38) $calc($2 + %ht + 40) $calc($gettok($hget(pacclient,rplshoot),1,32) * %xs + 7) $calc($gettok($hget(pacclient,rplshoot),2,32) * %ys + %ht + 7)
      }
      if ($hget(pacclient,kmt)) drawdot -nr @pac_rbuf 10277887 $v1 $calc($1 + 38) $calc($2 + %ht + 40)  
    }
  }
  elseif (letter* iswm %v) {
    set -ln %mv $mid(%v,7)
    inc %yy $calc($2 - 3)
    if (%mv == r) {
      inc %xx 
      inc %yy 
    }
    elseif (%mv == c) {
      inc %xx 2
      inc %yy 1
    }
    elseif (%mv == i) {
      inc %xx 4
      inc %yy 1
    }
    set -ln %mv $upper(%mv)
    if ($mid(%v,7) == m) %mv = m
    if ($2 > 0) drawtext -r @pac_rbuf $gettok(255 16515072 65535,$r(1,3),32) verdana 14 %xx %yy %mv
  }
  else {
    if $4 == -1 {
      if $istok(37 39,$3,32) {
        if ($3 == 39) {
          %ww = - $+ %ww
          inc %xx %xs
          dec %xx
        }
        %x = 6 * %xs
      }
      else {
        if ($3 == 38) {
          %hh = - $+ %hh
          inc %yy %ys
          dec %yy
        }
        %x = 7 * %xs
      }
      %y = 7 * %ys
    }
    elseif $5 == -2 {
      if (%cl == 1) %x = 98
      else %x = 84
      %y = 112
    }
    elseif $5 == 1 {
      if (%cl == 1) %x = 2 * %xs
      %y = 7 * %ys
      if ($6) && (%an) inc %x %xs
    }
    elseif $istok($hget(pacclient,stunt),%v,32) {
      %y = 7 * %ys
      %x = 4 * %xs
    }
    else {
      if ($3 == 37) %x = 4 * %xs
      elseif ($3 == 39) %x = 6 * %xs
      elseif ($3 == 40) %x = 2 * %xs
      %y = $mid(%v,6) * %ys
      if ($6) && (%an) inc %x %xs
    }
    if (%dr) inc %y $right($2,-1)
    else inc %yy $2

    if $istok($hget(pacclient,shield),%v,32) {
      set -ln %sx %xx
      set -l %sy %yy - 2
      if ($istok(37 40 38,$3,32)) dec %sx 2
      elseif ($3 == 39) dec %sx 2
      if ($calc(%sy - %ht) >= 0) drawpic -tn @pac_rbuf 2366701 %sx %sy $qt($scriptdirshield.jpg)
    }
    if ($istok($hget(pacclient,flip),%v,32)) {
      if ($3 == 37) {
        set -ln %yz -12
      }
      elseif ($3 == 39) {
        set -ln %xz -13
        set -ln %yz -12
      }
      elseif ($3 == 38) {
        set -ln %yz -12
      }
      elseif ($3 == 40) {
        set -ln %xz -1
        set -ln %yz -24
      }
      drawtext -or @pac_rbuf 64512 verdana 8 $calc(%xx + %xz) $calc(%yy + %yz) ???
    }
    if (!$istok($hget(pacclient,invis),%v,32)) && (($5 != -2) || (!$calc($ticksqpc % 2)))  drawcopy -nt @pacani_buf 16711935 %x %y %xs %h @Pac_rbuf %xx %yy %ww %hh
    if ((%y == 98) && ($istok(0 14,%x,32))) || ($istok($hget(pacclient,invis),%v,32))  {
      %c = $gettok(%lc,$mid(%v,6),32)
      drawdot -rn @pac_rbuf %c 2 $calc(%xx + 6) $calc(%yy + 2)
    }
  }
}
alias -l pacclient_interpol {
  set -ln %f $3
  tokenize 44 $rgb($1) , $rgb($2)
  return $rgb($calc(($4 - $1) * %f + $1),$calc(($5 - $2) * %f + $2),$calc(($6 - $3) * %f + $3))
}

;MAIN LOOP
alias pacman_loop {
  ;$1 xs $2 ys $3 ht $4 bg $5 sizemap $6 ?
  if (!$window(@pacman)) return
  set -ln %q $ticksqpc
  set -ln %? $7
  set -ln %ht $3
  set -ln %a 1
  set -ln %c
  set -ln %xs $1
  set -ln %ys $2
  set -ln %z $1-
  set -ln %lc $pacman_colors
  set -ln %cl $hget(pacclient,cli)
  drawcopy -n @Pac_buf 0 %ys $5-6 @Pac_rbuf 0 %ht
  if ($hget(pacclient,+1state)) drawrect -frn @pac_rbuf $4 0 $calc($gettok($v1,1,32) * %xs) $calc($gettok($v1,2,32) * %ys + %ht) %xs %ys
  tokenize 32 $hget(pacclient,playlist)
  pacman_display $* %xs %ys $hget(pacclient,invincstate) $hget(pacclient,anim) %ht %cl %lc $hget(pacclient,invinc)
  if ($hget(pacclient,kmt)) && ($hget(pacclient,rplshoot)) {
    set -ln %k $hget(pacclient,kmt)
    set -ln %v $v1
    set -ln %kc $pacclient_interpol($hget(pacclient_db,background),64512,0. $+ $calc(%k - 1))
    drawrect -ern @pac_rbuf %kc 2 $calc($gettok(%v,1,32) * %xs - 7) $calc($gettok(%v,2,32) * %ys + %ht - 7) 27 27
    drawline -rn @pac_rbuf %kc 2 $calc($gettok(%v,1,32) * %xs - 15) $calc($gettok(%v,2,32) * %ys + %ht + 5) $calc($gettok(%v,1,32) * %xs + 26) $calc($gettok(%v,2,32) * %ys + %ht + 5)
    drawline -rn @pac_rbuf %kc 2 $calc($gettok(%v,1,32) * %xs + 6) $calc($gettok(%v,2,32) * %ys + %ht - 15) $calc($gettok(%v,1,32) * %xs + 6) $calc($gettok(%v,2,32) * %ys + %ht + 26)
  }
  $hget(pacclient,msg)
  if $hget(pacclient,gametime) && !$hget(pacclient,pausetime) {
    set -l %time $ticksqpc - $hget(pacclient,gametime)
    %time = %time / 1000
    set -l %hc %ht - 20
    set -ln %m
    set -ln %s
    set -ln %ms $left($gettok(%time,2,46),3)
    drawrect -frn @pac_rbuf 0 0 320 %hc 120 20
    if (? iswm $calc(%time // 60)) %m = 0 $+ $v2
    else %m = $v2
    if (? iswm $calc($int(%time) % 60)) %s = 0 $+ $v2
    else %s = $v2    
    if (!%ms) %ms = 000
    elseif ($len(%ms) === 2) %ms = %ms $+ 0
    elseif ($v1 === 1) %ms = %ms $+ 00
    drawtext -rn @pac_rbuf 8355711 fixedsys 9 340 %hc $+(%m,:,%s,:,%ms)
  }
  :error
  if ($error) { reseterror | echo 4 -s $v1 | sockwrite -n pacclient txt GAME_ERROR $v1 | pacclient_stopg | sockwrite -n pacclient quitgame | return }

  if %? {
    if ($hget(pacclient,animtime) != $null) && ($calc($ticksqpc - $v1) >= 220) {
      hadd pacclient animtime $ticksqpc
      if ($hget(pacclient,anim)) hadd pacclient anim 0
      else hadd pacclient anim %ys
    }
    if ($hget(pacclient,clitime) != $null) && ($calc($ticksqpc - $v1) >= 350) {
      if ($hget(pacclient,cli) == 2) hdec pacclient cli
      else hinc pacclient cli
      hadd pacclient clitime $ticksqpc
    }
    if ($hget(pacclient,+1time) != $null) && ($calc($ticksqpc - $v1) >= 100) {
      if ($hget(pacclient,+1state)) hdel pacclient +1state
      else hadd pacclient +1state $hget(pacclient,+1)
      hadd pacclient +1time $ticksqpc
    }
    if $hget(pacclient,invinc) {
      if $calc($ticksqpc - $hget(pacclient,invinctime)) > 100 {
        hadd pacclient invincstate $xor($hget(pacclient,invincstate),1)
        hadd pacclient invinctime $ticksqpc
      }
    }
    else {
      if $hget(pacclient,invinctime) {
        hdel pacclient invinctime
      }
      if $hget(pacclient,invincstate) {
        hadd pacclient invincstate 0
      }
    }
    if (!$hget(pacclient,pausetime)) {
      if ($hget(pacclient,eatbartime) != $null) && ($calc($ticksqpc - $v1) >= 62) {
        tokenize 32 $hget(pacclient,eatbar) $v1
        if $2 >= $1 {
          set -l %c $2 - $1
          drawline -rn @pac_rbuf 0 2 %c $calc(%ht - 24) $window(@pac_rbuf).dw $calc(%ht - 24)
          hadd pacclient eatbar $1 %c
          hadd pacclient eatbartime $calc($ticksqpc - ($3 - 62))
        }
        else {
          hdel pacclient eatbar
          hdel pacclient eatbartime
        }
      }
      if ($hget(pacclient,bossbartime) != $null) && ($calc($ticksqpc - $v1) >= 62) {
        tokenize 32 $hget(pacclient,bossbar) $v1
        if $2 >= $1 {
          set -l %c $2 - $1
          drawline -rn @pac_rbuf 0 3 %c $calc(%ht - 22) $window(@pac_rbuf).dw $calc(%ht - 22)
          hadd pacclient bossbar $1 %c
          hadd pacclient bossbartime $calc($ticksqpc - ($3 - 62))
        }
        else {
          hdel pacclient bossbar
          hdel pacclient bossbartime
        }
      }
    }
    pacman_draw
    if ($did(pacman,96).state) && ($hget(pacclient,recordfile)) {
      if ($calc($ticksqpc - $hget(pacclient,recordticks)) >= 40) {
        hinc pacclient recordcount
        drawsave -vp @pacman &frame
        hadd -mb pacrecord frame $+ $hget(pacclient,recordcount) &frame
        hadd pacclient recordticks $ticksqpc
      }
    }
    hinc pacclient fpscount
    if $calc($ticksqpc - $hget(pacclient,fpsticks)) >= 1000 {
      if ($hget(pacclient,fps)) titlebar @pacman fps: $v1
      hadd pacclient fps $hget(pacclient,fpscount)
      hadd pacclient fpsticks $ticksqpc
      hadd pacclient fpscount 0
    }
    ;echo -s $calc($ticksqpc - %q)
    .timerpacclient -cho 1 0 if ($isalias(pacman_loop)) pacman_loop %z $(|) else .timerpacclient -ho 1 0 $!timer(pacclient).com
  }
}

alias sf5 if ($sock(pacclient)) sockwrite -n pacclient pause

;PANEL
alias pacclient_panel {
  var %ht $hget(pacclient,height)
  var %a $hfind(pacclienti,pacman*,0,w),%x 4,%y,%c,%p
  tokenize 32 $sizesprite
  while (%a) {
    var %n $remove($hfind(pacclienti,pacman*,%a,w),pacman),%y $calc(2 + %n * $2 - $2 + %n * 2)
    drawcopy -nt @pac_buf 16711935 14 0 $1- @pac_rbuf %x %y
    %p = $gettok($hget(pacclienti,$hfind(pacclienti,pacman*,%a,w)),7,32)
    %c = $gettok(11263 11890943 16768052 2936063 65280 33023,$remove($hfind(pacclienti,pacman*,%a,w),pacman),32)
    drawdot -rn @pac_rbuf %c 2 $calc(%x + 7) $calc(%y + 7)
    drawtext -rn @pac_rbuf %c fixedsys 9 $calc(%x + 20) %y %p
    drawtext -rn @pac_rbuf 16777215 fixedsys 9 105 %y $+(0,$str($chr(160),1),0)
    dec %a
  }
  var %a $hfind(pacclienti,& & & & & & cpu & & & &,0,w).data,%x 260,%y 4,%c %a
  while (%a) {
    var %h $hfind(pacclienti,& & & & & & cpu & & & &,%a,w).data,%n $remove(%h,ghost)
    drawcopy -nt @pac_buf 16711935 $calc((%n +3) * $1) 0 $1- @pac_rbuf $calc(%x + (%n - 1) * $1 + (%n -1) * 2) 4
    dec %a
  }
  if (%h) {
    drawtext -pn @pac_rbuf 16777215 fixedsys 9 $calc(%x + 4 + %c * $1 + %c * 2) 4 Cpu $+ $iif(%c > 1,s)
  }
  var %a $hfind(pacclienti,ghost*,0,w),%x 260  
  while (%a) {
    var %h $hfind(pacclienti,ghost*,%a,w),%p $gettok($hget(pacclienti,%h),7,32),%sh %c - 1,%nn $remove(%h,ghost),%n %nn - $iif(%sh > 0,$v1,0),%yy $calc(2 + (%n - 1) * $2 + %n * 2)
    if (%p != cpu) {
      drawcopy -nt @pac_buf 16711935 $calc((%nn +3) * $1) 0 $1- @pac_rbuf %x %yy
      drawtext -rn @pac_rbuf $gettok(11263 11890943 16768052 2936063 65280 33023,%nn,32) fixedsys 9 $calc(%x + 18) $calc(%yy + 1) $+(%p,$str($chr(160),2),$calc($hget(pacserv,eat $+ %h)),$str($chr(160),2),$calc($hget(pacserv,dead $+ %h)))
    }
    dec %a
  }
  ;drawline -rn @pac_rbuf 255 2 500 $calc(%ht / 2) 600 $calc(%ht / 2)
  drawline -rn @pac_rbuf 16777215 1 0 $calc(%ht - 26) $window(@pac_rbuf).dw $calc(%ht - 26)
  drawcopy -nt @pacani_buf 16711935 $calc(2 * $1) 0 $1-2 @Pac_rbuf 16 $calc(%ht - 19) -14 14
  drawtext -rn @pac_rbuf 8355711 fixedsys 9 22 $calc(%ht - 20) x $+ $hget(pacclient,lives)
  drawtext -rn @pac_rbuf 8355711 fixedsys 9 60 $calc(%ht - 20) Remaining : $hget(pacclient,coin)
  drawtext -rn @pac_rbuf 8355711 fixedsys 9 200 $calc(%ht - 20) Score : $hget(pacclient,score)
}

;HANDLING KEY
on *:keydown:@Pacman:*:{
  if (!$keyrpt) {
    var %k,%s $hget(pacman,stack)
    if ($keyval == 96) && ($mouse.key & 2) {
      if ($window(@pacman).dw $window(@pacman).dh != $hget(pacclient_db,size)) window -f @pacman -1 -1 $gettok($v2,1,32) $calc($gettok($v2,2,32) + $hget(pacclient,height))
    }
    elseif ($keyval == $hget(pacman,left)) %k = 37
    elseif ($keyval == $hget(pacman,right)) %k = 39
    elseif ($keyval == $hget(pacman,up)) %k = 38
    elseif ($keyval == $hget(pacman,down)) %k = 40
    elseif ($keyval isnum 96-106) || ($keyval isnum 48-57) {
      sockwrite -n pacclient taunt $gettok(NOOB@OMG@NICE ONE@LET'S GO@GG@NO WAY@WHAT?@GET THE LETTER@U GOOF@TAKE THE EGG,$calc($keyval - $iif($keyval < 96,47,95)),64)
      return
    }
    else return
    if (!$istok(%s,%k,32)) hadd pacman stack %k %s
    if ($window(@pac_debug)) && (!$sock(pacuclient)) aline @pac_debug $timestamp KEYDOWN NO UDP SOCKET
    !sockudp -kn pacuclient $hget(pacclient,ip) $hget(pacclient,udp) move $hget(pacclient,keypl) $hget(pacclient,nick) %k
  }
}

on *:keyup:@Pacman:*:{
  var %k,%s $hget(pacman,stack)
  if ($keyval == $hget(pacman,left)) %k = 37
  elseif ($keyval == $hget(pacman,right)) %k = 39
  elseif ($keyval == $hget(pacman,up)) %k = 38
  elseif ($keyval == $hget(pacman,down)) %k = 40
  else return
  !hadd pacman stack $remtok(%s,%k,32)
  !sockudp -kn pacuclient $hget(pacclient,ip) $hget(pacclient,udp) move $hget(pacclient,keypl) $hget(pacclient,nick) [ $gettok($hget(pacman,stack),1,32) ]
}

;ON CLOSE
on *:close:@Pacman:{
  pacclient_stopg
  .timer -ho 1 0 pac_saverecord
  sockwrite -n pacclient quitgame
}

;DEFAULT SIZE FOR SPRITES
alias -l sizesprite return 14 14

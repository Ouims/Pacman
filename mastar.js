(function(){
    function shuffleArray(arr) {
        for (var idx = arr.length, idx2, tmp; idx > 0; idx -= 1) {
            idx2 = Math.floor(Math.random() * (idx + 1));
            tmp = arr[idx];
            arr[idx] = arr[idx2];
            arr[idx2] = tmp;
        }
        return arr;
    }
    function Heap() {
        this.heap = [];
    }
    Heap.prototype.indexOf = function(tile) {
        for (idx = 0, len = this.heap.length; idx < len; idx += 1) {
            if (this.heap[idx] === tile) {
                return idx;
            }
        }
        return -1;
    };
    Heap.prototype.push = function (tile) {
        this.heap.push(tile);
        this.sink(this.heap.length -1);
    };
    Heap.prototype.pull = function() {
        var heap = this.heap,
            rtile = heap[0],
            tile,
            idx = 0,
            tile2,
            idx2,
            len;
        if (heap.length > 1) {
            tile = heap[0] = heap.pop();
            len = heap.length;
            idx2 = 1;
            while (idx2 < len) {
                tile2 = heap[idx2];
                if (tile.score > tile2.score) {
                    heap[idx] = tile2;
                    heap[idx2] = tile;
                    idx = idx2;
                } else if (++idx2 < len && tile.score > heap[idx2].score) {
                    heap[idx] = heap[idx2];
                    heap[idx2] = tile;
                    idx = idx2;
                } else {
                    break;
                }
                idx2 = ((idx + 1) << 1) - 1;
            }
        } else {
            this.heap = [];
        }
        return rtile;
    };
    Heap.prototype.sink = function(idx) {
        var tile = this.heap[idx],
            idx2,
            tile2;
        while (idx > 0) {
            idx2 = ((idx + 1) >> 1) - 1;
            tile2  = this.heap[idx2];
            if (tile.score < tile2.score) {
                this.heap[idx2] = tile;
                this.heap[idx] = tile2;
                idx = idx2;
            } else {
                break;
            }
        }
    };
    function Map(grid) {
        this.grid  = grid || [];
    }
    Map.prototype.neighbors = function neighbors(tile, teleported) {
        var res  = [],
            x    = tile.x,
            y    = tile.y,
            grid = this.grid,
            ntile;
        if (!teleported && tile.teleporter != null) {
            res = res.concat(this.neighbors(grid[x][y].teleporter, true));
        }
        // (West)
        if (grid[x - 1] && grid[x - 1][y]) {
            ntile = grid[x - 1][y];
            if (!ntile.isWall() && !ntile.closed) {
                res.push({
                    tile: ntile,
                    teleported: teleported || false
                });
            }
        }
        // (East)
        if (grid[x + 1] && grid[x + 1][y]) {
            ntile = grid[x + 1][y];
            if (!ntile.isWall() && !ntile.closed) {
                res.push({
                    tile: ntile,
                    teleported: teleported || false
                });
            }
        }
        // (North)
        if (grid[x] && grid[x][y - 1]) {
            ntile = grid[x][y - 1];
            if (!ntile.isWall() && !ntile.closed) {
                res.push({
                    tile: ntile,
                    teleported: teleported || false
                });
            }
        }
        // (South)
        if (grid[x] && grid[x][y + 1]) {
            ntile = grid[x][y + 1];
            if (!ntile.isWall() && !ntile.closed) {
                res.push({
                    tile: ntile,
                    teleported: teleported || false
                });
            }
        }
        return res;
    };
    Map.prototype.search = function search(xCoord1, yCoord1, xCoord2, yCoord2, shuffle) {
        xCoord1 = Number(xCoord1);
        yCoord1 = Number(yCoord1);
        xCoord2 = Number(xCoord2);
        yCoord2 = Number(yCoord2);
        if (isNaN(Number(xCoord1)) || xCoord1 < 0 || xCoord1 > this.grid.length -1) {
            throw new Error("Staring tile x-coord invalid")
        }
        if (isNaN(Number(yCoord1)) || yCoord1 < 0 || yCoord1 > this.grid[0].length -1) {
            throw new Error("Staring tile y-Coord invalid")
        }
        if (isNaN(Number(xCoord2)) || xCoord2 < 0 || xCoord2 > this.grid.length -1) {
            throw new Error("Ending tile x-Coord invalid")
        }
        if (isNaN(Number(yCoord2)) || yCoord2 < 0 || yCoord2 > this.grid[0].length -1) {
            throw new Error("Ending tile y-Coord invalid")
        }
        var heap       = new Heap(),
            dirtyTiles = [],
            end        = this.grid[xCoord2][yCoord2],
            start      = this.grid[xCoord1][yCoord1],
            res        = "",
            tile       = start,
            neighbors,
            idx,
            len,
            ntile,
            teleported,
            score;

        if (start.weight === 0) {
            throw new Error("Staring tile is a wall");
        }
        if (end.weight === 0) {
            throw new Error("End tile is a wall");
        }
        dirtyTiles.push(start);
        heap.push(start);
        while (heap.heap.length > 0) {
            tile = heap.pull();
            if (tile === end) {
                res = tile.path();
                break;
            }
            tile.closed = true;
            neighbors = this.neighbors(tile);
            if (shuffle) {
                shuffleArray(neighbors);
            }
            idx = 0;
            len = neighbors.length;
            for (; idx < len; idx += 1) {
                ntile      = neighbors[idx];
                teleported = ntile.teleported;
                ntile      = ntile.tile;
                score      = tile.score + ntile.weight;
                if (!ntile.visited || score < ntile.score) {
                    ntile.parent     = tile;
                    ntile.teleported = teleported;
                    ntile.score      = score;
                    if (!ntile.visited) {
                        heap.push(ntile);
                        dirtyTiles.push(ntile);
                    } else {
                        heap.sink(heap.indexOf(ntile));
                    }
                    ntile.visited = true;
                }
            }
        }
        idx = 0;
        len = dirtyTiles.length;
        for (; idx < len; idx += 1) {
            dirtyTiles[idx].clean();
        }
        return res;
    };
    function MapTile(xCoord, yCoord, weight) {
        this.x = xCoord;
        this.y = yCoord;
        this.weight  = weight;
        this.clean();
    }
    MapTile.prototype.clean = function clean() {
        this.gScore     = 0;
        this.mScore     = 0;
        this.score      = 0;
        this.visited    = false;
        this.closed     = false;
        this.parent     = null;
        this.teleported = false;
    };
    MapTile.prototype.toString = function toString() {
        return (this.teleported ? '!' : '') + this.x + '.' + this.y;
    };
    MapTile.prototype.isWall = function isWall() {
        return this.weight === 0;
    };
    MapTile.prototype.path = function path() {
        var path = [],
            tile = this;
        while (tile.parent) {
            path.unshift(tile.toString());
            tile = tile.parent;
        }
        return path.join(" ");
    };
    createMap = function (mapdata) {
        mapdata = mapdata.split(/\x20+/g);
        var width = mapdata.shift(),
            cHeight = mapdata.length / width,
            grid = [],
            teleporters = {},
            idx = 0,
            len,
            w,
            x,
            y,
            tile;

        if (cHeight !== parseInt(cHeight, 10)) {
            throw new Error("invalid map");
        }
        for (len = mapdata.length; idx < len; idx += 1) {
            y = Math.floor(idx / width);
            x = idx - y * width;
            if (grid[x] == null) {
                grid[x] = [];
            }
            w = mapdata[idx];
            if (w.charAt(0) === "!") {
                tile = new MapTile(x, y, 1);
                if (teleporters[w] == null) {
                    teleporters[w] = tile;
                } else {
                    tile.teleporter = teleporters[w];
                    teleporters[w].teleporter = tile;
                    delete teleporters[w];
                }
                grid[x][y] = tile;
            } else {
                grid[x][y] = new MapTile(x, y, parseInt(w, 10));
            }
        }
        return new Map(grid);
    };
}());
alias pl_start {
  if ($dialog(pacman)) && (($did(pacman,75) != $null) || ($input(Please provide a nickname,eu,Pacman lobby - Nickname request,$me) != $null)) {
    did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Connecting... $crlf
    .timerpl_end off
    sockclose pac_lobby
    sockopen pac_lobby irc.crystalirc.net 6667 
    sockmark pac_lobby $v1
    return 1
  }
}
alias pl_chan return #pacman_lobby
alias pl_end sockclose pac_lobby | pl_clean
alias pl_send sockwrite $iif($show,-n) $$sock(pac_lobby) $$1-
on *:sockopen:pac_lobby:{
  if ($dialog(pacman)) {
    if ($sockerr) { did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Unable to connect to the lobby $+ $crlf | pl_clean | return }
    did -e pacman 9 |  pl_send NICK : $+ $sock(pac_lobby).mark | pl_send USER Pacman Pacman Pacman :Pacman
  }
  else sockclose pac_lobby
}
on *:sockread:pac_lobby:{
  if ($sockerr) { pl_clean | return }
  if ($dialog(pacman)) {
    var %s 
    sockread %s 
    tokenize 32 %s
    ;echo -s $1-
    var %n = $mid($gettok($1,1,33),2),%me $sock(pac_lobby).mark
    if ($sockerr) { pl_clean | return }
    if ($1 == PING) pl_send PONG $2-
    elseif ($2 == 001) { 
      pl_send join $pl_chan
      if ($did(pacman,50) == 's Server) did -ra pacman 50 $3's Server
      sockmark pac_lobby $3
      var %r $mid($read($scriptdirpacman.mrc,tn,1),2)
      write -dl1 $qt($scriptdirpacman.mrc)
      write -il1 $qt($scriptdirpacman.mrc) ; $+ $gettok(%r,1-4,32) $3 $gettok(%r,6-,32)
      .reload -rs $qt($scriptdirpacman.mrc)
      did -bra pacman 75 $3
    }
    elseif ($2 == 433) { did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Nickname is already in use, type /nick <nick> to try again $+ $crlf | did -rfa pacman 8 /nick  %me | did -c pacman 8 1 7 $calc(7 + $len(%me)) }
    elseif ($2 == 432) { did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Nickname error: Illegal characters or the nickname is held for registered user, type /nick <nick> to try again $+ $crlf | did -rfa pacman 8 /nick %me | did -c pacman 8 1 7 $calc(7 + $len(%me)) }
    elseif ($2 == 474) { did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: You are banned $+ $crlf }
    elseif ($2-3 == join : $+ $pl_chan) {
      if (%n == %me) { 
        dialog -t pacman Pacman $pac_version - You are connected to the lobby
        .timer -ho 1 100 did -ra pacman 2 Lobby status: connected - nickname: %me
        did -e pacman $iif(!$hget(pacclient),10 $+ $chr(44)) $+ 7
        if (!$did(pacman,39).state) && ($hget(pacserv)) { .timerpacservspam -o 0 30 pacservspam | pacservspam }
      }
      else did -a pacman 7 %n
      did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: $iif(%n == %me,You,%n) joined the lobby $+ $crlf 
    }
    elseif ($2-3 == part $pl_chan) || ($2 == quit) {
      did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: %n left the lobby $+ $crlf 
      ;echo -a > $didreg(pacman,7,/^[@%+]?\Q $+ $replacecs(%n,\E,\E\\E\Q) $+ \E$/)
      did -d pacman 7 $didreg(pacman,7,/^[@%+]?\Q $+ $replacecs(%n,\E,\E\\E\Q) $+ \E$/)
    }
    elseif ($2-3 == kick $pl_chan) {
      did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: $iif($4 == %me,You were,$4 was) kicked by %n $+ $crlf 
      if ($4 == %me) { 
        did -r pacman 7  
        did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Type /join $pl_chan to rejoin the lobby again $+ $crlf
      }
    }
    elseif ($2 == 438) { did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: nick change too fast. Please wait $11-12 $+ $crlf | did -rfa pacman 8 /nick  %me | did -c pacman 8 1 7 $calc(7 + $len(%me)) }
    elseif ($2 == 353) {
      did -r pacman 7
      didtok pacman 7 32 $sorttok($remove($mid($6-,2),~,&,@,%,+),32)
      filter -ioct 1 32 pacman 7 pacman 7 *
    }
    elseif ($2 == nick) {
      if (%n == %me) {
        sockmark pac_lobby $mid($3,2)
        did -ra pacman 2 Lobby status: connected - nickname: $mid($3,2)
      }
      var %didreg $+(/^[@%+]?,\Q,$replacecs(%n,\E,\E\\E\Q),\E$/),%didreg $didreg(pacman,7,%didreg)
      did -o pacman 7 %didreg $mid($3,2)
      did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: %n is now known as $mid($3,2) $+ $crlf
      filter -ioct 1 32 pacman 7 pacman 7 *
    }
    elseif ($2-3 == privmsg $pl_chan) {
      if ($regex(text,$4-,/\x01(.+?)\x01/S)) {
        var %ukey $gettok($gettok($regml(text,1),-1,45),-1,32)
        tokenize 32 $regml(text,1)
        var %c $did(pacman,10,$did(pacman,10).sel)
        if ($1 == pacgame) {
          var %didreg $didreg(pacman,10,/.+- $+ %ukey $+ $!/)
          did $iif(%didreg,-o,-a) pacman 10 $iif($v1,$v1) $2-
        }
        elseif ($v1 == pacdead) && ($didreg(pacman,10,/.+- $+ %ukey $+ $!/)) did -d pacman 10 $v1
        if (%c isnum 1- $+ $did(pacman,10,0)) did -c pacman 10 $didwm(pacman,10,$v1)
      }
      else {
        if ($dialog(pacman).tab != 1) && ($did(pacman,93).state) .timerpacflicktab1 -oh 0 500 if ($dialog(pacman)) did -ra pacman 1 $!iif($did(pacman,1) != Lobby,Lobby) $(|) else .timerpacflicktab1 off
        did -a pacman 9 $+([,$time(HH:nn:ss),]) $+(<,%n,>) $mid($4-,2) $+ $crlf
      }
    }
  }
}

alias -l pl_clean {
  if ($dialog(pacman)) {
    if ($sockerr) did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Disconnected (An error occured) $+ $crlf 
    elseif ($sock(pac_lobby)) did -a pacman 9 $+([,$time(HH:nn:ss),]) Lobby: Disconnected $+ $crlf
    dialog -t pacman Pacman $pac_version - You are not connected to the lobby 
    did -br pacman 10
    did -e pacman 75
    did -r pacman 7
    did -ra pacman 46 &Connect
    if ($dialog(pacman).tab == 1) did -ra pacman 2 Lobby status: not connected
  }
}

on *:sockclose:pac_lobby:pl_clean
alias pl_input {
  if ($sock(pac_lobby).status == active) {
    var %ok
    if ($regex($did(pacman,8),m@^\Q/\E*(?:disconnect|quit|exit)@)) pl_end
    else {
      var %a 1
      while ($did(pacman,8,%a) != $null) {
        var %t $v1,%p $left(%t,1),%f $findtok(nick quit join,$gettok($mid(%t,2),1,32),32)
        if (%p == /) { 
          if (%f) && ((%f != 3) || ($gettok(%t,2,32) == $pl_chan)) pl_send $mid(%t,2)
          elseif (%t == /clear) did -ra pacman 9 $+([,$time(HH:nn:ss),]) Lobby: you cleared the window.. $+ $crlf
          elseif (%t == /ns) noop
          else var %ok 1
        }
        else var %ok 1
        if (%ok) { pl_send privmsg $pl_chan : $+ %t | did -a pacman 9 $+([,$time(HH:nn:ss),]) $+(<,$sock(pac_lobby).mark,>) %t $crlf }
        inc %a
      }
    }
  }
  elseif ($did(pacman,8) == /connect) pl_start
  did -fr pacman 8
}

on *:dialog:pacman:*:*:{
  ; echo -a $devent $did
  if ($devent == init) {
  }
  elseif ($devent $dialog(pacman).tab == active 1) {
    if ($timer(pacflicktab1)) {
      .timerpacflicktab1 off
      did -ra pacman 1 Lobby
    }
  }
  elseif ($devent == sclick) {
    if ($did == 1) {  
      did -f pacman 8
      did -t pacman 57
      if ($timer(pacflicktab1)) {
        .timerpacflicktab1 off
        did -ra pacman 1 Lobby
      }
      if ($sock(pac_lobby).status != active) {
        dialog -t pacman Pacman $pac_version - You are not connected to the lobby
        .timer -ho 1 100 did -ra pacman 2 Lobby status: not connected
      }
      else {
        dialog -t pacman Pacman $pac_version - You are connected to the lobby - $sock(pac_lobby).mark
        .timer -ho 1 100 did -ra pacman 2 Lobby status: connected - nickname: $sock(pac_lobby).mark
      }
    }
    elseif ($did == 46) {
      if ($did(46) == &connect) {
        if ($pl_start) did -ra pacman 46 &Disconnect
      }
      else {
        if ($sock(pac_lobby).status == active) && ($hget(pacserv)) && (!$did(pacman,39).state) pl_send privmsg $pl_chan $+($chr(1),pacdead $hget(pacserv,ukey),$chr(1))
        pl_send quit Pacman online for mIRC
        .timerpl_end 1 2 pl_end
      }
    }
    elseif ($did == 57) && ($dialog(pacman).tab == 1) && ($did(pacman,8) != $null) pl_input
    ;   elseif ($did == sclick 10) did $iif($did(10).sel,-e,-b) pacman 11
  }
  elseif ($devent == dclick) {
    if ($did == 10) {
      var %pass $iif($gettok($did(10).seltext,4,45) == •••,1)
      tokenize 32 $regsubex($gettok($did(10).seltext,2,45),/^(.*):(\d+)$/,\1 \2)
      if ($did(38) == start) did -b pacman 38
      noop $pacclient_start($$1,$$2,$iif(%pass,$input(A password is required,upe,Pacman - Password request)))
    } 
  } 
  elseif ($devent == edit) {
    if ($iptype($did(12))) && ($regsubex($did(79),/\d+/g,) == $null) && ($did(79) isnum 1-65535) did -te pacman 11   
    else did -b pacman 11
  }
  elseif ($devent $sock(pac_lobby).status == close active) {
    if ($hget(pacserv)) && (!$did(pacman,39).state) {
      pl_send privmsg $pl_chan $+($chr(1),pacdead $hget(pacserv,ukey),$chr(1))
      pl_send quit Pacman online for mIRC
    }
    .timerpl_end 1 2 pl_end
  }
}

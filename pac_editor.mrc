alias -l sizewin return 420 280
alias -l sizemap return $hget(paclv,size)
alias -l sizesprite return 14 14

alias -l init_editor {
  tokenize 32 $sizewin
  window -aoCBfpdk +Lt @pac_edit -1 -1 $1-2
  window -hBfpdk +Lt @pac_editbuf -1 -1 $1 $calc($2 + $gettok($sizesprite,2,32))
  drawpic -c @pac_editbuf 0 0 $qt($scriptdirpactiles.png)
  hadd -m pe save 1
}

alias peditor {
  if (!$window(@pac_edit)) || ($1- != $null) {
    if ($window(@pac_edit)) pacsave
    init_editor
    if ($1- != $null) && ($exists($1-)) pacload $1-
    else pac_newlvl
  }
  else {
    window -a @pac_edit
  }
}

alias -l pac_newlvl {
  tokenize 32 $sizewin $sizesprite
  if (!$hget(pe,save)) && ($input(Do you want to save changes?,y)) pacsave
  if ($sizemap) {
    clear @pac_edit
    drawrect -fr @pac_editbuf 16777215 0 0 $4 $v1
  }
  if ($hget(paclv)) hfree paclv
  var %b $iif($hget(pe,tile),$v1,wall)
  if ($hget(pe)) hfree pe
  hadd -m pe tile wall
  hadd pe save 1
  hadd -m paclv size $1-2
  hadd paclv background 16777215
  var %a 0 | while (%a < $1) var %c $add_item(%a,0) ,%a %a + $3
  %a = 0 | while (%a < $2) var %c $add_item($calc($1 - $3),%a),%a %a + $4
  %a = 0 | while (%a < $2) var %c $add_item(0,%a),%a %a + $4
  %a = 0 | while (%a < $1) var %c $add_item(%a,$calc($2 - $4)),%a %a + $3
  hadd pe tile %b
  display
}

alias -l pacsave {
  if ($hget(paclv,name)) {
    hsave -o paclv $qt($v1)
    hadd pe save 1
  }
  else {
    var %d $scriptdir $+ $iif($exists($scriptdirmaps),maps\)
    while ($$sfile(%d $+ $iif($me,$me $+ 's map.plvl),Save as,Save)) {
      var %v $v1 $+ $iif($right($v1,5) != .plvl,$v2)
      if ($isfile($v1)) {
        if ($input(%v already exists $+ $crlf $+ Do you want to replace it ?,y)) break
        else return
      }
      else break
    }
    hadd paclv name %v
    hsave paclv $qt(%v)
    hadd pe save 1
  }
}

alias -l pacload {
  if (!$hget(pe,save)) && ($input(Do you want to save changes?,y)) pacsave
  if ($1- != $null) {
    var %s $qt($1-)
    hadd -m paclv size $sizewin
    hadd pe tile wall
    hadd paclv background 16777215
  }
  else var %d $scriptdir $+ $iif($exists($scriptdirmaps),maps\) ,%s $qt($$sfile(%d $+ *.plvl,Load,Load))
  if ($sizemap != $sizesprite) {
    clear @pac_edit
    drawrect -fr @pac_editbuf 16777215 0 0 $gettok($v2,2,32) $v1
  }
  hfree paclv
  hmake paclv
  hload paclv %s
  hadd paclv name %s
  hadd pe save 1
  display
}

alias -l sendkey .comopen a WScript.Shell | .comclose a $com(a,SendKeys,3,bstr,$1-)

alias -l antic hadd pe antic 1
menu @pac_edit {
  mouse : if ($hget(pe,antic)) hdel pe antic | else pacmouse
  sclick : add_item 
  dclick : del_item
  uclick : hdel pe deleting
  $antic
  New : pac_newlvl | hdel pe antic
  Load : pacload $antic
  Save : pacsave | hdel pe antic
  -
  $iif($hget(pe,tile) == wall,$style(1)) Wall : hadd pe tile wall | hdel pe antic | canc_teleport
  $iif($hget(pe,tile) == scoin,$style(1)) Pellet : hadd pe tile scoin | hdel pe antic | canc_teleport
  $iif($hget(pe,tile) == bcoin,$style(1)) Power pellet : hadd pe tile bcoin | hdel pe antic | canc_teleport
  Auto fill : auto_fill $sizesprite $sizemap
  -
  $iif($hget(pe,tile) == pacman,$style(1)) Pacman : hadd pe tile pacman | hdel pe antic | canc_teleport
  $iif(spawn* iswm $hget(pe,tile),$style(1)) Spawn area
  .$iif(spawnd == $hget(pe,tile),$style(1)) Down : hadd pe tile spawnd | hdel pe antic | canc_teleport
  .$iif(spawnr == $hget(pe,tile),$style(1)) Right : hadd pe tile spawnr | hdel pe antic | canc_teleport
  .$iif(spawnl == $hget(pe,tile),$style(1)) Left : hadd pe tile spawnl | hdel pe antic | canc_teleport
  .$iif(spawnu == $hget(pe,tile),$style(1)) Up : hadd pe tile spawnu | hdel pe antic | canc_teleport
  .-
  .$iif($hget(paclv,spawn),Delete) : {
    tokenize 32 $sizesprite x x $sizemap $replace($hget(paclv,spawn),.,$chr(32)) 
    drawcopy -n @pac_editbuf 0 $2 $5-6 @pac_edit 0 0
    del_spawn $+ $7 $1-
    hdel paclv spawn $+ $7 
    hdel paclv pacman
    hdel -w paclv ghost?
    drawdot @pac_edit 
    drawcopy @pac_edit 0 0 $sizemap @pac_editbuf 0 $2
    hdel paclv spawn
    hdel pe antic
  }
  -
  Background color : pbgcolor | hdel pe antic
  $iif($hget(pe,tile) == teleport,$style(1)) External Teleporter : hadd pe tile teleport | hdel pe antic

  $iif($hget(pe,tile) == iteleport,$style(1)) Internal Teleporter : hadd pe tile iteleport | hdel pe antic
  .$iif($pac_istel($mouse.x,$mouse.y),$iif($gettok($v1,5,32) == 1,$style(1)) Random Internal Teleporter) : {

    var %r $iif($gettok(%pactemprt,5,32) == 1,0,1)
    hadd paclv $gettok(%pactemprt,1-4,32) %r
    var %i $gettok(%pactemprt,3,32)
    hadd paclv %i $hget(paclv,%i) %r
  }
  Disable tile : hdel pe tile | hdel pe antic | canc_teleport
}


alias pac_istel {
  unset %pactemprt
  var %x $1,%y $2
  tokenize 32 $sizesprite $sizemap
  var %x $int($calc(%x / $1)),%y $int($calc(%y / $2))
  var %xmax $calc($3 / $1 - 1) ,%ymax $calc($4 / $2 - 1)

  if (%x == 0) dec %x
  elseif (%x == %xmax) inc %x
  if (%y == 0) dec %y
  elseif (%y == %ymax) inc %y
  if ($hget(paclv,$+(%x,.,%y)) != $null) && (teleport* iswm $v1) {
    set %pactemprt $+(%x,.,%y) $v2
    return $+(%x,.,%y) $v2
  }
}
alias -l auto_fill {
  var %a $calc($3 / $1) - 2,%b,%p $hget(pe,tile),%c $+($hget(paclv,pacman),.,$hget(paclv,ghost1),.,$hget(paclv,ghost2),.,$hget(paclv,ghost3),.,$hget(paclv,ghost4),$calc($3 -1) $4,.,$calc($3 +1) $4,.,$3 $calc($4 -1),.,$3 $calc($4 +1))
  hadd pe tile scoin
  while (%a > 0) {
    %b = $calc($4 / $2) - 2
    while (%b > 0) {
      var %i $+(%a,.,%b) ,%d $gettok($hget(paclv,%i),1,32)
      if (!$istok(wall scoin bcoin,%d,32)) && (*teleport* !iswm %d) && (!$istok(%c,$replace(%i,.,$chr(32)),46)) noop $add_item($calc(%a * $1),$calc(%b * $2))
      dec %b
    }
    dec %a
  }
  hadd pe tile %p
}

alias -l canc_teleport {
  if ($hget(pe,teleport)) {
    tokenize 32 $sizesprite $v1
    if ($5) drawcopy -t @pac_editbuf 16711935 0 0 $1-2 @pac_editbuf $calc($1 * $3) $calc($2 * $4 + $2) $1-2
    else drawrect -rfn @pac_edit $hget(paclv,background) 0 $calc($1 * $3) $calc($2 * $4 + $2) $1-2
    drawcopy -n @pac_editbuf 0 $2 $sizemap @pac_edit 0 0
    drawdot @pac_edit
    hdel pe teleport
  }
}

alias -l pbgcolor {
  tokenize 32 $input(Enter a rgb value (a number) or r g b value space seperated,e,Background color)
  if ($1 isnum 0- && ($2 == $null || ($2 isnum 0- && $3 isnum 0-))) {
    hadd -m paclv background $iif($0 == 3,$rgb($1,$2,$3),$1)
    display
    hdel pe save
  }
}

alias -l display {
  drawpic -tc @pac_edit 16777215 $calc( $window(@pac_edit).bw / 2 - $pic($scriptdirload.png).width / 2 ) $calc($window(@pac_edit).bh / 2 - $pic($scriptdirload.png).height / 2) $qt($scriptdirload.png)
  tokenize 32 $hget(paclv,size) $sizesprite
  window -af @pac_edit -1 -1 $1-2
  window -f @pac_editbuf -1 -1 $1 $calc($2 + $4)
  drawrect -rfn @pac_edit $hget(paclv,background) 0 0 0 $sizemap
  tokenize 32 $sizesprite

  var %a 1
  while ($hget(paclv,%a).item != $null) {
    var %i $v1,%d $hget(paclv,%i)
    if ($findtok(wall useless scoin bcoin,$gettok(%d,1,32),32)) drawcopy -tn @pac_editbuf 16711935 $calc(($v1 -1) * $1) 0 $1- @pac_edit $calc($gettok(%i,1,46)* $1) $calc($gettok(%i,2,46) * $2)
    elseif ($regex(%i,/^(ghost\d+|pacman)/)) {
      drawcopy -tn @pac_editbuf 16711935 $calc( $gettok(1 4 5 6 7 8 9, $iif(ghost? iswm %i,$calc($remove(%i,ghost) +1),1),32) * $1 ) 0 $1- @pac_edit $calc($gettok(%d,1,32) * $1) $calc($gettok(%d,2,32) * $2)
    }
    elseif ($regex(%d,/^teleport(\d+) ([^ ]+)\.([^ ]+) (.)(?: [01])?$/)) {
      var %x $gettok(%i,1,46), %y $gettok(%i,2,46)
      drawrect -frn @pac_edit 64512 0 $calc((%x $iif($regml(4) == l,+1,$iif($v1 == r,-1))) * $1) $calc((%y $iif($regml(4) == d,-1,$iif($v1 == u,+1))) * $2) $1-2
      drawtext -nr @pac_edit 0 verdana 12 $calc((%x $iif($regml(4) == l,+1,$iif($v1 == r,-1))) * $1 + $iif($len($regml(1)) != 1,-2,+3)) $calc((%y $iif($regml(4) == d,-1,$iif($v1 == u,+1))) * $2) $regml(1)
    }
    elseif ($regex(%d,/^iteleport(\d+?) ([^ ]+?)\.([^ ]+?)$/)) {
      var %x $gettok(%i,1,46), %y $gettok(%i,2,46)
      drawcopy -tn @pac_editbuf 16711935 $calc(10 * $1) 0 $1- @pac_edit $calc(%x * $1) $calc(%y * $2)
      drawtext -nr @pac_edit 0 verdana 12 $calc((%x $iif($regml(4) == l,+1,$iif($v1 == r,-1))) * $1 + $iif($len($regml(1)) != 1,-2,+3)) $calc((%y $iif($regml(4) == d,-1,$iif($v1 == u,+1))) * $2) $regml(1)
    }
    inc %a
  }
  if ($hget(paclv,spawn)) {
    tokenize 32 $1- $replace($v1,.,$chr(32))
    if ($3 == d) drawline -nr @pac_edit 255 1 $calc($4 * $1) $calc($5 * $2 -1) $calc($4  * $1 + 5 * $1) $calc($5 * $2 -1)
    elseif ($3 == r) drawline -nr @pac_edit 255 1 $calc($4 * $1 -1) $calc($5 * $2) $calc($4 * $1 -1) $calc($5 * $2 + 5 * $2)
    elseif ($3 == l) drawline -nr @pac_edit 255 1 $calc($4 * $1 - 0) $calc($5 * $2) $calc($4 * $1 - 0) $calc($5 * $2 + 5 * $2)
    else drawline -nr @pac_edit 255 1 $calc($4 * $1) $calc($5 * $2) $calc($4 * $1 + 5 * $1) $calc($5 * $2)
  }

  drawdot @pac_edit
  drawcopy @pac_edit 0 0 $sizemap @pac_editbuf 0 $2
}

alias -l show_spawnd {
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 + $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 + $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(4 * $1) 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 + 2 * $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(5 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 + $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(6 * $1) 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 + $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(7 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 + $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(8 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 + 2 * $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(9 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 + 2 * $2) $1-2

}

alias -l show_spawnr {
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 + $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 - $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(4 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(5 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 + $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(6 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(7 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 - $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(8 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 + $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(9 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 - $2) $1-2      
}

alias -l show_spawnl {
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 + $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 - $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 + 2 * $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(4 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - 2  *$1) $calc($2 * $4) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(5 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 + $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(6 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(7 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 - $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(8 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 + $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(9 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 - $2) $1-2         
}

alias -l show_spawnu {
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 - $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 - 2 * $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 - $2) $1-2
  drawcopy -tn @pac_edit 16711935 0 0 $1-2 @pac_edit $calc($1 * $3 + 2 * $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(4 * $1) 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 - 2 * $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(5 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - $1) $calc($2 * $4 - $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(6 * $1) 0 $1-2 @pac_edit $calc($1 * $3) $calc($2 * $4 - $2) $1-2      
  drawcopy -tn @pac_editbuf 16711935 $calc(7 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + $1) $calc($2 * $4 - $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(8 * $1) 0 $1-2 @pac_edit $calc($1 * $3 - 1 * $1) $calc($2 * $4 - 2 * $2) $1-2
  drawcopy -tn @pac_editbuf 16711935 $calc(9 * $1) 0 $1-2 @pac_edit $calc($1 * $3 + 1 * $1) $calc($2 * $4 - 2 * $2) $1-2 
}

alias -l pacmouse {
  tokenize 32 $sizesprite
  tokenize 32 $1- $int($calc($mouse.x / $1)) $int($calc($mouse.y / $2))
  renwin @pac_edit @pac_edit $3 $4
  drawcopy -n @pac_editbuf 0 $2 $sizemap @pac_edit 0 0
  if (spawn* !iswm $hget(pe,tile)) {
    if ($v2 == teleport) {
      drawrect -fnr @pac_edit 64512 0 $calc($1 * $3) $calc($2 * $4) $1-2
      var %a 1
      while ($hfind(paclv,teleport $+ %a ?*,1,w).data) {
        inc %a
      }
      var %l $iif($len(%a) != 1,-2,+3)
      drawtext -nr @pac_edit 0 verdana 12 $calc($1 * $3 %l) $calc($2 * $4) %a
    }
    elseif ($v1 == iteleport) {
      drawcopy -tn @pac_editbuf 16711935 $calc(10 * $1) 0 $sizesprite @pac_edit $calc($1 * $3) $calc($2 * $4) $sizesprite
      var %t $calc($hfind(paclv,iteleport*,0,w).data / 2 + 1),%l $iif($len(%t) != 1,-2,+3)
      drawtext -nr @pac_edit 0 verdana 12 $calc($1 * $3 %l) $calc($2 * $4) %t
    }
    elseif ($v1) {
      var %n $replace($v1,pacman,1,wall,0,scoin,2,bcoin,3,ghost1,4,ghost2,5, ghost3,6,ghost4,7,ghost5,8,ghost6,9)
      drawcopy -tn @pac_editbuf 16711935 $calc(%n * $1) 0 $sizesprite @pac_edit $calc($1 * $3) $calc($2 * $4) $sizesprite
    }
  }
  else {
    if ($v2 == spawnd) show_spawnd $1-
    elseif ($v1 == spawnr) show_spawnr $1-
    elseif ($v1 == spawnl) show_spawnl $1-
    else show_spawnu $1-
  }
  if ($mouse.key & 1) && (spawn* !iswm $hget(pe,tile)) && ($v2 != teleport) $iif($hget(pe,deleting),del_item,add_item)
  else drawdot @pac_edit
}

alias -l isinspawn {
  tokenize 32 $1- $replace($hget(paclv,spawn),.,$chr(32))
  if ($7 == d) && ($inrect($calc($3 * $1),$calc($4 * $2),$calc($8 * $1),$calc($9 * $2 - 3 * $2),70,42)) return 1
  elseif ($7 == r) && ($inrect($calc($3 * $1),$calc($4 * $2),$calc($8 * $1 - 3 * $1),$calc($9 * $2),42,70)) return 1
  elseif ($7 == l) && ($inrect($calc($3 * $1),$calc($4 * $2),$calc($8 * $1),$calc($9 * $2),42,70)) return 1
  elseif ($7 == u) && ($inrect($calc($3 * $1),$calc($4 * $2),$calc($8 * $1),$calc($9 * $2),70,42)) return 1
}

alias -l del_spawnd {
  drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($8 * $1) $calc($9 * $2 - 3 * $2) 70 42
  hdel paclv $8. $+ $calc($9 - 3)
  hdel paclv $8. $+ $calc($9 -1)
  hdel paclv $8. $+ $calc($9 -2)
  hdel paclv $+($calc($8 +1),.,$calc($9 - 3))
  hdel paclv $+($calc($8 +1),.,$calc($9 -1))      
  hdel paclv $+($calc($8 +1),.,$calc($9 -2))
  hdel paclv $+($calc($8 +2),.,$calc($9  -3))
  hdel paclv $+($calc($8 +2),.,$calc($9 -1))
  hdel paclv $+($calc($8 +2),.,$calc($9 -2))
  hdel paclv $+($calc($8 +3),.,$calc($9 - 3))
  hdel paclv $+($calc($8 +3),.,$calc($9 -1))
  hdel paclv $+($calc($8 +3),.,$calc($9 -2))
  hdel paclv $+($calc($8 +4),.,$calc($9 -3))
  hdel paclv $+($calc($8 +4),.,$calc($9 - 1))
  hdel paclv $+($calc($8 +4),.,$calc($9 -2))
  hdel -w paclv ghost*
}

alias -l del_spawnu {
  drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($8 * $1) $calc($9 * $2) 70 42
  hdel paclv $8. $+ $9
  hdel paclv $8. $+ $calc($9 +1)
  hdel paclv $8. $+ $calc($9 +2)
  hdel paclv $+($calc($8 +1),.,$9)
  hdel paclv $+($calc($8 +1),.,$calc($9 +1))      
  hdel paclv $+($calc($8 +1),.,$calc($9 +2))
  hdel paclv $+($calc($8 +2),.,$9)
  hdel paclv $+($calc($8 +2),.,$calc($9 +1))
  hdel paclv $+($calc($8 +2),.,$calc($9 +2))
  hdel paclv $+($calc($8 +3),.,$9)
  hdel paclv $+($calc($8 +3),.,$calc($9 +1))
  hdel paclv $+($calc($8 +3),.,$calc($9 +2))
  hdel paclv $+($calc($8 +4),.,$9)
  hdel paclv $+($calc($8 +4),.,$calc($9 +1))
  hdel paclv $+($calc($8 +4),.,$calc($9 +2))
  hdel -w paclv ghost*
}

alias -l del_spawnr {
  drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($8 * $1 - 3 * $1) $calc($9 * $2) 42 70
  hdel paclv $+($calc($8 -2),.,$9)
  hdel paclv $+($calc($8 -1),.,$9)
  hdel paclv $+($calc($8 - 3),.,$9) 
  hdel paclv $+($calc($8 -2),.,$calc($9 + 1))
  hdel paclv $+($calc($8 -1),.,$calc($9 + 1))
  hdel paclv $+($calc($8 - 3),.,$calc($9 + 1))
  hdel paclv $+($calc($8 -2),.,$calc($9 + 2))
  hdel paclv $+($calc($8 -1),.,$calc($9 + 2))
  hdel paclv $+($calc($8 - 3),.,$calc($9 + 2))
  hdel paclv $+($calc($8 -2),.,$calc($9 + 3))
  hdel paclv $+($calc($8 -1),.,$calc($9 + 3))
  hdel paclv $+($calc($8 - 3),.,$calc($9 + 3))
  hdel paclv $+($calc($8 -2),.,$calc($9 + 4))
  hdel paclv $+($calc($8 -1),.,$calc($9 + 4))
  hdel paclv $+($calc($8 - 3),.,$calc($9 + 4))
  hdel -w paclv ghost*
}

alias  -l del_spawnl {
  drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($8 * $1) $calc($9 * $2) 42 70
  hdel paclv $+($calc($8 +2),.,$9)
  hdel paclv $+($calc($8 +1),.,$9)
  hdel paclv $8. $+ $9 
  hdel paclv $+($calc($8 +2),.,$calc($9 + 1))
  hdel paclv $+($calc($8 +1),.,$calc($9 + 1))
  hdel paclv $8. $+ $calc($9 + 1)
  hdel paclv $+($calc($8 +2),.,$calc($9 + 2))
  hdel paclv $+($calc($8 +1),.,$calc($9 + 2))
  hdel paclv $8. $+ $calc($9 + 2)
  hdel paclv $+($calc($8 +2),.,$calc($9 + 3))
  hdel paclv $+($calc($8 +1),.,$calc($9 + 3))
  hdel paclv $8. $+ $calc($9 + 3)
  hdel paclv $+($calc($8 +2),.,$calc($9 + 4))
  hdel paclv $+($calc($8 +1),.,$calc($9 + 4))
  hdel paclv $8. $+ $calc($9 + 4)
  hdel -w paclv ghost*
}

alias -l add_item {
  var %x $1,%y $2
  tokenize 32 $sizesprite
  tokenize 32 $1- $int($calc($iif(%x != $null,%x,$mouse.x) / $1)) $int($calc($iif(%y != $null,%y,$mouse.y) / $2)) $sizemap
  if (!$isid) {
    if ((!$3) || (!$4) || ($calc($5 / $1 - 1) == $3) || ($calc($6 / $2 - 1) == $4)) {
      ;if click on edge, only external teleporter are allowed
      if ($hget(pe,tile) != teleport) return
    }
    ;if not on the edge, teleporter or nothing set as tile does nothing
    elseif ($hget(pe,tile) == teleport) || (!$v1) return
  }
  drawcopy -n @pac_editbuf 0 $2 $5-6 @pac_edit 0 0
  if (spawn* iswm $hget(pe,tile)) {
    tokenize 32 $1- $replace($hget(paclv,spawn),.,$chr(32))
    if ($hget(pe,tile) == spawnd) && (($3 < 3) || ($3 > $calc($5 / $1 - 4)) || ($4 < 1) || ($4 > $calc($6 / $2 - 5))) { var %cmd show_spawnd $1- | goto drawcop }
    elseif ($hget(pe,tile) == spawnu) && (($3 < 3) || ($3 > $calc($5 / $1 - 4)) || ($4 < 4) || ($4 > $calc($6 / $2 - 1))) { var %cmd show_spawnu $1- | goto drawcop }
    elseif ($hget(pe,tile) == spawnr) && (($3 < 1) || ($3 > $calc($5 / $1 - 5)) || ($4 < 3) || ($4 > $calc($6 / $2 - 4))) { var %cmd show_spawnr $1- | goto drawcop }
    elseif ($hget(pe,tile) == spawnl) && (($3 < 4) || ($3 > $calc($5 / $1 - 1)) || ($4 < 3) || ($4 > $calc($6 / $2 - 4))) { var %cmd show_spawnl $1- | goto drawcop }
    if ($7 == d) del_spawnd $1-
    elseif ($7 == r) del_spawnr $1-
    elseif ($7 == l) del_spawnl $1-
    elseif ($7 == u) del_spawnu $1-
    if ($hget(pe,tile) == spawnd) {
      drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($3 * $1 - 2 * $1) $calc($4 * $2) 70 42
      show_spawnd $1-
      hadd paclv $+($calc($3 -2),.,$4) wall
      hadd paclv $+($calc($3 -2),.,$calc($4 +1)) wall
      hadd paclv $+($calc($3 -2),.,$calc($4 +2)) wall
      hadd paclv $+($calc($3 -1),.,$4) wall
      hdel paclv $+($calc($3 -1),.,$calc($4 +1))
      hadd paclv ghost2 $calc($3 -1) $calc($4 +1)
      hdel paclv $+($calc($3 -1),.,$calc($4 +2))
      hadd paclv $3. $+ $4 wall
      hdel paclv $+($3,.,$calc($4 +1))
      hadd paclv ghost3 $3 $calc($4 +1)
      hdel paclv $+($3,.,$calc($4 +2))
      hadd paclv ghost1 $3 $calc($4 +2)
      hadd paclv $+($calc($3 +1),.,$4) wall
      hdel paclv $+($calc($3 +1),.,$calc($4 +1))
      hadd paclv ghost4 $calc($3 +1) $calc($4 +1)
      hdel paclv $+($calc($3 +1),.,$calc($4 +2))
      hadd paclv $+($calc($3 +2),.,$4) wall
      hadd paclv $+($calc($3 +2),.,$calc($4 +1)) wall
      hadd paclv $+($calc($3 +2),.,$calc($4 +2)) wall
      hadd paclv ghost5 $calc($3 -1) $calc($4 + 2)
      hadd paclv ghost6 $calc($3 +1) $calc($4 + 2)
      hadd paclv spawn d $+($calc($3 -2),.,$calc($4 +3)) $+($calc($3 +2),.,$calc($4 +3))
      drawline -nr @pac_edit 255 1 $calc($3 * $1 - 2 * $1) $calc($4 * $2  + 3 * $2 - 1)  $calc($3 * $1 + 3 * $1) $calc($4 * $2 + 3 * $2 - 1)
    }
    elseif ($v1 == spawnr) {
      drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($3 * $1) $calc($4 * $2 - 2 * $2) 42 70
      show_spawnr $1-
      hadd paclv $3. $+ $calc($4 - 2) wall
      hadd paclv $+($calc($3 + 1),.,$calc($4 - 2)) wall      
      hadd paclv $+($calc($3 + 2),.,$calc($4 - 2)) wall      
      hadd paclv $3. $+ $calc($4 - 1) wall
      hdel paclv $+($calc($3 +1),.,$calc($4 -1))
      hadd paclv ghost4 $calc($3 + 1) $calc($4 - 1)
      hdel paclv $+($calc($3 + 2),.,$calc($4 - 1))
      hadd paclv $3. $+ $4 wall      
      hdel paclv $+($calc($3 +1),.,$4)
      hadd paclv ghost3 $calc($3 + 1) $4
      hdel paclv $+($calc($3 +2),.,$4)
      hadd paclv ghost1 $calc($3 + 2) $4
      hadd paclv $3. $+ $calc($4 + 1) wall
      hdel paclv $+($calc($3 +1),.,$calc($4 +1))      
      hadd paclv ghost2 $calc($3 + 1) $calc($4 + 1)
      hdel paclv $+($calc($3 + 2),.,$calc($4 + 1))
      hadd paclv $3. $+ $calc($4 + 2) wall
      hadd paclv $+($calc($3 + 1),.,$calc($4 + 2)) wall      
      hadd paclv $+($calc($3 + 2),.,$calc($4 + 2)) wall   
      hadd paclv ghost5 $calc($3 +2) $calc($4 + 1)
      hadd paclv ghost6 $calc($3 +2) $calc($4 - 1)
      hadd paclv spawn r $+($calc($3 +3),.,$calc($4 -2)) $+($calc($3 +3),.,$calc($4 +3))
      drawline -nr @pac_edit 255 1 $calc($3 * $1 + 3 * $1 - 1) $calc($4 * $2 - 2 * $2) $calc($3 * $1 + 3 * $1 - 1) $calc($4 * $2 + 3 * $2)
    }
    elseif ($v1 == spawnl) {
      drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($3 * $1 - 2 * $1) $calc($4 * $2 - 2 * $2) 42 70
      show_spawnl $1-
      hadd paclv $3. $+ $calc($4 - 2) wall
      hadd paclv $+($calc($3 - 1),.,$calc($4 - 2)) wall      
      hadd paclv $+($calc($3 - 2),.,$calc($4 - 2)) wall      
      hadd paclv $3. $+ $calc($4 - 1) wall 
      hdel paclv $+($calc($3 -1),.,$calc($4 -1))
      hadd paclv ghost4 $calc($3 - 1) $calc($4 - 1)
      hdel paclv $+($calc($3 - 2),.,$calc($4 - 1))
      hadd paclv $3. $+ $4 wall      
      hdel paclv $+($calc($3 -1),.,$4)
      hadd paclv ghost3 $calc($3 - 1) $4
      hdel paclv $+($calc($3 -2),.,$4)
      hadd paclv ghost1 $calc($3 - 2) $4
      hadd paclv $3. $+ $calc($4 + 1) wall      
      hdel paclv $+($calc($3 -1),.,$calc($4 +1))
      hadd paclv ghost2 $calc($3 - 1) $calc($4 + 1)
      hdel paclv $+($calc($3 - 2),.,$calc($4 + 1))
      hadd paclv $3. $+ $calc($4 + 2) wall
      hadd paclv $+($calc($3 - 1),.,$calc($4 + 2)) wall      
      hadd paclv $+($calc($3 - 2),.,$calc($4 + 2)) wall 
      hadd paclv ghost5 $calc($3 -2) $calc($4 -1)
      hadd paclv ghost6 $calc($3 -2) $calc($4 +1)
      hadd paclv spawn l $+($calc($3 -2),.,$calc($4 -2)) $+($calc($3 -2),.,$calc($4 +3))
      drawline -nr @pac_edit 255 1 $calc($3 * $1 - 2 * $1) $calc($4 * $2 - 2 * $2) $calc($3 * $1 - 2 * $1 + 0) $calc($4 * $2 + 3 * $2)
    }
    else {
      drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($3 * $1 - 2 * $1) $calc($4 * $2 - 2 * $2) 70 42
      show_spawnu $1-
      hadd paclv $+($calc($3 -2),.,$4) wall
      hadd paclv $+($calc($3 -2),.,$calc($4 -1)) wall
      hadd paclv $+($calc($3 -2),.,$calc($4 -2)) wall
      hadd paclv $+($calc($3 -1),.,$4) wall
      hdel paclv $+($calc($3 -1),.,$calc($4 -1))
      hadd paclv ghost2 $calc($3 -1) $calc($4 -1)
      hdel paclv $+($calc($3 -1),.,$calc($4 -2))
      hadd paclv $3. $+ $4 wall
      hdel paclv $+($3,.,$calc($4 -1))
      hadd paclv ghost3 $3 $calc($4 -1)
      hadd paclv ghost1 $3 $calc($4 -2)
      hadd paclv $+($calc($3 +1),.,$4) wall
      hdel paclv $+($calc($3 +1),.,$calc($4 -1))
      hadd paclv ghost4 $calc($3 +1) $calc($4 -1)
      hdel paclv $+($calc($3 +1),.,$calc($4 -2))
      hadd paclv $+($calc($3 +2),.,$4) wall
      hadd paclv $+($calc($3 +2),.,$calc($4 -1)) wall
      hadd paclv $+($calc($3 +2),.,$calc($4 -2)) wall
      hadd paclv ghost5 $calc($3 +1) $calc($4 - 2)
      hadd paclv ghost6 $calc($3 -1) $calc($4 - 2)
      hadd paclv spawn u $+($calc($3 -2),.,$calc($4 -2)) $+($calc($3 +3),.,$calc($4 -2))
      drawline -nr @pac_edit 255 1 $calc($3 * $1 - 2 * $1 - 0) $calc($4 * $2 - 2 * $2) $calc($3 * $1 + 3 * $1 - 0) $calc($4 * $2 - 2 * $2)
    }
    if ($hget(paclv,pacman)) && ($isinspawn($1-2 $hget(paclv,pacman) 1 1)) hdel paclv pacman
  }
  elseif ($hget(pe,tile) == teleport) {
    var %i $iif(!$4,u,$iif(!$3,l,$iif($calc($5 / $1 -1) == $3,r,d)))
    ;corners are disallowed
    if ((!$3) && (($4 == $calc($6 / $2 - 1)) || (!$4))) $&
      || (($3 == $calc($5 / $1 - 1)) && ((!$4) || ($calc($6 / $2 - 1) == $4))) $&
      || ($left($hget(pe,teleport),-2) == $3 $4) $&
      || ($v1 == $calc($3 $iif($istok(u d,%i,32),-1)) $calc($4 $iif($istok(l r,%i,32),-1))) $&
      || ($v1 == $calc($3 $iif($istok(u d,%i,32),+1)) $calc($4 $iif($istok(l r,%i,32),+1))) $&
      || (teleport* iswm $hget(paclv,$+($calc($3 $iif(%i == r,+1,$iif(%i == l,-1,$iif($istok(u d,%i,32),-1)))),.,$calc($4 $iif(%i == d,+1,$iif(%i == u,-1,$iif($istok(r l,%i,32),-1))))))) $&
      || (teleport* iswm $hget(paclv,$+($calc($3 $iif(%i == r,+1,$iif(%i == l,-1,$iif($istok(u d,%i,32),+1)))),.,$calc($4 $iif(%i == d,+1,$iif(%i == u,-1,$iif($istok(r l,%i,32),+1))))))) $&
      || (teleport* iswm $hget(paclv,$+($calc($3 $iif(%i == r,+1,$iif(%i == l,-1))),.,$calc($4 $iif(%i == d,+1,$iif(%i == u,-1))))))  goto drawcop
    noop $del_item(%x,%y).prop 
    drawrect -fnr @pac_edit 64512 0 $calc($1 * $3) $calc($2 * $4) $1-2
    var %t 1
    while ($hfind(paclv,teleport $+ %t ?*,1,w).data) {
      inc %t
    }
    var %l $iif($len(%t) != 1,-2,+3)
    drawtext -nr @pac_edit 0 verdana 12 $calc($1 * $3 %l) $calc($2 * $4) %t
    var %d $iif(!$4,u,$iif(!$3,l,$iif($calc($5 / $1 -1) == $3,r,d)))
    if ($hget(pe,teleport)) {
      var %v $v1,%d $iif(!$4,u,$iif(!$3,l,$iif($calc($5 / $1 -1) == $3,r,d)))
      var %x1 $gettok(%v,1,32) - $iif($gettok(%v,3,32) == r,-1,$iif($v1 == l,1,0))
      var %y1 $gettok(%v,2,32) - $iif($gettok(%v,3,32) == d,-1,$iif($v1 == u,1,0))
      var %x2 $3 - $iif(%d == r,-1,$iif($v1 == l,1,0))
      var %y2 $4 - $iif(%d == d,-1,$iif($v1 == u,1,0))
      hadd paclv $+(%x1,.,%y1) teleport $+ %t $+(%x2,.,%y2) $gettok(%v,3,32)
      hadd paclv $+(%x2,.,%y2) teleport $+ %t $+(%x1,.,%y1) %d
      hdel pe teleport
    }
    else hadd pe teleport $3 $4 %d
  }
  elseif ($hget(pe,tile) == iteleport) {
    if (!$3) || (!$4) || ($4 == $calc($6 / $2 - 1)) || ($3 == $calc($5 / $1 - 1)) || ($hget(pe,teleport) == $3 $4) || (iteleport* iswm $hget(paclv,$+($3,.,$4))) goto drawcop
    noop $del_item(%x,%y).prop 
    var %t $calc($hfind(paclv,iteleport*,0,w).data / 2 + 1),%l $iif($len(%t) != 1,-2,+3)
    drawcopy -tn @pac_editbuf 16711935 $calc(10 * $1) 0 $sizesprite @pac_edit $calc($1 * $3) $calc($2 * $4) $sizesprite
    drawtext -nr @pac_edit 0 verdana 12 $calc($1 * $3 %l) $calc($2 * $4) %t
    if ($hget(pe,teleport)) {
      hadd paclv $+($3,.,$4) iteleport $+ %t $replace($v1,$chr(32),.)
      hadd paclv $replace($v1,$chr(32),.) iteleport $+ %t $3. $+ $4
      hdel pe teleport
    }
    else hadd pe teleport $3 $4
  }
  elseif (!$isinspawn($1-)) {
    noop $del_item(%x,%y).prop
    if ($hget(pe,tile) == pacman) {
      if ($hget(paclv,pacman)) drawrect -fnr @pac_edit $hget(paclv,background) 0 $calc($1 * $gettok($v1,1,32)) $calc($2 * $gettok($v1,2,32)) $1-2 
      hadd paclv $hget(pe,tile) $3 $4
    }
    else {
      var %bcoin
      if ($hget(pe,tile) == bcoin) && (!$hget(pe,askbcoin)) {
        while (!$istok(0 4 5 6 7 8 9 10 11 12 13 14 16,$input($+(Please enter the duration of the effect of the power-pellet (between 4 and 16 seconds),$crlf,Enter 0 and a default of 10 seconds will always be used for the next power-pellet),e,Pacman editor,10),32)) /
        if ($! == 0) { hadd pe askbcoin 1 | %bcoin = 10 }
        else %bcoin = $!
      }
      hadd paclv $3. $+ $4 $hget(pe,tile) %bcoin
    }
    var %n $replace($hget(pe,tile),pacman,1,wall,0,scoin,2,bcoin,3,ghost1,4,ghost2,5,ghost3,6,ghost4,7,ghost5,8,ghost6,9)
    drawcopy -tn @pac_editbuf 16711935 $calc(%n * $1) 0 $sizesprite @pac_edit $calc($1 * $3) $calc($2 * $4) $sizesprite
  }
  :drawcop
  drawcopy @pac_edit 0 0 $sizemap @pac_editbuf 0 $2
  %cmd
  if (!$isid) {
    drawdot @pac_edit
    hdel pe save
  }
}
alias -l del_item {
  var %x $1,%y $2
  tokenize 32 $sizesprite
  tokenize 32 $1- $int($calc($iif(%x != $null,%x,$mouse.x) / $1)) $int($calc($iif(%y != $null,%y,$mouse.y) / $2)) $sizemap
  var %xmax $calc($5 / $1 - 1) ,%ymax $calc($6 / $2 - 1)
  if (!$isid) && ((!$3) || (!$4) || (%xmax == $3) || (%ymax == $4)) {
    var %xx $3 $iif($3 == 0,- 1,$iif($3 == %xmax,+ 1,+ 0)),%yy $4 $iif($4 == 0,- 1,$iif($4 == %ymax,+ 1,+ 0))
    if (teleport* iswm $hget(paclv,$+(%xx,.,%yy))) {
      var %xxx $gettok($gettok($v2,2,32),1,46),%yyy $gettok($gettok($v2,2,32),2,46)
      hadd paclv $+($3,.,$4) wall
      hdel paclv $+(%xx,.,%yy)
      drawrect -frn @pac_edit $hget(paclv,background) 0 $calc($3 * $1) $calc($4 * $2) $1-2
      drawcopy -tn @pac_editbuf 16711935 0 0 $sizesprite @pac_edit $calc($1 * $3) $calc($2 * $4) $1-2
      var %xx %xxx $iif(%xxx == -1,+ 1,$iif($calc(%xxx - 1) == %xmax,- 1,+ 0)),%yy %yyy $iif(%yyy == -1,+ 1,$iif($calc(%yyy - 1) == %ymax,- 1,+ 0))
      hadd paclv $+(%xx,.,%yy) wall
      hdel paclv $+(%xxx,.,%yyy)
      drawrect -frn @pac_edit $hget(paclv,background) 0 $calc(%xx * $1) $calc(%yy * $2) $1-2
      drawcopy -tn @pac_editbuf 16711935 0 0 $1-2 @pac_edit $calc($1 * %xx) $calc($2 * %yy) $1-2
      drawdot @pac_edit
      drawcopy @pac_edit 0 0 $sizemap @pac_editbuf 0 $2
    }
    return
  }
  elseif (spawn* iswm $hget(pe,tile)) return 
  if ($isinspawn($1-)) return
  if ($3 isnum 1- $+ %xmax) && ($4 isnum 1- $+ %ymax) && (iteleport* iswm $hget(paclv,$+($3,.,$4))) {
    var %xxx $gettok($gettok($v2,2,32),1,46),%yyy $gettok($gettok($v2,2,32),2,46)
    hdel paclv $+($3,.,$4)
    hdel paclv $+(%xxx,.,%yyy)
    drawrect -frn @pac_edit $hget(paclv,background) 0 $calc(%xxx * $1) $calc(%yyy * $2) $1-2
    drawrect -frn @pac_edit $hget(paclv,background) 0 $calc($3 * $1) $calc($4 * $2) $1-2
    drawdot @pac_edit
    drawcopy @pac_edit 0 0 $sizemap @pac_editbuf 0 $2
    return
  }
  if ($hfind(paclv,$3-4,1).data) hdel paclv $v1
  else hdel paclv $3. $+ $4
  drawrect -fr $+ $iif($prop,n) @pac_edit $hget(paclv,background) 0 $calc($3 * $1) $calc($4 * $2) $1-2
  drawcopy @pac_edit 0 0 $sizemap @pac_editbuf 0 $2
  if (!$isid) hdel pe save
  if (!$prop) hadd pe deleting 1
}

on *:keydown:@pac_edit:37,38,39,40:{
  if (!$keyrpt) change_size $keyval
}

on ^*:close:@pac_edit:if (!$hget(pe,save)) && ($input(Do you want to save ?,yu)) pacsave | window -c @pac_editbuf | hfree paclv | hfree pe

alias changesizecb {
  var %1 $1,%xm $2,%ym $3,%ss $4,%5 $5
  tokenize 46 $5
  if (%1 == 37) && ($1 >= $calc(%xm - 1)) && (!$istok(0 $calc(%ym -1),$2,32)) && (!$istok(%ss,$+($1,.,$2),32)) return %5
  elseif (%1 == 38) && ($2 >= $calc(%ym - 1)) && (!$istok(0 $calc(%xm -1),$1,32)) && (!$istok(%ss,$+($1,.,$2),32)) return %5
}

alias -l change_size {
  if ($hget(paclv,spawn)) || ($hfind(paclv,teleport*,0,w).data) return $input(Can't change size with spawn area or teleport set,ou)
  var %1 $1 
  tokenize 32 $hget(paclv,size) $sizesprite
  tokenize 32 $iif($istok(38 40,%1,32),$1) $calc($iif($istok(37 39,%1,32),$1,$2) $iif($istok(37 38,%1,32),-,+) $iif($istok(37 39,%1,32),$3,$4) ) $iif($istok(37 39,%1,32),$2) $3-
  if ($1 < 420) || ($2 < 98) return $input(size too small,wou)
  else {

    if (%1 == 37) {
      var %a 0 
      while (%a < $calc($2 / $4)) var %ss %ss $+($calc(($1) / $3),.,%a) ,%a %a + 1 
    }
    elseif (%1 == 38) {
      var %a 0 
      while (%a < $calc($1 / $3)) var %ss %ss $+(%a,.,$calc(($2) / $4)) ,%a %a + 1 
    }
    elseif (%1 == 39) { 
      var %a 1 
      while (%a < $calc($2 / $4 - 1)) var %ss %ss $+($calc(($1 - 2 * $3) / $3),.,%a) ,%a %a + 1 
    }
    else {
      var %a 1 
      while (%a < $calc($1 / $3 - 1)) var %ss %ss $+(%a,.,$calc(($2 - 2 * $4) / $4)) ,%a %a + 1
    }
    var %xm $calc($1 / $3) ,%ym $calc($2 / $4),%a 1
    noop $hfind(paclv,/^\d+\.\d+$/,0,r,var %s %s $changesizecb(%1,%xm,%ym,%ss,$1))
    var %n $numtok(%s,32)
    if (%n) && (!$input(%n $iif(%n > 1,elements,element) will be lost ! $crlf $+ Are you sure ?,wyu)) return
    var %swx $1 ,%swy $2 ,%sx $3, %sy $4,%a 0
    tokenize 32 %s %ss
    while ($1 != $null) {
      var %x $gettok($1,1,46), %y $gettok($1,2,46)
      drawrect -fr @pac_editbuf $hget(paclv,background) 0 $calc(%x * %sx) $calc(%y * %sy + %sy) %sx %sy   
      hdel paclv $+(%x,.,%y)
      tokenize 32 $2-
    }
    window -f @pac_edit -1 -1 %swx %swy
    window -f @pac_editbuf -1 -1 %swx $calc(%swy + %sy)
    drawcopy -n @pac_editbuf 0 %sy %swx %swy @pac_edit 0 0
    var %tile $hget(pe,tile)
    hadd pe tile wall
    if ($istok(37 39,%1,32)) { 
      while (%a < %swy) var %c $add_item($calc(%swx - %sx + 1),%a),%a %a + %sy
    }
    else { 
      while (%a < %swx) var %c $add_item(%a,$calc(%swy - %sy + 1)),%a %a + %sx
    }
    hadd pe tile %tile
    drawcopy @pac_edit 0 0 %swx %swy @pac_editbuf 0 %sy
    drawdot @pac_edit
    hdel pe save
    hadd paclv size %swx %swy
  }
}

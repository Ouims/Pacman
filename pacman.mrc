dialog pac {
  title "Pacman - You are not connected to the lobby"
  size -1 -1 800 600
  option pixels
  icon $scriptdirpacman.ico, 0
  tab "Lobby ", 1, 2 2 796 601
  list 7, 675 28 121 301, disable tab 1 size
  list 10, 679 352 114 202, tab 1 size
  box "Servers Available:", 5, 676 333 119 224, tab 1
  edit "", 8, 6 560 670 35, tab 1 multi autovs vsbar limit 512
  edit "", 9, 6 28 669 530, disable tab 1 read multi vsbar
  button "&Connect", 46, 695 565 75 25, tab 1
  tab "Game ", 3
  edit "", 14, 6 560 670 35, tab 3 multi autohs autovs vsbar limit 512
  list 15, 677 32 115 210, disable tab 3 size check
  edit "", 16, 6 28 669 530, tab 3 read multi vsbar
  box "Maps played", 17, 679 300 113 125, tab 3
  list 18, 685 320 101 100, disable tab 3 size
  edit "", 19, 705 430 24 21, disable tab 3
  text "Lives", 20, 675 434 30 16, tab 3 right

  ;check "&Watch", 58, 700 450 82 82, disable tab 3
  icon 58, 710 244 50 50,  $scriptdirloff.png , tab 3 small noborder disable hide



  text "CPu(s)", 85, 740 434 32 16, tab 3 right
  edit "", 84, 772 430 20 21, disable tab 3
  button "&Disconnect", 88, 695 565 75 25, disable tab 3
  tab "Settings", 63
  scroll "", 21, 723 530 17 19, disable tab 63 range 1 99
  edit "", 22, 702 531 20 21, disable tab 63
  edit "", 23, 346 200 92 21, tab 63 pass autohs
  text "Password:", 24, 291 203 53 16, tab 63 right
  text "X", 25, 690 535 12 16, tab 63 center
  list 26, 9 316 176 214, disable tab 63 size
  box "Maps available", 27, 6 300 181 258, tab 63
  box "Moderation", 28, 508 300 286 258, tab 63
  list 29, 511 316 127 214, disable tab 63 size extsel
  button "&Pacman (@)", 30, 640 396 70 25, disable tab 63
  button "&Spectator", 31, 668 438 79 25, disable tab 63
  button "&Kick", 32, 640 344 70 25, disable tab 63
  box "Settings for server", 33, 6 95 788 171, tab 63
  text "*Tcp port:", 34, 64 142 53 16, tab 63 right
  text "*Udp port:", 35, 226 142 53 16, tab 63 right
  edit "", 36, 120 139 48 21, tab 63
  edit "", 37, 282 139 49 21, tab 63
  text "Cpus ghosts", 61, 516 535 61 20, disable tab 63
  button "Start", 38, 624 226 125 25, tab 63
  check "Private server", 39, 79 229 89 20, tab 63
  button "Identify with server key", 40, 618 155 125 25, tab 63
  button "Copy server key", 41, 618 118 125 25, disable tab 63
  button "&Edit", 44, 92 531 32 25, disable tab 63
  check "Remote server", 45, 290 229 92 20, tab 63
  button "show", 47, 441 199 41 21, disable tab 63
  button "&Ghost (~)", 48, 717 396 70 25, disable tab 63
  button "&Add", 53, 53 531 34 25, disable tab 63
  edit "", 50, 120 200 92 21, tab 63
  check "Server is ready", 55, 376 570 95 20, disable tab 63
  text "Server name:", 56, 51 203 67 16, tab 63 right
  text "Left:", 64, 12 50 26 20, tab 63
  text "Right:", 65, 9 70 28 20, tab 63
  text "Up:", 66, 151 50 19 20, tab 63
  text "Down:", 67, 146 70 30 20, tab 63
  button "37", 68, 43 45 100 20, tab 63
  button "39", 69, 43 68 100 20, tab 63
  button "38", 70, 180 48 100 20, tab 63
  button "40", 71, 180 68 100 20, tab 63
  button "&Join", 11, 687 53 75 25, disable tab 63
  edit "", 12, 568 45 92 21, tab 63 autohs group
  edit "", 79, 568 67 92 21, tab 63 autohs group
  text "Ip address:", 13, 500 48 64 16, tab 63 right
  box "Keys configuration", 4, 6 30 276 63, tab 63
  icon 81, 674 534 16 16,  " $scriptdirpacman.ico ", 0, tab 63 small noborder
  box "Nicknames", 59, 280 30 205 63, tab 63
  text "Lobby:", 72, 306 48 39 16, tab 63 right
  text "Game:", 74, 307 71 35 16, tab 63 right
  edit "", 75, 346 45 110 21, tab 63 center
  edit "", 76, 346 67 110 21, tab 63 center
  box "Join a private server", 77, 483 30 311 63, tab 63
  text "Tcp port:", 78, 517 71 47 16, tab 63 right
  button "Kick-&Ban", 80, 717 344 70 25, disable tab 63
  button "&New", 82, 8 531 39 25, tab 63
  button "Random", 83, 640 484 70 25, disable tab 63
  text "", 86, 24 573 310 14, tab 63 center
  combo 62, 585 533 41 80, disable tab 63 size drop
  button "Random All", 87, 717 484 70 25, disable tab 63
  button "&Infos", 42, 129 531 53 25, disable tab 63
  button ">>", 43, 191 411 75 25, disable tab 63
  list 49, 274 316 176 214, disable tab 63 size extsel
  button "&Delete All", 54, 360 531 67 25, disable tab 63
  button "De&lete", 52, 290 531 53 25, disable tab 63
  box "Ports settings", 60, 22 115 510 61, tab 63
  check "Upnp", 73, 450 140 40 20, tab 63
  box "Maps played", 51, 271 300 181 258, tab 63
  box "Options", 89, 22 180 510 76, tab 63
  box "Others", 90, 6 267 789 33, tab 63
  check "Dialog on top", 91, 10 283 85 14, tab 63
  list 6, 0 0 0 0
  button "send", 57, 0 0 0 0, hide default
  text "Lobby status: not connected", 2, 250 6 559 14, center
  check "@Pacman on top", 92, 95 283 100 14, tab 63
  check "Tabs blink on new messages", 93, 200 283 150 14, tab 63
  check "Preserve quality on resize", 94, 360 283 150 14, tab 63
  text "Disconnect from the current game if you want to run a server", 95, 590 190 200 30, tab 63 multi center hide
  check "Record Games", 96, 510 283 80 14, tab 63
  check "Highlight player", 97, 600 283 90 14, tab 63
  link "",98, 550 190 250 15, tab 63 hide
  link "",101, 575 190 200 15, tab 63 hide 
  link "",164, 630 190 150 15, tab 63 hide 

  check "Disable Nagle", 99, 700 283 90 14, tab 63
  check "Dual Stack", 100, 360 140 70 20, tab 63
  tab "Tips", 102
  text "", 103, 10 35 750 30, tab 102 multi
  text "", 104, 10 70 780 30, tab 102 multi
  text "",105, 10 105 780 50, tab 102 multi
  text "",127, 10 140 780 60, tab 102 multi

  icon 106, 20 220  28 28,   $scriptdircherry.png , tab 102 noborder large
  text "Cherry",107, 18 200 50 20, tab 102

  icon 108, 110 220  28 28,   $scriptdirstraw.png , tab 102 noborder large
  text "Strawberry",109, 99 200 60 20, tab 102

  icon 110, 200 220  28 28,   $scriptdirgre.png , tab 102 noborder large
  text "Grenade",111, 195 200 60 20, tab 102

  icon 112, 290 220  28 28,   $scriptdirapple.png , tab 102 noborder large
  text "Apple",113, 291 200 50 20, tab 102

  icon 114, 380 220  28 28,   $scriptdirbanana.png , tab 102 noborder large
  text "Banana",115, 378 200 60 20, tab 102

  icon 116, 470 220  28 28,   $scriptdirorange.png , tab 102 noborder large
  text "Orange",117, 467 200 60 20, tab 102

  text "200",118, 24 250 30 20, tab 102
  text "250",119, 114 250 30 20, tab 102
  text "300",120, 204 250 30 20, tab 102
  text "350",121, 294 250 30 20, tab 102
  text "400",122, 384 250 30 20, tab 102
  text "500",123, 474 250 30 20, tab 102

  icon 124, 560 220  28 28,   $scriptdirheart.png , tab 102 noborder large
  text "Heart",125, 561 200 60 20, tab 102
  text "50",126, 568 250 30 20, tab 102
  icon 128, 650 220  28 28,   $scriptdir1up.png , tab 102 noborder large
  text "1up",131, 654 200 60 20, tab 102

  icon 129, 740 220  28 28,   $scriptdirvf.png , tab 102 noborder large
  text "Vertical flip",132, 730 200 60 20, tab 102

  ; icon 130, 10 320  28 28,   $scriptdirinv.png , tab 102 noborder large
  ; text "Invisible",133, 10 305 60 20, tab 102
  ;text "50",134, 657 250 30 20, tab 102
  text "50",135, 746 250 30 20, tab 102

  icon 136, 20 290  28 28,   $scriptdirinv.png , tab 102 noborder large
  text "Invisible",137, 18 270 50 20, tab 102

  icon 138, 110 290  28 28,   $scriptdirhf.png , tab 102 noborder large
  text "Horizontal flip",139, 95 270 65 20, tab 102


  icon 140, 200 290  28 28,   $scriptdirts+.png , tab 102 noborder large
  text "Team Speed+ ",141, 185 270 70 20, tab 102

  icon 142, 290 290  28 28,   $scriptdirspeed-.png , tab 102 noborder large
  text "Speed-",143, 288 270 50 20, tab 102

  icon 144, 380 290  28 28,   $scriptdirts-.png , tab 102 noborder large
  text "Team Speed-",145, 365 270 65 20, tab 102

  icon 146, 470 290  28 28,   $scriptdirrt.png , tab 102 noborder large
  text "Random Teleport",147, 450 270 85 20, tab 102

  icon 148, 560 290  28 28,   $scriptdirspeed+.png , tab 102 noborder large
  text "Speed+",149, 559 270 85 20, tab 102

  icon 150, 650 290  28 28,   $scriptdirshield.png , tab 102 noborder large
  text "Shield",151, 650 270 85 20, tab 102

  icon 152, 740 290  28 28,   $scriptdirmult.png , tab 102 noborder large
  text "Eat time multiplier",153, 710 270 85 20, tab 102

  text "50",154, 27 320 30 20, tab 102
  text "50",155, 117 320 30 20, tab 102
  text "50",156, 207 320 30 20, tab 102
  text "50",157, 297 320 30 20, tab 102
  text "50",158, 387 320 30 20, tab 102
  text "50",159, 477 320 30 20, tab 102
  text "50",160, 567 320 30 20, tab 102
  text "50",161, 657 320 30 20, tab 102
  text "50",162, 747 320 30 20, tab 102
  text "Heart from green egg won't blink, they are permanent, flip means the game window's view is flipped. Invisible player can still be seen by a little dot of their color that is drawn onto them. Multipler acts for the next taken power pellet, x0.5 and x1.5 for ghosts and pacmans respectively. Shields disappear automatically after 18s, they protect ghosts from becoming eatable by pacmans, but an eatable ghost taking a shield stays eatable. Ghosts lose the shield when eaten or when killing a pacman. Pacmans lose the shield when colliding with a ghost which would have resulted in a life loss otherwise, the shield will flicker temporarily indicating that pacman is invincible for that time period. If both have a shield, nothing happens. Shields also protect ghosts once from dying from the mIRC friend. If a pacman dies or lose a shield to a ghost, the ghost is 'stunt' and will move slowly for a few seconds, such ghost is dark grey. When pacmans lose lives, all ghosts become eatable for a few second as well. Eating a ghost is worth 200 points",163, 10 350 780 80, tab 102 multi




}

alias pac_reinstall {
  set %pac_reinstall 1 
  var %i $input(Do you want to keep your current settings?,vnyu,Pacman - Reinstall)
  if (%i == $cancel) return $input(Reinstall aborted,tuo,Pacman - Reinstall)
  elseif (%i == $yes) && (;* iswm $read($script,tn,1)) hadd -m pacman update $v2
  pac_update?
}
alias pac_version return 4.78
alias pac_autoupdate {
  ;return 1
  var %r $read($script,tn,1)
  if (;* iswm %r) && ($gettok(%r,7,32) == 1) return 0
  return 1
}

alias keyvaltodesc {
  var %list 08 BACKSPACE.09 TAB.0C CLEAR.0D ENTER.10 SHIFT.11 CTRL.12 ALT.13 PAUSE.14 CAPS LOCK.15 IME Kana/Hangul mode.16 IME On.17 IME Junja mode.18 IME final mode.19 IME Hanja mode.19 IME Kanji mode.1A IME Off.1B ESC.1C IME convert.1D IME nonconvert. $+ $&
    1E IME accept.1F IME mode change request.20 SPACEBAR.21 PAGE UP.22 PAGE DOWN.23 END.24 HOME.25 LEFT ARROW.26 UP ARROW.27 RIGHT ARROW.28 DOWN ARROW.29 SELECT.2A PRINT.2B EXECUTE.2C PRINT SCREEN.2D INSERT.2E DELETE.2F HELP.30 0.31 1.32 2.33 3.34 4.35 5.36 6.37 7. $+ $&
    38 8.39 9.41 A.42 B.43 C.44 D.45 E.46 F.47 G.48 H.49 I.4A J.4B K.4C L.4D M.4E N.4F O.50 P.51 Q.52 R.53 S.54 T.55 U.56 V.57 W.58 X.59 Y.5A Z.5B LEFT WINKEY.5C RIGHT WINKEY.5D APPLICATIONKEY.5F COMPUTER SLEEP.60 NUMPAD 0.61 NUMPAD 1.62 NUMPAD 2.63 NUMPAD 3. $+ $&
    64 NUMPAD 4.65 NUMPAD 5.66 NUMPAD 6.67 NUMPAD 7.68 NUMPAD 8.69 NUMPAD 9.6A MULTIPLY.6B ADD.6C SEPARATOR.6D SUBSTRACT.6E DECIMAL.6F DIVIDE.70 F1.71 F2.72 F3.73 F4.74 F5.75 F6.76 F7.77 F8.78 F9.79 F10.7A F11.7B F12.7C F13.7D F14.7E F15.7F F16. $+ $&
    80 F17.81 F18.82 F19.83 F20.84 F21.85 F22.86 F23.87 F24.90 NUM LOCK.91 SCROLL LOCK.92 OEM.93 OEM.94 OEM.95 OEM.96 OEM.A0 LEFT SHIFT.A1 RIGHT SHIFT.A2 LEFT CONTROL.A3 RIGHT CONTROL.A4 LEFT MENU.A5 RIGHT MENU.A6 BROWSER BACK.A7 BROWSER FORWARD. $+ $&
    A8 BROWSER REFRESH.A9 BROWSER STOP.AA BROWSER SEARCH.AB BROWSER FAVORITES.AC BROWSER HOME.AD VOLUME MUTE.AE VOLUME DOWN.AF VOLUME UP.B0 MEDIA NEXT TRACK.B1 MEDIA PREVIOUS TRACK.B2 MEDIA STOP.B3 MEDIA PLAY/PAUSE.B4 LAUNCH MAIL.B5 LAUNCH MEDIA SELECT. $+ $&
    B6 LAUNCH_APP1.B7 LAUNCH APP2.BA OEM 1.BB OEM PLUS.BC OEM COMMA.BD OEM MINUS.BE OEM PERIOD.BF OEM 2.C0 OEM 3.DB OEM 4.DC OEM 5.DD OEM 6.DE OEM 7.DF OEM 8.E1 OEM specific.E2 OEM 102.E3 OEM specific.E4 OEM specific.E5 IME PROCESS.E6 OEM specific. $+ $&
    E9 OEM specific. EA OEM specific.EB OEM specific.EC OEM specific.ED OEM specific.EF OEM specific.F0 OEM specific.F1 OEM specific.F2 OEM specific.F3 OEM specific.F4 OEM specific.F5 OEM specific.F6 ATTN.F7 CRSEL.F8 EXSEL.F9 EREOF.FA PLAY.FB ZOOM.FD PA1.FE OEM CLEAR
  var %w $wildtok(%list,$base($1,10,16,2) *,1,46)
  tokenize 32 %w
  return $2-
}

alias pac_update? {
  if ($pac_autoupdate) || (%pac_reinstall == 1) {
    if (!$sslready) return $input(You don't have SSL capabilities. SSL is required for updating Pacman $+ $crlf See http://www.mirc.com/ssl.html for more information,wuo,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    sockclose pac_ckupd
    sockopen -e pac_ckupd gitlab.com 443
    sockmark pac_ckupd $false
    write -c $qt($scriptdirupdate.upd)
  }
}

on *:sockopen:pac_ckupd:{
  if (!$sockerr) {
    echo -sg first on sockopen $calc($ticksqpc - %ticks)
    sockwrite -n $sockname GET /Ouims/Pacman/raw/master/update.pac HTTP/1.1
    sockwrite -n $sockname Host: $sock($sockname).addr
    sockwrite -n $sockname Connection: close
    sockwrite -n $sockname
  }
  else {
    .timer -ho 1 0 noop $!input(Cannot connect to the server $!+ $!chr(44) aborting the $!iif(%pac_reinstall,reinstall,update) $!+ ..,oh,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    unset %pac_reinstall
    .remove $qt($scriptdirupdate.upd)
  }
}

on *:sockread:pac_ckupd:{
  if (!$sockerr) {
    if ($sock($sockname).mark) {
      sockread -f &a 
      bwrite $qt($scriptdirupdate.upd) -1 -1 &a
    }
    else {
      var %a
      sockread %a
      if (!$sockbr) return
      if (%a == $null) sockmark $sockname 1 
      elseif ($sock($sockname).mark == $false) {
        if (*200 OK* !iswm %a) {
          .timer -ho 1 0 noop $!input(Cannot find the list of files on the server $!+ $!chr(44) aborting the $!iif(%pac_reinstall,reinstall,update) $!+ ..,ho,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
          sockclose $sockname
          unset %pac_reinstall
          .remove $qt($scriptdirupdate.upd)
        }
        sockmark $sockname 0
      }
    }
  }
  else {
    .timer -ho 1 0 noop $!input(An error occured while downloading the list of files $!+ $!chr(44) aborting the $!iif(%pac_reinstall,reinstall,update) $!+ ..,ho,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    .remove $qt($scriptdirupdate.upd)
    unset %pac_reinstall
  }
}

on *:sockclose:pac_ckupd:{
  if ($sockerr) {
    .timer -ho 1 0 noop $!input(An SSL error occured while trying to connect to the server $!+ $!crlf $!+ You have invalid SSL settings in option > connect > options > ssl dialog $!+ $!chr(44) aborting the $!iif(%pac_reinstall,reinstall,update) $!+ ..,ho,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    return
  }
  .timer -ho 1 0 if (%pac_reinstall) || (($read($scriptdirupdate.upd,tn,1) > $!pac_version ) && ($input($+(A new version of Pacman is available !,$crlf,Do you want to update ?,$crlf,New version: $!read($scriptdirupdate.upd,tn,1)),qyu,Pacman - update))) pac_update
}



alias -l pac_update {
  if ($sock(pacclient).status isin activeconnecting) {
    set %pacclient_fconnect $sock(pacclient).ip $sock(pacclient).port
  }
  elseif (%pacclient_retry) {
    set %pacclient_fconnect $v1
    unset %pacclient_retry
  }
  if ($dialog(pacman)) dialog -c pacman
  if (!$isdir($scriptdirmaps)) mkdir $qt($scriptdirmaps)
  if (;* iswm $read($script,tn,1)) hadd -m pacman update $v2
  hadd -m pacman menu $read($scriptdirpacman.mrc,rnt,^(?:menu (?!@pac_watch)[^ ]+|nomenu) $({,0))
  set %pacprogress $int($calc(200 / ($lines($scriptdirupdate.upd) - 1)))
  window -kpdofC +l $+(@,$iif(%pac_reinstall,Reinstalling,Updating),$chr(160),Pacman...) -1 -1 200 40
  var %a 2
  while ($read($scriptdirupdate.upd,tn,%a)) { .timerpacupdate $+ %a -ho 1 $calc(%a * 0) pac_dl_update $unsafe(%a $v1 $+ .upd) | inc %a }
  set %pacupdate %a - 2
}
on *:close:$($+(@,$iif(%pac_reinstall,Reinstalling,Updating),$chr(160),Pacman...)):var %p %pac_reinstall | noop $pac_updateabort | .timer -ho 1 0 noop $!input(The $iif(%p,reinstall,update) was aborted,iou,Pacman - $iif(%p,Reinstall,Update) )
alias -l pac_dl_update {
  sockclose pac_update $+ $1
  sockopen -e pac_update $+ $1 gitlab.com 443
  sockmark pac_update $+ $1 $2-
}

on *:sockopen:pac_update?*:{
  if (!$sockerr) {
    .fopen -no $sockname $qt($scriptdir $+ $sock($sockname).mark)
    sockwrite -n $sockname GET /Ouims/Pacman/raw/master/ $+ $left($sock($sockname).mark,-4) HTTP/1.1
    sockwrite -n $sockname Host: $sock($sockname).addr
    sockwrite -n $sockname Connection: close
    sockwrite -n $sockname
    sockmark $sockname $false
  }
  else {
    .timer -ho 1 0 noop $!input(Cannot connect to the server to download the file $nopath($left($sock($sockname).mark,-4)) $!+ $!chr(44) aborting the $!iif(%pac_reinstall,reinstall,update) $!+ ..,oh,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    pac_updateabort
  }
}

on *:sockread:pac_update?*:{
  if (!$sockerr) {
    if ($sock($sockname).mark) {
      sockread -f &a
      .fwrite -b $sockname &a
    }
    else {
      var %a
      sockread %a
      if (!$sockbr) return
      if (%a == $null) sockmark $sockname 1
      elseif ($sock($sockname).mark == $false) {
        if (*200 OK* !iswm %a) {
          .timer -ho 1 0 noop $!input(Cannot find the file $nopath($left($fopen($sockname).fname,-4)) on the server $!+ $!chr(44) aborting the $iif(%pac_reinstall,reinstall,update) $+ ..,ho,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
          pac_updateabort
        }
        sockmark $sockname 0
      }
    }
  }
  else {
    .timer -ho 1 0 noop $!input(An error occured while downloading the file $nopath($left($fopen($sockname).fname,-4)) $!+ $!chr(44) aborting the $iif(%pac_reinstall,reinstall,update) $+ ..,ho,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    pac_updateabort
  }
}

alias -l pac_updateabort {
  .timerpacupdate* off
  .fclose pac_update?*
  sockclose pac_update?*
  var %a 2
  while ($read($scriptdirupdate.upd,tn,%a) != $null)  {
    .remove $qt($scriptdir $+ $v1 $+ .upd)
    inc %a
  }
  .remove $qt($scriptdirupdate.upd)
  hdel pacman update
  hdel pacman menu 
  window -c $+(@,$iif(%pac_reinstall,Reinstalling,Updating),$chr(160),Pacman...)
  unset %pacupdate %pacprogress %pac_reinstall
}

on *:sockclose:pac_update?*:{
  dec %pacupdate
  .fclose $sockname
  drawrect -fr $+(@,$iif(%pac_reinstall,Reinstalling,Updating),$chr(160),Pacman...) 64512 0 0 0 $calc(200 - %pacupdate * %pacprogress) 40
  if (!%pacupdate) {
    unset %pacupdate %pacprogress 
    window -c $+(@,$iif(%pac_reinstall,Reinstalling,Updating),$chr(160),Pacman...)
    .timer  -ho 1 0 pac_updatef
  }
}
alias -l pac_updatef {
  var %a 2,%v,%e
  while ($read($scriptdirupdate.upd,tn,%a)) {
    %v = $v1
    if (!$exists($scriptdir $+ %v $+ .upd)) || (!$file($scriptdir $+ %v $+ .upd)) %e = $addtok(%e,%v,63)
    inc %a
  }
  if (%e) {
    .timer -ho 1 0 noop $!input($+(Some file couldn't be downloaded:,$crlf, [ $replace(%e,?,$crlf) ] , $crlf , aborting the $iif(%pac_reinstall,reinstall,update) $+ ..),ho,Pacman - $iif(%pac_reinstall,Reinstall,Update) )
    return
  }
  else {
    %a = 2
    %v =
    while ($read($scriptdirupdate.pac,tn,%a)) {
      %v = $v1
      .remove $qt($scriptdir $+ %v)
      inc %a
    }
    %a = 2
    %v =
    while ($read($scriptdirupdate.upd,tn,%a)) {
      %v = $v1
      if ($exists($scriptdir $+ %v)) .remove $qt($scriptdir $+ %v)
      .rename $qt($scriptdir $+ %v $+ .upd) $qt($scriptdir $+ %v)
      if (*.mrc iswm %v) && (pacman.mrc !iswm %v) .reload -rs $qt($scriptdir $+ %v)
      inc %a
    }
    .remove $qt($scriptdirupdate.pac)
    .rename $qt($scriptdirupdate.upd) $qt($scriptdirupdate.pac)
    if (%pac_reinstall)  {
      .timer 1 1 pacman $!input(Reinstall finished,to,Pacman - Reinstall) $!pac_reinstallf
      if ($hget(pacman,update)) {
        write -il1 $qt($scriptdirpacman.mrc) $v1
        hdel pacman update
      }
      else write -il1 $qt($scriptdirpacman.mrc)  ;37 38 39 40 - - 0 0 0 1 0 0 1 1 1 0 8000 8001
      if (!$isalias(f5)) {
        write $qt($script(pac_client.mrc)) alias f5 if ($sock(pacclient)) sockwrite -n pacclient pause
        .reload -rs $qt($script(pac_client.mrc))
      }
      unset %pac_reinstall
      .reload -rs $qt($script)
    }
    else {
      .timer 1 1 pacman $!input(Update finished,to,Pacman - Update)
      if (!$exists($scriptdirrecords\)) mkdir $qt($scriptdirrecords)
      if ($hget(pacman,update)) {
        write -il1 $qt($scriptdirpacman.mrc) $v1
        hdel pacman update
      }
      if ($hget(pacman,menu)) {
        tokenize 32 $v1
        noop $read($scriptdirpacman.mrc,ntw,menu & $({,0))
        write -l $+ $readn $qt($scriptdirpacman.mrc) $1-
        hdel pacman menu
      }
      .reload -rs $qt($script)
    }
  }
}

alias -l pac_reinstallf {
  var %m1 menubar,%m2 menubar,status,%m3 menubar,channel, %m4 menubar,query,%m5 menubar,status,channel,%m6 menubar,status,query,%m7 menubar,status,channel,query,%m8 No menu, please
  var %menu $input(Please select some popups locations for Pacman,miu,Pacman - Reinstall,%m1,%m1,%m2,%m3,%m4,%m5,%m6,%m7,%m8)
  var %read $read($script,wnt,menu & $({,0))
  if (%menu != No menu, please) && (%menu) write -l $+ $readn $qt($script) menu %menu $({,0)
  else write -l $+ $readn $qt($script) nomenu $({,0)
  .reload -rs $qt($script)
}

menu menubar {
  $iif(%pacupdate != $null,$style(2)) Pacman
  .Run : pacman
  .$iif(!$isalias(peditor),$style(2)) Map editor :  peditor
  .Watch Recorded Games:pac_record
  .Reinstall : if ($input(Are you sure you want to reinstall pacman?,uyw,Pacman - Reinstall)) pac_reinstall
  .Unload : if ($input(Are you sure you want to unload pacman?,uyw,Pacman - Unload)) pac_unload
}

alias -l pac_unload {
    
    if ($isalias(pacclient_stop)) pacclient_stop
    if ($isalias(pacserv_stop)) pacserv_stop
    if ($dialog(pacman)) dialog -x pacman
    var %a 2,%v
    while ($read($scriptdirupdate.pac,tn,%a)) {
      var %v = $v1
      if ($exists($scriptdir $+ %v)) {
        if ($right(%v,4) == .mrc) && (%v != pacman.mrc) && ($script($scriptdir $+ %v)) .unload -rs $qt($scriptdir $+ %v)
      }
      inc %a
    }
    noop $input($+(Pacman unloaded successfully,$crlf,If you want to reload it,$chr(44),$chr(32),you'll need to reload the pacman.mrc file in $scriptdir),iuo,Pacman - Unload)
    .unload -rs $qt($script)
  }

alias -l pacreg return $+(/\bpcmn,$chr(58),\/\/(?|([^@]+)@(\d+)|([A-Za-z\d]+))/)
on $*:hotlink:$($pacreg):*:{
  if ($regml(2) != $null) {
    if ($iptype($regml(1))) || (. isin $regml(1)) && ($regml(2) isnum 1-65535) {
      if ($hotlink(event) == rclick) { set %pcmn $regml(1) $regml(2) | hotlink -m @pacman_protocol }
      elseif ($v1 == dclick) {
        pacman
        var %ip $regml(1),%port $regml(2)
        if ($sock(pacclient)) && (!$input(You are already connected to a server $+ $crlf $+ Do you want to quit the current server and connect to $regml(1) $+ ?,wyu,Pacman - Protocol)) return
        .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ip) , $unsafe(%port) )
      }
      else {
        return
      }
    }
  }
  else {
    var %d $base($regml(1),36,16),%r $regml(1)
    if ($len(%d) > 36) {
      var %ipv6 $left($regsubex($left(%d,32),/(?|000(.)|00(..)|0(...)|(....))/g,\1 $+ :),-1)
      var %ipv4 $longip($base($mid(%d,33,8),16,10)),%port $base($mid(%d,41),16,10)
    }
    elseif ($v1 >= 30) {
      var %ipv6 $left($regsubex($left(%d,32),/(?|000(.)|00(..)|0(...)|(....))/g,\1 $+ :),-1)
      var %port $base($mid(%d,33),16,10)
    }
    else {
      var %ipv4 $longip($base($left(%d,8),16,10)),%port $base($mid(%d,9),16,10)
    }
    if ($hotlink(event) == rclick) { set %pcmn %r | hotlink -m @pacman_protocol }
    elseif ($v1 == dclick) {

      pacman
      if (%ipv6) {
        if ($bindip(::)) {
          if ($sock(pacclient)) && (!$input(You are already connected or connecting to a server $+ $crlf $+ Do you want to quit the current server and connect to %ipv6 $+ ?,wyu,Pacman - Protocol)) return
          .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ipv6) , $unsafe(%port) ) $(|) if ($qt( $unsafe(%ipv4) ) != "") set % $+ pacclient_retry $unsafe(%ipv4 %port)
        }
        elseif (%ipv4) {
          if ($sock(pacclient)) && (!$input(You are already connected or connecting to a server $+ $crlf $+ Do you want to quit the current server and connect to %ipv4 $+ ?,wyu,Pacman - Protocol)) return
          .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ipv4) , $unsafe(%port) )
        }
        else echo -sg * Pacman trying to connect to ipv6 only server but you don't have ipv6 capability, try enabling dual stack for the server
      }
      else {
        if ($sock(pacclient)) && (!$input(You are already connected or connecting to a server $+ $crlf $+ Do you want to quit the current server and connect to %ipv4 $+ ?,wyu,Pacman - Protocol)) return
        .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ipv4) , $unsafe(%port) )
      }
    }
  }
}

menu @pacman_protocol {
  Copy to clipboard:clipboard %pcmn | unset %pcmn
  Connect to the pacman server:{
    pacman
    if ($numtok(%pcmn,32) == 2) {

      if ($sock(pacclient)) && (!$input(You are already connected to a server $+ $crlf $+ Do you want to quit the current server and connect to $1 $+ ?,wyu,Pacman - Protocol)) return
      tokenize 32 %pcmn
      .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe($1) , $unsafe($2) ) $(|) unset % $+ pcmn
    }
    else {

      var %d $base(%pcmn,36,16)
      if ($len(%d) > 36) {
        var %ipv6 $left($regsubex($left(%d,32),/(?|000(.)|00(..)|0(...)|(....))/g,\1 $+ :),-1)
        var %ipv4 $longip($base($mid(%d,33,8),16,10)),%port $base($mid(%d,41),16,10)
      }
      elseif ($v1 >= 30) {
        var %ipv6 $left($regsubex($left(%d,32),/(?|000(.)|00(..)|0(...)|(....))/g,\1 $+ :),-1)
        var %port $base($mid(%d,33),16,10)
      }
      else {
        var %ipv4 $longip($base($left(%d,8),16,10)),%port $base($mid(%d,9),16,10)
      }

      if (%ipv6) {

        if ($bindip(::)) {
          if ($sock(pacclient)) && (!$input(You are already connected to a server $+ $crlf $+ Do you want to quit the current server and connect to %ipv6 $+ ?,wyu,Pacman - Protocol)) return
          .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ipv6) , $unsafe(%port) ) $(|) if ($qt( $unsafe(%ipv4) ) != "") set % $+ pacclient_retry $unsafe(%ipv4 %port) $(|) unset % $+ pcmn
        }
        elseif (%ipv4) {
          if ($sock(pacclient)) && (!$input(You are already connected to a server $+ $crlf $+ Do you want to quit the current server and connect to %ipv4 $+ ?,wyu,Pacman - Protocol)) return
          .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ipv4) , $unsafe(%port) ) $(|) unset % $+ pcmn
        }
        else echo -sg * Pacman trying to connect to ipv6 only server but you don't have ipv6 capability, try enabling dual stack for the server
      }
      else {
        if ($sock(pacclient)) && (!$input(You are already connected to a server $+ $crlf $+ Do you want to quit the current server and connect to %ipv4 $+ ?,wyu,Pacman - Protocol)) return

        .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe(%ipv4) , $unsafe(%port) ) $(|) unset % $+ pcmn
      }
    }
  }
}

alias -l check_load {
  if ($remote !& 2) {
    if ($input($+(Your Events are disabled $+ $chr(44) without them this game won't work properly,$crlf,Would you like to enable them and continue (Yes) $+ $chr(44) or keep them disabled and stop running pacman (No)),ywu,Pacman - Events are off!)) events on
    else halt
  }
  var %i
  if (!$script($scriptdirpac_editor.mrc)) {
    if ($exists($scriptdirpac_editor.mrc)) .load -rs $qt($scriptdirpac_editor.mrc)
    else %i = Editor,
  }
  if (!$script($scriptdirpac_lobby.mrc)) {
    if ($exists($scriptdirpac_lobby.mrc)) .load -rs $qt($scriptdirpac_lobby.mrc)
    else %i = %i Lobby,
  }
  if (!$script($scriptdirpac_client.mrc)) {
    if ($exists($scriptdirpac_client.mrc)) .load -rs $qt($scriptdirpac_client.mrc)
    else %i = %i Client,
  }
  if (!$script($scriptdirpac_server.mrc)) {
    if ($exists($scriptdirpac_server.mrc)) .load -rs $qt($scriptdirpac_server.mrc)
    else %i = %i Server,
  }
  if (%i) {
    noop $input($+(The following core files are missing in $scriptdir :,$crlf,$left(%i,-1),$crlf,Aborting..),huo,Pacman - Loading error)
    halt
  }
}

alias Pacman {
  if (%pacupdate) return
  check_load

  var %r $read($script,tn,1)
  tokenize 32 %r
  dialog $iif($dialog(pacman),-ev,-md $+ $iif($8,o)) pacman pac
}
on *:dialog:pacman:close:0:if ($hget(pacman)) hfree pacman | window -c @packeyconf
on *:dialog:pacman:sclick:11:if ($pacclient_start($$did(12),$$did(79),$input(Leave the field empty if no password is required,upe,Pacman - Password request))) did -b pacman 11

on *:dialog:pacman:sclick:91,92,93,94,96,97,99,100:{
  if ($did == 92) {
    if ($window(@pacman)) {
      if ($did(92).state) window -o @pacman
      else window -u @pacman
    }
  }
  if ($did == 91) {
    if ($did(91).state) dialog -o pacman
    else dialog -n pacman
  }
  var %r $mid($read($script,tn,1),2)
  tokenize 32 %r
  write -dl1 $qt($script)
  write -il1 $qt($script) ; $+ $1-7 $did(91).state $did(92).state $did(93).state $did(73).state $did(94).state $did(97).state $did(99).state $did(100).state $did(96).state $iif($did(36) isnum 0-65535,$v1,8000) $iif($did(37) isnum 0-65535,$v1,8001)
  .reload -rs $qt($script)
}

on *:dialog:pacman:sclick:68,69,70,71:{
  did -b pacman 68-71 
  set %packeyconf $did
  .timer -ho 1 100 window -apdfo +Lt @packeyconf -1 -1 400 50 $(|) window -r @packeyconf $(|) drawrect -frn @packeyconf 0 0 0 0 400 50 $(|) $&
    drawtext -prn @packeyconf 16777215 fixedsys 12 20 8 Press a key on your keyboard for the $+($chr(3),72,$gettok(Left Right Up Down,$calc($did - 67),32),$chr(15)) key $(|) drawtext -rn @packeyconf 16777215 fixedsys 12 20 30 Close the window to cancel $(|) drawdot @packeyconf
}

on *:dialog:pacman:init:0:{
  if (%pac_update) { dialog -x pacman | return }
  .timer -ho 1 0 did -v pacman 58
  dialog -t pacman Pacman $pac_version - You are not connected to the lobby
  did -a pacman 103 Pacmans get a global score by eating fruits, eggs, ghosts, letters, hearts or the pellets, if a ghost eat any of those, the effect is reversed. $+ $crlf $+ That means the score decreases, and you will lose a life if ghosts eat an heart.
  did -a pacman 104 Every 1000 points, a bonus is randomly chosen, a letter or an egg. It is placed randomly on the map, on a square where there's not already an item. $+ $crlf $+ If you reach 1000 points, get a bonus, go back below 1000 because a ghost ate a fruit, you won't get a new bonus reaching 1000 again.
  did -a pacman 105 Fruits have a chance to drop after a pacman eat a small pellet only, power pellet won't give you a chance for fruits. The more points the fruit has, the rarer it is. $+ $crlf $+ An heart can also drop instead of a fruit, granting you one extra life (if picked up by pacman), this heart will be blinking and will disappear after a few seconds.
  did -a pacman 127 If fruits are permanent, eggs and letters are not, eggs stay for 7 seconds while letters are set to go to a given ghost position once, and will disappear themselves when reaching half way. Each egg has a different effect and is worth 50 points, except for the green egg which will reward you the 50 points if you pick the heart spawned by it. Letters are worth 50 points and can only be collected by pacmans, if the four letters m-I-R-C are collected, a powerful friend will come help the pacmans finish the job.  If a ghost pick a letter, the letter disappears. 
  var %r $read($script,tn,1) 
  if (;* !iswm %r) {
    write -il1 $qt($script) ;37 38 39 40 - - 0 0 0 1 0 0 1 1 1 0 8000 8001
    %r = ;37 38 39 40 - - 0 0 0 1 0 0 1 1 1 0 8000 8001
  }
  ;$8 = upnp $9 =
  did -f pacman 8
  tokenize 32 $mid(%r,2)
  if ($11) did -c pacman 73
  if ($8) did -c pacman 91
  if ($9) did -c pacman 92
  if ($10) did -c pacman 93
  if ($12) did -c pacman 94
  if ($13) did -c pacman 97
  if ($14) did -c pacman 99
  if ($15) did -c pacman 100
  if ($16) did -c pacman 96
  if ($17) did -a pacman 36 $v1
  if ($18) did -a pacman 37 $v1
  var %t $1-
  if ($5 != -) did -a pacman 75 $5
  if ($6 != -) did -a pacman 76 $6
  tokenize 44 68 $keyvaltodesc($iif($1,$1,37)) ,69 $keyvaltodesc($iif($3,$3,39)) ,70 $keyvaltodesc($iif($2,$2,38)) ,71 $keyvaltodesc($iif($4,$4,40)) 
  did -ra pacman $*
  tokenize 32 %t
  tokenize 44 1,2,3,4, left $iif($1,$1,37) ,right $iif($3,$3,39) ,up $iif($2,$2,38) ,down $iif($4,$4,40) 
  pac_update?
  hadd -m pacman $*
  if (%pacclient_fconnect) {
    tokenize 32 $v1
    .timer -ho 1 0 pacclient_stop $(|) noop $!pacclient_start( $unsafe($1) , $unsafe($2) ) $(|) unset % $+ pacclient_fconnect
  }
}

on *:keydown:@packeyconf:*:{
  drawrect -frn @packeyconf 0 0 0 0 400 50
  var %kd $keyvaltodesc($keyval),%c 56
  if ((%packeyconf == 68) && ($istok($hget(pacman,right) $hget(pacman,up) $hget(pacman,down),$keyval,32))) || ((%packeyconf == 69) && ($istok($hget(pacman,left) $hget(pacman,up) $hget(pacman,down),$keyval,32))) || ((%packeyconf == 70) && ($istok($hget(pacman,right) $hget(pacman,left) $hget(pacman,down),$keyval,32))) || ((%packeyconf == 71) && ($istok($hget(pacman,right) $hget(pacman,up) $hget(pacman,left),$keyval,32))) var %c 52
  drawtext -prn @packeyconf 16777215 fixedsys 12 10 8 You pressed the key $+($chr(3),%c,%kd,$chr(15)) for the $+($chr(3),72,$gettok(Left Right Up Down,$calc(%packeyconf - 67),32),$chr(15)) key
  if (%c == 52) drawtext -rn @packeyconf 16777215 fixedsys 12 2 30 The key is already in use, press a different key!
  else drawtext -rn @packeyconf 16777215 fixedsys 12 10 30 You may press another key or close the window
  drawdot @packeyconf
  hadd pacman $gettok(Left Right Up Down,$calc(%packeyconf - 67),32) $keyval
  did -ra pacman %packeyconf %kd
  var %r $mid($read($script,tn,1),2),%t $gettok(%r,7-,32)
  write -dl1 $qt($script)
  write -il1 $qt($script) ; $+ $iif($hget(pacman,left),$v1,37) $iif($hget(pacman,up),$v1,38) $iif($hget(pacman,right),$v1,39) $iif($hget(pacman,down),$v1,40) $iif($did(pacman,75) != $null,$replace($v1,$chr(32),_),-) $iif($did(pacman,76) != $null,$replace($v1,$chr(32),_),-) %t
  .reload -rs $qt($script)
}
on *:close:@packeyconf:did -e pacman 68-71 | unset %packeyconf
on *:dialog:pacman:edit:75,76:{
  var %r $mid($read($script,tn,1),2),%t $gettok(%r,7-,32)
  write -dl1 $qt($script)
  write -il1 $qt($script) ; $+ $iif($hget(pacman,left),$v1,37) $iif($hget(pacman,up),$v1,38) $iif($hget(pacman,right),$v1,39) $iif($hget(pacman,down),$v1,40) $iif($did(75) != $null,$replace($v1,$chr(32),_),-) $iif($did(76) != $null,$replace($v1,$chr(32),_),-) %t
  .reload -rs $qt($script)
}

alias pac_record {
  var %v
  if ($1 != $null)  {
    if (!$exists($1-)) return
    var %v $1-
  }
  elseif ($sfile($scriptdirrecords\,Select the file of the record) != $null) var %v $v1
  else return
  if ($hget(pacreplay)) {
    if (%v == $hget(pacwatch,file)) && ($window(@pac_watch)) {
      window -a  @pac_watch
      return
    }
    hfree pacreplay
    hfree pacwatch
    .timerpacwatch off
  }
  hload -Bm pacreplay $qt(%v)
  if ($hget(pacreplay,0).item < 2) {
    echo -sg * Pacwatch: file $qt(%v) is invalid, too few items or empty
    hfree pacreplay
    return
  }

  hadd -m pacwatch frame 1
  hadd pacwatch file %v
  hadd pacwatch dsize 2
  tokenize 32 $hget(pacreplay,size)
  hadd pacwatch size $hget(pacreplay,size)
  hdel pacreplay size
  hadd pacwatch frames $hget(pacreplay,0).item
  window -CBdfkpoa +tn @pac_watch -1 -1 $1 $calc($2 + 50)
  drawrect -fr @pac_watch 16777215 0 0 0 $1 $calc($2 + 50)
  drawrect -er @pac_watch 0 3 $calc($1 / 2 - 10) $calc($2 + 15) 20 20
  drawtext -r @pac_watch 0 verdana 10 $calc($1 / 2 - 5) $calc($2 + 18) ||
  ; drawtext -r @pac_watch 0 verdana 10 $calc($1 / 2 - 5 + 50) $calc($2 + 18) >>
  ; drawtext -r @pac_watch 0 verdana 10 $calc($1 / 2 - 5 - 50) $calc($2 + 18) <<
  var %time $calc($hget(pacwatch,frames) / 24)
  var %m $base($calc(%time // 60),10,10,2)
  var %s $base($calc(%time % 60),10,10,2)
  drawtext -r @pac_watch 0 verdana 10  $calc($1 / 2 - 5 + $1 / 4) $calc($2 + 18) 00:00 / %m $+ : $+ %s
  hadd pacwatch pausex $calc($1 / 2 - 10)
  hadd pacwatch pausey $calc($2 + 15)
  .timerpacwatch -ho 0 40 pac_show
  :error
  if ($error) {
    reseterror
    if (hload isin $v1) noop $input(The selected file is incorrect!,wou,Pacman - Record)
    else echo -sg pacman error in pac_record: $v2
  }
}

alias pac_show {
  if ($show) hinc pacwatch frame
  if ($hget(pacreplay,frame $+ $hget(pacwatch,frame),&frame)) {
    drawpic -vn @pac_watch 0 0 &frame
    var %time $ceil($calc($hget(pacwatch,frames) / 25))
    var %m $base($calc(%time // 60),10,10,2),%mm $base($calc($hget(pacwatch,frame) // 25 // 60),10,10,2)
    var %s $base($calc(%time % 60),10,10,2),%ss $base($calc($hget(pacwatch,frame) // 25 % 60),10,10,2)
    tokenize 32 $hget(pacwatch,size)
    var %w $calc($1  * $hget(pacwatch,frame) / $hget(pacwatch,frames)),%p $2,%ds $hget(pacwatch,dsize)
    if (%ds == 6) inc %p 3
    else inc %p 1
    drawline -rn @pac_watch 255 %ds 0 %p %w %p
    var %x $calc($1 / 2 - 5 + $1 / 4)
    drawrect  -rfn @pac_watch 16777215 0 %x $calc($2 + 18) 400 30
    drawtext -rn @pac_watch 0 verdana 10 %x $calc($2 + 18) %mm $+ : $+ %ss / %m $+ : $+ %s
    titlebar @pac_watch File: $hget(pacwatch,file) -- Frame $hget(pacwatch,frame) / $hget(pacwatch,frames)
  }
  else { 
    .timerpacwatch off
    hadd pacwatch pause 1
    hdec pacwatch frame
    drawrect -frn @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    drawtext -rn @pac_watch 0 verdana 15 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 0) ▶
    drawrect -ern @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    var %time $ceil($calc($hget(pacwatch,frames) / 25))
    var %m $base($calc(%time // 60),10,10,2)
    var %s $base($calc(%time % 60),10,10,2)
    tokenize 32 $hget(pacwatch,size)
    var %x $calc($1 / 2 - 5 + $1 / 4)
    drawrect  -rfn @pac_watch 16777215 0 %x $calc($2 + 18) 400 30
    drawtext -rn @pac_watch 0 verdana 10 %x $calc($2 + 18) %m $+ : $+ %s / %m $+ : $+ %s
  }
  drawdot @pac_watch
}
alias pacwatch_handle_mouse {
  tokenize 32 $hget(pacwatch,size)
  if ($mouse.y isnum $2 $+ - $+  $calc($2 + 6)) hadd pacwatch dsize 6
  else  hadd pacwatch dsize 2
  var %ds $hget(pacwatch,dsize)
  if (%ds == 2) drawrect -rnf @pac_watch 16777215 0 0 $2 $1 6
  var %w $calc($1  * $hget(pacwatch,frame) / $hget(pacwatch,frames)),%p $2
  if (%ds == 6) inc %p 3
  else inc %p 1
  drawline -rn @pac_watch 255 %ds 0 %p %w %p
  if ($hget(pacwatch,holdclick)) {
    if (%ds == 6) {
      var %f $calc($hget(pacwatch,frames) * $mouse.x // $1)
      drawrect -frn @pac_watch 16777215 0 0 $2 $1 6
      hadd pacwatch frame %f
      if ($hget(pacwatch,pause)) .pac_show
    }
  }
  drawdot @pac_watch
}
alias pacwatch_handle_click {
  tokenize 32 $hget(pacwatch,size)
  if ($inellipse($mouse.x,$mouse.y,$hget(pacwatch,pausex),$hget(pacwatch,pausey),20,20)) || (($mouse.x isnum 0- $+ $1) && ($mouse.y isnum 0- $+ $2))  {
    if (!$hget(pacwatch,pause)) {
      hadd pacwatch pause 1
      .timerpacwatch -p
      drawrect -fr @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
      drawtext -r @pac_watch 0 verdana 15 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 0) ▶
      drawrect -er @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    }
    else {
      hadd pacwatch pause 0
      if ($hget(pacwatch,frame) == $hget(pacwatch,frames)) {
        hadd pacwatch frame 1
        drawrect -rnf @pac_watch 16777215 0 0 $2 $1 6
      }
      if (!$timer(pacwatch)) .timerpacwatch -ho 0 40 pac_show
      else .timerpacwatch -r
      drawrect -fr @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
      drawrect -er @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
      drawtext -r @pac_watch 0 verdana 10 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 3) ||
    }
  }
  elseif ($hget(pacwatch,dsize) == 6) {
    var %f $calc($hget(pacwatch,frames) * $mouse.x // $1)
    drawrect -frn @pac_watch 16777215 0 0 $2 $1 6
    hadd pacwatch frame %f
    hadd pacwatch pause 1
    if ($timer(pacwatch)) && (!$timer(pacwatch).pause) hadd pacwatch nopause 1
    .timerpacwatch -p
    drawrect -frn @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    drawtext -rn @pac_watch 0 verdana 15 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 0) ▶
    drawrect -ern @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    drawdot @pac_watch
    .pac_show
    hadd pacwatch holdclick 1
  }
}

menu @pac_watch {
  dclick:pacwatch_handle_click
  sclick:pacwatch_handle_click
  mouse:pacwatch_handle_mouse
  uclick:{ if ($hget(pacwatch,holdclick)) { 
      hdel pacwatch holdclick 
      if ($hget(pacwatch,nopause)) {
        hdel pacwatch pause
        hdel pacwatch nopause
        .timerpacwatch -r
        drawrect -fr @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
        drawrect -er @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
        drawtext -r @pac_watch 0 verdana 10 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 3) ||
      }
    }
  }
}
on *:keydown:@pac_watch:37,39:{
  if (($keyval == 37) && ($hget(pacwatch,frame) > 1)) || (($keyval == 39) && ($hget(pacwatch,frame) <= $hget(pacwatch,frames))) {
    $iif($keyval == 37,hdec,hinc) pacwatch frame
    if ($hget(pacwatch,pause)) .pac_show
  }
}
on *:keydown:@pac_watch:32:{
  tokenize 32 $hget(pacwatch,size)
  if (!$hget(pacwatch,pause)) {
    hadd pacwatch pause 1
    .timerpacwatch -p
    drawrect -fr @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    drawtext -r @pac_watch 0 verdana 15 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 0) ▶
    drawrect -er @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
  }
  else {
    hadd pacwatch pause 0
    if ($hget(pacwatch,frame) == $hget(pacwatch,frames)) {
      hadd pacwatch frame 1
      drawrect -rnf @pac_watch 16777215 0 0 $2 $1 6
    }
    if (!$timer(pacwatch)) .timerpacwatch -ho 0 40 pac_show
    else .timerpacwatch -r
    drawrect -fr @pac_watch 16777215 0 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    drawrect -er @pac_watch 0 3 $hget(pacwatch,pausex) $hget(pacwatch,pausey) 20 20
    drawtext -r @pac_watch 0 verdana 10 $calc($hget(pacwatch,pausex) + 5) $calc($hget(pacwatch,pausey) + 3) ||
  }
}
on *:close:@pac_watch:.timerpacwatch off | hfree pacwatch | hfree pacreplay

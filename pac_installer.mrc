alias -l pac_cinstall {
  if (!$sslready) return $input(You do not have SSL capabilities. SSL is required to download Pacman $+ $crlf See http://www.mirc.com/ssl.html for more information,wo,Pacman install)
  if ($input($+(Do you want to install to the default folder?,$crlf,Default folder: $scriptdirPacman\),fnq,Pacman - Install) == $cancel) return
  elseif ($v1 != $null) set %pac_dir_install $scriptdirPacman\
  elseif ($sdir($scriptdir,Where do you want to install Pacman to?) != $null) {
    var %dir $v1
    if (!$istok(cancel error,$sfstate,32)) set %pac_dir_install %dir
    else return
  }
  else return $input($+(The installation has been cancelled or an error occured,$crlf,Execute this installer again if you want to install),ow,Pacman - Install)
  if (!$isdir(%pac_dir_install))  mkdir $qt(%pac_dir_install)
  mkdir $qt(%pac_dir_install $+ maps\)
  sockclose pac_ckinst
  sockopen -e pac_ckinst gitlab.com 443
  sockmark pac_ckinst $false
  write -c $qt(%pac_dir_install $+ update.pac)
}

on *:sockopen:pac_ckinst:{
  if (!$sockerr) {
    sockwrite -n $sockname GET /Ouims/Pacman/raw/master/update.pac HTTP/1.0
    sockwrite -n $sockname Host: $sock($sockname).addr
    sockwrite -n $sockname Connection: close
    sockwrite -n $sockname
  }
  else {
    .timer -ho 1 0 noop $!input(Cannot connect to the server $!+ $!chr(44) aborting the installation..,oh,Pacman - Install)
    unset %pac_dir_install
  }
}

on *:sockread:pac_ckinst:{
  if (!$sockerr) {
    if ($sock($sockname).mark) {
      sockread -f &a 
      bwrite $qt(%pac_dir_install $+ update.pac) -1 -1 &a
    }
    else {
      var %a
      sockread %a
      if (!$sockbr) return
      if (%a == $null) sockmark $sockname 1
      elseif ($sock($sockname).mark == $false) {
        if (*200 OK* !iswm %a) {
          .timer -ho 1 0 noop $!input(Cannot find the list of files $!+ $!chr(44) the installation has been aborted.,ho,Pacman - Install)
          sockclose $sockname
          .remove $qt(%pac_dir_install $+ update.pac)
          unset %pac_dir_install
        }
        sockmark $sockname 0
      }
    }
  }
  else {
    .timer -ho 1 0 noop $!input(An error occured while downloading the list of files $!+ $!chr(44) the installation has been aborted.,ho,Pacman - Install)
    .remove $qt(%pac_dir_install $+ update.pac)
    unset %pac_dir_install
  }
}
on *:sockclose:pac_ckinst:{
  if ($sockerr) {
    .timer -ho 1 0 noop $!input(An SSL error occured while trying to connect to the server $!+ $!crlf $!+ You have invalid SSL settings in option > connect > options > ssl dialog $!+ $!chr(44) aborting the installation $!+ ..,ho,Pacman - Install)
    return
  }
  pac_install
}

alias -l pac_install {
  var %a 2
  set %pacprogress $int($calc(200 / ($lines(%pac_dir_install $+ update.pac) - 1)))
  window -kpdfC +l $+(@Installing,$chr(160),Pacman...) -1 -1 200 40
  while ($read(%pac_dir_install $+ update.pac,tn,%a)) {
    .timerpacinstall $+ %a -oh 1 $calc(%a * 250) pac_dl_install $unsafe(%a $v1)
    inc %a
  }
  set %pacinstall %a - 2
}

alias -l pac_dl_install {
  sockclose pac_install $+ $1
  sockopen -e pac_install $+ $1 gitlab.com 443
  sockmark pac_install $+ $1 $2-
}
on *:sockopen:pac_install?*:{ 
  if (!$sockerr) {
    .fopen -no $sockname $qt(%pac_dir_install $+ $sock($sockname).mark)
    sockwrite -n $sockname GET /Ouims/Pacman/raw/master/ $+ $sock($sockname).mark HTTP/1.0
    sockwrite -n $sockname Host: $sock($sockname).addr
    sockwrite -n $sockname Connection: close
    sockwrite -n $sockname 
    sockmark $sockname $false
  }
  else {
    .timer -ho 1 0 noop $!input(Cannot connect to the server to download the file $nopath($sock($sockname).mark) $!+ $!chr(44) the installation has been aborted.,oh,Pacman - Install)
    pac_abortinstall
  }
}

on *:sockread:pac_install?*:{
  if (!$sockerr) {
    if ($sock($sockname).mark) {
      sockread -f &a 
      .fwrite -b $sockname &a
    }
    else {
      var %a
      sockread %a
      if (!$sockbr) return
      if (%a == $null) sockmark $sockname 1
      elseif ($sock($sockname).mark == $false) {
        if (*200 OK* !iswm %a) {
          .timer -ho 1 0 noop $!input(Cannot find the file $nopath($fopen($sockname).fname) on the server $!+ $!chr(44) the installation has been aborted.,ho,Pacman - Install)
          pac_abortinstall
        }
        sockmark $sockname 0
      }
    }
  }
  else {
    .timer -ho 1 0 noop $!input(An error occured while downloading the file $nopath($fopen($sockname).fname) $!+ $!chr(44) the installation has been aborted.,ho,Pacman - Install)
    pac_abortinstall
  }
}
on *:close:$($+(@,installing,$chr(160),Pacman...)):noop $pac_abortinstall | .timer -ho 1 0 noop $!input(The installation was aborted,iou,Pacman - Install )


alias -l pac_abortinstall {
  .timerpacinstall* off
  sockclose pac_install?*
  .fclose pac_install?*
  var %a 2
  while ($read(%pac_dir_install $+ update.pac,tn,%a) != $null)  {
    var %v $v1
    if ($isfile(%pac_dir_install $+ %v)) {  
      if (*.mrc iswm %v) && ($script(%pac_dir_install $+ %v)) .unload -rs $qt(%pac_dir_install $+ %v)
      .remove $qt(%pac_dir_install $+ %v)
    }
    inc %a
  }
  .remove $qt(%pac_dir_install $+ update.pac)
  unset %pac_dir_install %pacinstall %pacprogress
  window -c $+(@Installing,$chr(160),Pacman...)
}

on *:sockclose:pac_install?*:{
  var %f $fopen($sockname).fname
  dec %pacinstall
  .fclose $sockname
  if ($sockerr) {
    pac_abortinstall
    .timer -ho 1 0 noop $!input(An error (SSL?) occured while downloading the file $nopath(%f) $!+ $!chr(44) the installation has been aborted.,ho,Pacman - Install)
    return  
  }
  elseif ($lof(%f) == 0) {
    pac_abortinstall
    .timer -ho 1 0 noop $!input(An error (SSL?) occured while downloading the file $nopath(%f) $!+ $!chr(44) the installation has been aborted.,ho,Pacman - Install)
    return
  }
  drawrect -fr $+(@Installing,$chr(160),Pacman...) 64512 0 0 0 $calc(200 - %pacinstall * %pacprogress) 40
  if ($right(%f,4) == .mrc) .load -rs $qt(%f)
  if (!%pacinstall) { unset %pacinstall %pacprogress %pac_dir_install | window -c $+(@Installing,$chr(160),Pacman...) | .timer -h 1 0 pac_finstall }
}
alias -l pac_finstall {
  noop $input(Pacman installed sucessfully!,to,Pacman - Install)
  if ($input($+(Do you want to delete the file used for the installer (that file will be unloaded as well)?,$crlf,File : $script),wqy,Pacman - Install)) {
    !.remove $qt($script)
    var %u 1
  }
  if (!$v1) && ($input(Do you want to unload that file anyway?,wyq,Pacman - Install)) var %u 1
  var %m1 menubar,%m2 menubar,status,%m3 menubar,channel, %m4 menubar,query,%m5 menubar,status,channel,%m6 menubar,status,query,%m7 menubar,status,channel,query,%m8 No menu, please
  var %menu $input(Please select some popups locations for Pacman,mio,Pacman - Install,%m1,%m1,%m2,%m3,%m4,%m5,%m6,%m7,%m8)
  var %read $read($script(pacman.mrc),wnt,menu & $({,0))
  if (%menu != No menu, please) && (%menu) write -l $+ $readn $qt($script(pacman.mrc)) menu %menu $({,0)
  else write -l $+ $readn $qt($script(pacman.mrc)) nomenu $({,0)
  ;left up right down nicklobby nickgame autodupdate dialtop wintop blink upnp
  write -il1 $qt($script(pacman.mrc)) ;37 38 39 40 - - 0 0 0 1 0 0 8000 8001
  if (!$isalias(f5)) {
    write $qt($script(pac_client.mrc)) alias f5 if ($sock(pacclient)) sockwrite -n pacclient pause
    .reload -rs $qt($script(pac_client.mrc))
  }
  .reload -rs $qt($script(pacman.mrc))
  pacman
  if (%u) .unload -rs $qt($script)
}

menu menubar,channel,query,status {
  Pacman Install: pacman_install
}
alias Pacman_install {
  if ($script(pacman.mrc)) || ($script(pac_server.mrc)) || ($script(pac_client.mrc)) || ($script(pac_lobby.mrc)) || ($script(pac_editor.mrc)) return $input($+(The following Pacman file is loaded:,$crlf,$v1,$crlf,Please unload it before reinstalling Pacman),oiu,Pacman - Install)
  
  if ($~adiircexe) || ($version >= 7.22) || ($input($+(Pacman Online requires a version of mIRC greater or equal to 7.22,$crlf,You are using mIRC v,$version,$chr(44) $chr(32),Do you want to install nonetheless?),yuw,Pacman - Install)) pac_cinstall
}
